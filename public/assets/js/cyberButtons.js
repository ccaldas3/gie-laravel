var fxArray = {
    '0' : 'https://s3-us-west-2.amazonaws.com/s.cdpn.io/217233/the_dark_footsteps.mp3',
    '1' : 'https://s3-us-west-2.amazonaws.com/s.cdpn.io/217233/the_dark_thud.wav', /// posible apertura
    '2' : 'https://s3-us-west-2.amazonaws.com/s.cdpn.io/217233/buttonHoverScary.wav',
    '3' : 'https://s3-us-west-2.amazonaws.com/s.cdpn.io/217233/drinking.wav',
    '4' : 'https://s3-us-west-2.amazonaws.com/s.cdpn.io/217233/Clothes_Backpack_02-%5BAudioTrimmer.com%5D.mp3',
    '5' : 'https://s3-us-west-2.amazonaws.com/s.cdpn.io/217233/Eat%20Vegetables%201-%5BAudioTrimmer.com%5D.mp3',
    '6' : 'https://s3-us-west-2.amazonaws.com/s.cdpn.io/217233/Low%20Hits%20(2).wav',    // Este
    '7' : 'https://s3-us-west-2.amazonaws.com/s.cdpn.io/217233/foley_cloth_light_fast_movement_long_01.mp3',
    '8' : 'https://s3-us-west-2.amazonaws.com/s.cdpn.io/217233/Zombies%203.mp3',
    '9' : 'https://s3-us-west-2.amazonaws.com/s.cdpn.io/217233/counter-03-by_YIO.wav'
}
var bgArray = {
    '0' : 'https://s3-us-west-2.amazonaws.com/s.cdpn.io/217233/The%20Plague%20Doctor%20(online-audio-converter.com).mp3',
    '1' : 'https://s3-us-west-2.amazonaws.com/s.cdpn.io/217233/the_dark_water.mp3'
}

var masterFx = [];
var masterBg = [];


function prepareAudio(audio, array, looping) {
    Object.keys(audio).forEach(function(key) {
        let a = new Audio(audio[key]);
        a.loop = looping;
        array.push(a);
    })
}

audio = true;
sfx = true;

prepareAudio(fxArray, masterFx, false);
prepareAudio(bgArray, masterBg, true);

function playFx(id, volume) {
    if(audio) {
        masterFx[id].volume = volume;
        masterFx[id].play();
    }
}

function playBg(id, volume) {
    if(sfx) {
        masterBg[id].volume = volume;
        masterBg[id].play();
    }
}



// HTML Elements
const characterCreationForm = $('.characterForm');



$('.js-career, .js-trait, label, .button, .js-randomize, .js-createCharacter, .row .button, li, a, .service-item').mouseenter(function() {
	console.log("sonidos 2");
    playFx(2, 1)
})

$('li, a, .service-item, .category').mouseover(function() {
    console.log("sonidos 3");
    playFx(2, 1)
})

console.log("hola ciberbuttons");
 

var introDone = true;
$('.game_characterCreation').css('opacity', 1);
            $('.game_characterCreation').css('pointer-events', 'all');

/* ===================================================

Game

===================================================== */


 
// Bugs
// Reset training on restart
// Can still scavenge when tired