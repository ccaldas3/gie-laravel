// let lightContainer = document.getElementById('light-box');
// let mc = new Hammer(body);
// function intervalScrolling() {
//     if (scrolling && !scrollingToSection) {
        
//         scrolling = false;
//         scrollingToSection = true;
//         scrollHandler();

//         if (nextSection === 7) {
//             signature.initialize();
//             setTimeout(function () {
//                 return signature.animate();
//             }, 500);
//         }

//     }
//     if (!ActiveMenu) {
//         scrollHandler();
//     }
    
    
    
// }

// setInterval(intervalScrolling, 200);





var master = new TimelineMax({delay:1.2}),
    centerY = $("#caja-contador").height() / 2,
    centerX = $("#caja-contador").width() / 2,
    radius = Math.max(centerX, centerY) + 50,
    slider = $("#ctrl_slider"),
    sliderValue = {value:0},
    _isOldIE = (document.all && !document.addEventListener);


    master.add( performance(), "-0.3")

    var n = 0;
    var l = document.getElementById("contador");
    var time = window.setInterval(myTimer,50);
    function myTimer() {
      l.innerHTML = n + "%";
      n++;
      if(n>= 101){
        window.clearInterval(time);
        // document.getElementById("cargado").show();
      }
    }

    function performance() {
        var tl = new TimelineLite({id:"contador"}),
            text = $("#contador"),
            cajaContador = $("#caja-contador");
            bg = $("#cargando");
            section = $(".section");
            hideCharger = $(".hide-charger");

            // tl.fromTo(text, 6, {scale:1.8, y:centerY, x:centerX+100, z:0.1}, {scale:2.8, ease:SlowMo.ease.config(0.5, 0.4)}, -10.3);
            tl.fromTo(text, 5, {scale:0.1, y:centerY, z:0.1}, {scale:1.8, ease:SlowMo.ease.config(0.5, 0.4)}, 0.1);
            tl.fromTo({text, cajaContador}, 5, {opacity:0}, {autoAlpha:1, ease:SlowMo.ease.config(0.1, 0.1, false)}, 0.3)
            .to(text, 0.5, {y:200, opacity:0, ease:Power2.easeIn});

            tl.to(bg, 1, {autoAlpha:0, opacity:0, ease:Power1.easeIn}, "-=0.5")
            .fromTo ( bg , 3, {scale:1}, {scale:15, ease:ExpoScaleEase.config(1, 8)}, "-=0.7")
            .to(section, 2, {autoAlpha:1, opacity:1, ease:Power1.easeIn}, "-=2.6")

            tl.to(hideCharger, 0.1, { display :'block', opacity : 1})
            .to(bg, 0.1, { display :'none' })
            .to( $("body"), 0.1, { overflow :'inherit' });

            // .to(hideCharger, 2, {display:'block', x:500, autoAlpha:1, opacity:1, ease:Power1.easeIn}, "-=2.6");

            // tl.set(stars, {display:"none"});
            return tl;
        }