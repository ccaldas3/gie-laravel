// let ActiveMenu = false;
let bodyEl = document.querySelector('body');
let scrollText = document.querySelector('.scroll-text');
let btnMenu = document.getElementById('btn-menu');
let menuSection = document.getElementById('menu-section');
let socialList = document.getElementById('social-list-side');
let logo = document.querySelector('.logo');
// let header = document.querySelector('.header');
let sideBar = document.querySelector('.side-bar');

const checkMenu = function() {
    if (!ActiveMenu) {
        menuSection.classList.add('isVisible');
        btnMenu.classList.add('btn-menu-active');
        if (socialList) { socialList.classList.add('hide'); }
        logo.classList.add('little-black-logo');
        // header.classList.add('header-menu');
        // header.style.zIndex = 13;
        sideBar.style.zIndex = 13;
        // if (currentSection > 0) {
        	// header.classList.remove('little-header'); 
        // }
        bodyEl.style.overflow = 'hidden';



    } else {
        logo.style.visibility = 'visible';
        $('.hud').css('visibility', 'hidden');

        bodyEl.style.overflow = 'auto';
        menuSection.classList.remove('isVisible');
        btnMenu.classList.remove('btn-menu-active');
        logo.classList.remove('little-black-logo');
        // header.classList.remove('header-menu');
        // header.style.zIndex = 10;
        sideBar.style.zIndex = 10;
        // if (currentSection === 0) {
        //     if (socialList) { socialList.classList.remove('hide'); }
        // } else {
        //     // header.classList.add('little-header');
        // }
    }
}


const handleClickBtnMenu = function() {
    checkMenu();
    ActiveMenu = !ActiveMenu;
}

if (scrollText) {
    scrollText.addEventListener('click', () => {
        nextSection = 1
        scrollingToSection = true;
        prepareNextSection();
    });
}
function handleLight(event) {
    event.preventDefault();
    let x, y;
    x = event.clientX || event.touches[0].clientX;
    y = event.clientY || event.touches[0].clientY - 100;

    lightContainer.style.setProperty('--cursorX', x + 'px');
    lightContainer.style.setProperty('--cursorY', y + 'px');
}

btnMenu.addEventListener('click', handleClickBtnMenu);

if (/Mobi|Android/i.test(navigator.userAgent)) {
    lightContainer.addEventListener('touchmove', handleLight);
} else {
    lightContainer.addEventListener('mousemove', handleLight);
}

