AOS.init();

let ActiveMenu = false;
let btnMenu = document.getElementById('btn-menu');
let bodyEl = document.querySelector('body');
let currentSection = 0;
let header = document.querySelector('.header');
let iconMenu = document.getElementById('icon-menu');
let logo = document.querySelector('.logo');
let menuSection = document.getElementById('menu-section');
let allSection = document.getElementsByClassName('section');
let nextSection;
let scrollDirection = '';
let scrolling = false;
let scrollingToSection = false;
let scrollPos = 0;
let scrollText = document.querySelector('.scroll-text');
let sideBar = document.querySelector('.side-bar');
let socialList = document.getElementById('social-list-side');

function debounce(func, wait, immediate) {
    var timeout;
    return function() {
        var context = this, args = arguments;
        var later = function() {
            timeout = null;
            if (!immediate) func.apply(context, args);
        };
        var callNow = immediate && !timeout;
        clearTimeout(timeout);
        timeout = setTimeout(later, wait);
        if (callNow) func.apply(context, args);
    };
};

function autoScroll(selector) {
    var scrollAttempts = 0;
    var incrementScrollAttempts = debounce(function() {
        scrollAttempts++;
    });

    window.addEventListener('scroll', incrementScrollAttempts);

    var el = document.querySelector(selector);
    var chkReadyState = setInterval(function() {
        if (el) {
            window.scrollTo({
                top: el.offsetTop,
                behavior: 'smooth',
            });
        }
        if (document.readyState == 'complete' || scrollAttempts > 1) {
            clearInterval(chkReadyState);
            window.removeEventListener('scroll', incrementScrollAttempts, false);
        }
    }, 100);
};


function scrollHandler() {
    if (scrollDirection === 'down'){
        nextSection = currentSection < 8 ? ++currentSection : currentSection;
        prepareNextSection();
    } else if (scrollDirection === 'up') {
        nextSection = currentSection > 0 ? --currentSection : currentSection;
        prepareNextSection();
    }
}

function prepareNextSection() {
    currentSection = nextSection;
    
    if(document.documentElement.scrollTop > 200) {
        if(header !=undefined)
            header.classList.add('little-header');
        logo.classList.add('little-logo');
        socialList.classList.add('hide');
        btnMenu.classList.add('onscroll');
        iconMenu.classList.add('onscroll');
    } else {
        if(header !=undefined)
            header.classList.remove('little-header');

        logo.classList.remove('little-logo');
        socialList.classList.remove('hide');
        btnMenu.classList.remove('onscroll');
        iconMenu.classList.add('onscroll');
    }

    // autoScroll(sections[nextSection]);

    if (ActiveMenu) { checkMenu(); }
}

const checkMenu = function() {
    if (!ActiveMenu) {
        menuSection.classList.add('isVisible');
        btnMenu.classList.add('btn-menu-active');
        if (socialList) { socialList.classList.add('hide'); }
        logo.classList.add('little-black-logo');
        if(header !=undefined){
            header.classList.add('header-menu');
            header.style.zIndex = 13;
            
            if (currentSection > 0) {header.classList.remove('little-header'); }
        }
        sideBar.style.zIndex = 13;
        bodyEl.style.overflow = 'hidden';



    } else {
        logo.style.visibility = 'visible';
        $('.hud').css('visibility', 'hidden');

        bodyEl.style.overflow = 'auto';
        menuSection.classList.remove('isVisible');
        btnMenu.classList.remove('btn-menu-active');
        logo.classList.remove('little-black-logo');
        if(header !=undefined){
            header.classList.remove('header-menu');
            header.style.zIndex = 10;
            
            if (currentSection === 0) {
                if (socialList) { socialList.classList.remove('hide'); }
            } else {
                header.classList.add('little-header');
            }
        }
        sideBar.style.zIndex = 10;
    }
}

const handleClickBtnMenu = function() {
    checkMenu();
    ActiveMenu = !ActiveMenu;
}

if (scrollText) {
    scrollText.addEventListener('click', () => {
        nextSection = 1
        scrollingToSection = true;
        prepareNextSection();
    });
}

btnMenu.addEventListener('click', handleClickBtnMenu);
