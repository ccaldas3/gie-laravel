// Define the `phonecatApp` module
var phonecatApp = angular.module('phonecatApp', [], function($interpolateProvider) {
            $interpolateProvider.startSymbol('<%');
            $interpolateProvider.endSymbol('%>');
  });

// Define the `PhoneListController` controller on the `phonecatApp` module
phonecatApp.controller('PhoneListController', function PhoneListController($scope) {

	$scope.services = [
    {
      bullet: 'A.',
      title: 'INVESTIGACIÓN PARA LA DEFENSA TÉCNICA',
      subtitle: 'REPRESENTACIÓN DE VÍCTIMAS',
      url: 'defensa-tecnica',
      items: [
      		{
		      sombra: 'Recaudo ',
		      text: 'Documental'
		    }, {
		      sombra: 'Realización de ',
		      text: 'Entrevistas'
		    }, {
		      sombra: 'Obtención de ',
		      text: 'Información'
		    }, {
		      sombra: 'Audiencia de Búsqueda',
		      text: 'Selectiva en Base de Datos'
		    }, {
		      sombra: '',
		      text: 'Comprarencia'
		    }, {
		      sombra: 'Informes ',
		      text: 'Técnicos'
		    }, {
		      sombra: 'Gastos',
		      text: 'Operativos Adicionales'
		    }, {
		      sombra: 'Conclusiones de Actividades de',
		      text: 'Audiencia Preparatoria y Jucio Oral'
		    }, {
		      sombra: 'Actividades',
		      text: 'Complementarias'
		    }
      ],
    }, {
      bullet: 'B.',
      title: 'INVESTIGACIÓN PRIVADA EMPRESARIAL',
      subtitle: '',
      url: 'investigacion-privada',
      items: [
      		{
		      sombra: 'Asesorías & Actividades de ',
		      text: 'Prevención Documental'
		    }, {
		      sombra: 'Consultoría & ',
		      text: 'Asesorías en Seguridad'
		    }, {
		      sombra: 'Investigación',
		      text: 'Empresarial'
		    }, {
		      sombra: 'Visita ',
		      text: 'Domiciliaria'
		    }, {
		      sombra: 'Verificación & ',
		      text: ' Referencia Personal'
		    }, {
		      sombra: 'Verificación de ',
		      text: 'Referencia Personal'
		    }, {
		      sombra: 'Verificación de ',
		      text: 'Conflictos'
		    }, {
		      sombra: 'Verificación ',
		      text: 'Financiera'
		    }, {
		      sombra: ' ',
		      text: 'Poligrafías'
		    }

      ],
    }, {
      bullet: 'C.',
      title: 'INVESTIGACIÓN FINANCIERA',
      subtitle: ' FORENSE',
      url: 'investigacion-financiera',
      items: [
      		{
		      sombra: 'Análisis y crítica ',
		      text: 'de elementos materiales probatorios'
		    }
		    // }, {
		    //   sombra: 'Asesoría  ',
		    //   text: 'en los procesos'
		    // }, {
		    //   sombra: 'Asesorar y controvertir ',
		    //   text: 'informes de estados financieros y libros contables'
		    // }, {
		    //   sombra: 'Asesorías respecto a ',
		    //   text: 'determinados conceptos y definiciones'
		    // }, {
		    //   sombra: 'Elaboración  ',
		    //   text: 'de cuestionarios'
		    // }
		    // , {
		    //   sombra: 'Estudios  ',
		    //   text: 'económicos '
		    // }
		    // , {
		    //   sombra: 'Examen y análisis de ',
		    //   text: ' documentación financiera'
		    // }
		    // , {
		    //   sombra: 'Actualizaciones de cifras, ',
		    //   text: 'valores financieros y/o peritajes anteriores'
		    // }
		    // , {
		    //   sombra: 'Valoración ',
		    //   text: 'informesde daños y perjuicios'
		    // }
		    // , {
		    //   sombra: 'Consulta de los diferentes ',
		    //   text: 'índices, tasas y valores necesarios'
		    // }
		    // , {
		    //   sombra: 'Determinar el valor del ',
		    //   text: 'objeto material de la conducta'
		    // }
		    // , {
		    //   sombra: 'Evaluación y revisión de ',
		    //   text: 'estados financieros y de resultado'
		    // }

      ],
    }, {
      bullet: 'D.',
      title: 'LABORATORIO DE CIBERSEGURIDAD',
      subtitle: 'E INFORMÁTICA FORENSE',
      url: 'ciber',
      items: [
      		{
		      sombra: 'Desde las ',
		      text: 'Directrices'
		    }, {
		      sombra: 'Desde la Óptica del ',
		      text: 'Derecho Penal'
		    }, {
		      sombra: 'Desde la Óptica de la ',
		      text: 'Investigación Privada'
		    }

      ],
    }, {
      bullet: 'F.',
      title: 'PSICOLOGÍA JURÍDICA',
      subtitle: ' FORENSE',
      url: 'psicologia',
      items: [
      		{
		      sombra: 'Evaluación Psicológica forense a la ',
		      text: 'Documentación de la Presunta Víctima'
		    }, {
		      sombra: 'Desarrollo de la Conducta & ',
		      text: 'Análisis Funcional '
		    }, {
		      sombra: '',
		      text: 'Objetivo General'
		    }, {
		      sombra: 'Objetivos de ',
		      text: 'Evaluación'
		    }, {
		      sombra: 'Prueba Psicológica de ',
		      text: 'Refutación'
		    }


      ],
    },
  ];



  $scope.phones = [
    {
      name: 'Nexus S',
      snippet: 'Fast just got faster with Nexus S.'
    }, {
      name: 'Motorola XOOM™ with Wi-Fi',
      snippet: 'The Next, Next Generation tablet.'
    }, {
      name: 'MOTOROLA XOOM™',
      snippet: 'The Next, Next Generation tablet.'
    }
  ];
});