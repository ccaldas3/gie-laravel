let body = document.querySelector('body');
let isScrolling;
let lightContainer = document.getElementById('light-box');
let transparentMenu = document.getElementById('transparent-menu');
let footer = document.getElementById('footer');
let mc = new Hammer(body);
const sections = [
    '.main-section',
    '.experience-section',
    '.forence-section',
    '.resources-section',
    '.services-section',
    '.special-services-section',
    '.ally-section',
    '.regards-section',
    '.contact-section'
];
let signatureElement = document.querySelectorAll('.sig3 path');
const signature = {
    initialize: function () {
        let delay, length, results, speed;
        delay = 0;
        results = [];
        return [...signatureElement].forEach(function (path) {
            length = path.getTotalLength();
            previousStrokeLength = speed || 0;
            speed = length < 100 ? 20 : Math.floor(length);
            delay += previousStrokeLength + 100;
            path.style.transition = 'none';
            path.setAttribute('data-length', length);
            path.setAttribute('data-speed', speed);
            path.setAttribute('data-delay', delay);
            path.setAttribute('stroke-dashoffset', length);
            path.setAttribute('stroke-dasharray', length + ',' + length);
            results.push(path);
            return results;
        });
    },
    animate: function () {
        let results = [];
        return [...signatureElement].forEach(function (path) {
            var delay, speed;
            speed = path.getAttribute('data-speed');
            delay = path.getAttribute('data-delay');
            path.style.transition = 'stroke-dashoffset ' + speed + 'ms ' + delay + 'ms linear';
            path.setAttribute('stroke-dashoffset', '0');
            results.push(path);
            return results;
        });
    }
};

function handleLight(event) {
    event.preventDefault();
    let x, y;
    x = event.clientX || event.touches[0].clientX;
    y = event.clientY || event.touches[0].clientY - 100;

    lightContainer.style.setProperty('--cursorX', x + 'px');
    lightContainer.style.setProperty('--cursorY', y + 'px');
}

function handleScroll(event, direction) {
    if (!ActiveMenu) {
        window.clearInterval(isScrolling);
        scrolling = true;
        if (((document.body.getBoundingClientRect()).top > scrollPos)) {
            scrollDirection = direction ? direction : 'up';
        } else {
            scrollDirection = direction ? direction : 'down';
        }
        scrollPos = (document.body.getBoundingClientRect()).top;
        isScrolling = setTimeout(() => {
            scrolling = false;
            scrollingToSection = false;
        }, 500);
    }
}

function intervalScrolling() {
    if (scrolling && !scrollingToSection) {
        
        scrolling = false;
        scrollingToSection = true;
        scrollHandler();

        if (nextSection === 7) {
            signature.initialize();
            setTimeout(function () {
                return signature.animate();
            }, 500);
        }

    }
    if (!ActiveMenu) {
        scrollHandler();
    }
    
    
    
}

setInterval(intervalScrolling, 200);
// transparentMenu.addEventListener('click', () => { btnMenu.click() });
if (/Mobi|Android/i.test(navigator.userAgent)) {
    mc.get('swipe').set({ direction: Hammer.DIRECTION_VERTICAL });
    mc.on('swipeup', (event) => handleScroll(event, 'down'));
    mc.on('swipedown', (event) => handleScroll(event, 'up'));
    lightContainer.addEventListener('touchmove', handleLight);
} else {
    window.addEventListener('scroll', handleScroll);
    lightContainer.addEventListener('mousemove', handleLight);
}


