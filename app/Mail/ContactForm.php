<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ContactForm extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->mail = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('juanmiguelangarita.public@gmail.com')
                ->subject('Nuevo registro en formulario de contacto')
                ->markdown('vendor.mail.html.message')
                ->with([
                        'url' => 'https://juanmiguelangarita.com',
                        'names' => $this->mail['nombres'],
                        'lastNames' => $this->mail['apellidos'],
                        'email' => $this->mail['correo'],
                        'phone' => $this->mail['telefono'],
                        'message' => $this->mail['mensaje'],
                        'acepto' => $this->mail['acepto'],
                    ]);;
        // return $this->view('vendor.mail.text.layout');
    }


}
