<aside id="side-bar-menu-right" class="side-bar">
    <input class="tgl tgl-ios" id="btn-menu" type="checkbox" />
    <label class="tgl-btn" for="btn-menu">
        <span id="icon-menu" class='hamburger'></span>
    </label>

    <!-- <button type="button" id="btn-menud" class="btn-menu">
        
        <i id="icon-menu" class="fas fa-bars"></i>
    </button> -->
    <ul class="social-list" id="social-list-side">
        <li class="follow">
            <span>SÍGUENOS</span>
        </li>
        <li class="social-item">
            <a href="https://web.whatsapp.com/send?phone=573105110889&text="  target="_blank"><i class="fab fa-whatsapp"></i></a>
        </li>
        <li class="social-item">
            <a href="https://www.facebook.com/GIEopinion/" target="_blank"><i class="fab fa-facebook-f"></i></a>
        </li>
        <li class="social-item">
            <a href="https://twitter.com/GIEopinion" target="_blank"><i class="fab fa-twitter"></i></a>
        </li>
        <li class="social-item">
            <a href="https://www.instagram.com/GIEopinion/" target="_blank"><i class="fab fa-instagram"></i></a>
        </li>
        <li class="social-item">
            <a href="https://www.youtube.com/channel/UCed-9zL_HYyklzfL2Ljv53g?view_as=subscriber"  target="_blank">
                <i class="fab fa-youtube"></i>
            </a>
        </li>
        <div class="social-item">
            <a href="https://waze.com/ul/hd2g698e50" target="_blank">
                <i class="fab fa-waze"></i>
            </a>
        </div>
    </ul>
</aside>
<div id="menu-section" class="menu-section menu-home">
    <div class="separator">
        <div class="title">
            <h1>GRUPO DE <br>INVESTIGACIONES <br>ESPECIALIZADO</h1>
        </div>
        <p class="hashtag-text">#ProfesionalismoGIE</p>
        <span class="follow-container">
            <button class="follow">SÍGUENOS</button>
        </span>
        <div class="social-list">
            <div class="social-item">
                <a href="https://www.facebook.com/GIEopinion/">
                    <i class="fab fa-facebook-f"></i>
                </a>
            </div>
            <div class="social-item">
                <a href="https://twitter.com/GIEopinion">
                    <i class="fab fa-twitter"></i>
                </a>
            </div>
        </div>
        <div class="social-list">
            <div class="social-item">
                <a href="https://www.instagram.com/juanmiguelangarita/">
                    <i class="fab fa-instagram"></i>
                </a>
            </div>
            <div class="social-item">
                <a href="https://www.youtube.com/channel/UCed-9zL_HYyklzfL2Ljv53g?view_as=subscriber">
                    <i class="fab fa-youtube"></i>
                </a>
            </div>
        </div>
    </div>
    <!-- <div class="content"> -->
        <div class="navigation">
            <ul>
                <li><a href="#" data-text="Quienes Somos">Quienes Somos</a></li>
                <li><a href="{{ route('ser')}}" data-text="Servicios">Servicios</a></li>
                <li><a href="#" data-text="Equipo GIE">Equipo GIE</a></li>
                <li><a href="{{ route('blog')}}" data-text="Articulos">Articulos</a></li>
                <li><a href="{{ route('noticias')}}" data-text="Noticias">Noticias</a></li>
                <li><a href="{{ route('galeria')}}" data-text="Galería">Galería</a></li>
                <li><a href="{{ route('contacto')}}" data-text="Contacto">Contacto</a></li>
            </ul>
            <div class="cursor"></div>
        </div>
    <!-- </div> -->
</div>