@extends('layouts.home')

@section('title', 'Page Title')

@section('sidebar')
    @parent

    <p>This is appended to the master sidebar.</p>
@stop

@section('content')
    <div id="barba-wrapper" aria-live="polite">
            <div class="barba-container">
            <div class="scroll-sentinel" style="height: 3525.39px;"></div>
            <div class="scroll-wrap">
            <div class="scroll-content" style="transform: matrix(1, 0, 0, 1, 0, 0);">


            <section class="hero">
                <img src="{{ asset('assets/img/home.jpg') }}" style="z-index: -8;position: absolute;filter: brightness(0.2); width: 100vw; right:0px;bottom: 0px;display: block;height: 100vh;object-fit: cover;">
                <div class="hero-wrap" style="transform: matrix3d(0.997996, 0, 0.0632794, 0, 0.00313446, 0.998772, -0.0494344, 0, -0.0632017, 0.0495337, 0.996771, 0, -18.1403, 14.1962, 0, 1);">
                    <div class="container hero-container" style="transform: translate(0%, 0.608828%) translate3d(0px, 0px, 0px);">
                        <h1 style="opacity: 1;">
                            <div class="text-box">
                                <p class="text" id="funcion">ESTAMOS &nbsp; CAMBIANDO</p>
                                <p class="text-line">LA &nbsp; CRIMINALISTICA</p>
                                <p class="text">EN &nbsp; COLOMBIA&nbsp; Y&nbsp; LATINOAMÉRICA</p>
                            </div>
                        </h1>
                    </div>
                    
                    <div class="video-wrap" style="opacity: 0.9; transform: translate(10%, 0%) translate3d(0px, 0px, -400px);">
                        <video class="autoplay active" data-src-mobile="{{ asset('assets/video/Somos_Trim.mp4') }}" data-src-desktop="{{ asset('assets/video/Somos_Trim.mp4') }}" data-src-full="{{ asset('assets/video/Somos.mp4') }}" loop="" autoplay muted="" playsinline="" webkit-playsinline="" src="{{ asset('Somos_Trim.mp4') }}"></video>
                    
                        
                    </div>
                    <button type="button" class="play">
                        <span class="watch" ></span>
                        <span class="play" >
                            <svg class="icon"><use xlink:href="#play"></use></svg>
                            <!-- <span class="bg"></span> -->
                        </span>
                        <span class="text">
                            <i class="fas fa-video"></i>
                        </span>
                    </button>
                </div>
            </section>
            <section class="section experience-section">
                <div class="text-box">
                    <p class="text" data-aos="fade-up" data-aos-offset="0" data-aos-delay="50" data-aos-duration="1000"
                        data-aos-easing="ease-in-out-back" data-aos-mirror="true" data-aos-once="false">EXPERIENCIA</p>
                    <p class="text" data-aos="fade-up" data-aos-offset="0" data-aos-delay="50" data-aos-duration="1000"
                        data-aos-easing="ease-in-out-back" data-aos-mirror="true" data-aos-once="false">Y PROFESIONALISMO</p>
                </div>
                <div class="svg-container">
                    <svg viewBox="0 0 400 800" xmlns="http://w3.org/2000/svg" class="svg-first-ellipse">
                        <path class="path-ellipse" id="path-1"
                            d="M 400 100 C 300 0 250 50 200 100 C 100 200 0 400 0 550 C 0 700 200 850 400 700 "></path>
                    </svg>
                    <svg viewBox="0 0 400 800" xmlns="http://w3.org/2000/svg" class="svg-first-ellipse">
                        <path class="path-ellipse" id="path-2" d="M 400 150 C 250 200 100 100 50 200 C 0 350 100 750 400 750 ">
                        </path>
                    </svg>
                    <svg viewBox="0 0 400 800" xmlns="http://w3.org/2000/svg" class="svg-first-ellipse">
                        <path class="path-animated" id="path-3"
                            d="M 400 100 C 300 0 250 50 200 100 C 100 200 0 400 0 550 C 0 700 200 850 400 700 "></path>
                    </svg>
                    <svg viewBox="0 0 400 800" xmlns="http://w3.org/2000/svg" class="svg-first-ellipse">
                        <path class="path-animated" id="path-4" d="M 400 150 C 250 200 100 100 50 200 C 0 350 100 750 400 750 ">
                        </path>
                    </svg>
                </div>
            </section>
            <section class="section forence-section" id="forenceText">
                <div class="light-box" id="light-box">
                    <div class="transparent-menu" id="transparent-menu"></div>
                </div>
                <div class="text-box">
                    <p class="text" data-aos="fade-up" data-aos-offset="0" data-aos-delay="50" data-aos-duration="1000"
                        data-aos-easing="ease-in-out-back" data-aos-mirror="true" data-aos-once="false">
                        Adquiridos en <strong>más de 14 años de ardua labor</strong>, nos acreditan como grandes referentes en
                        Colombia y Latinoamerica,
                        en temas de Criminalística e Investigación Forense.
                    </p>
                    <img data-aos="fade-down" data-aos-offset="0" data-aos-delay="50" data-aos-duration="1000"
                        data-aos-easing="ease-in-out-back" data-aos-mirror="true" data-aos-once="false" class="img-forence"
                        src="{{ asset('assets/img/abogado.png') }}" alt="abogado">
                </div>
            </section>
            <section class="section resources-section">
                <div class="text-box">
                    <img data-aos="fade-down" data-aos-offset="0" data-aos-delay="50" data-aos-duration="1000"
                        data-aos-easing="ease-in-out-back" data-aos-mirror="true" data-aos-once="false" class="img-forence"
                        src="{{ asset('assets/img/abogado1.png') }}" alt="abogado">
                    <div class="resources-text" data-aos="fade-up" data-aos-offset="0" data-aos-delay="50"
                        data-aos-duration="1000" data-aos-easing="ease-in-out-back" data-aos-mirror="true"
                        data-aos-once="false">
                        <p class="text big-text">Y EL TALENTO</p>
                        <p class="text big-text">HUMANO</p>
                        <p class="text">E INFRAESTRUCTURA</p>
                        <P class="text">
                            Con el que contamos, nos permite ofrecer <strong>el mejor respaldo en todos los procesos jurídicos y
                                probatorios</strong> en los que se requiere la mejor resolución investigativa posible.
                        </P>
                    </div>
                </div>
            </section>
            <section class="section services-section">
                <div class="separator" data-aos="fade-right" data-aos-offset="0" data-aos-delay="50" data-aos-duration="1000"
                    data-aos-easing="ease-in-out-back" data-aos-mirror="true" data-aos-once="false">
                </div>
                <div class="separator2" data-aos="fade-right" data-aos-offset="0" data-aos-delay="60" data-aos-duration="1000"
                    data-aos-easing="ease-in-out-back" data-aos-mirror="true" data-aos-once="false">
                </div>
                <div class="content">
                    <div style="width: 100%;height: 100vh;position: absolute;overflow: hidden;">
                        <video autoplay muted loop class="backgroundVideo">
                            <source src="{{ asset('assets/video/Servicios.mp4')}}" type="video/mp4">
                        </video>
                    </div>
                    <p class="text" data-aos="fade-up" data-aos-offset="0" data-aos-delay="50" data-aos-duration="1000"
                        data-aos-easing="ease-in-out-back" data-aos-mirror="true" data-aos-once="false">NUESTROS</p>
                    <p class="text" data-aos="fade-up" data-aos-offset="0" data-aos-delay="50" data-aos-duration="1000"
                        data-aos-easing="ease-in-out-back" data-aos-mirror="true" data-aos-once="false">SERVICIOS</p>
                    <p class="text" data-aos="fade-up" data-aos-offset="0" data-aos-delay="50" data-aos-duration="1000"
                        data-aos-easing="ease-in-out-back" data-aos-mirror="true" data-aos-once="false">Ampliamente integrales,
                        le ofrecen la más grande asistencia especializada que podrá encontrar a su entera disposición.</p>
                    <div class="services-btn" data-aos="fade-up" data-aos-offset="0" data-aos-delay="50"
                        data-aos-duration="1000" data-aos-easing="ease-in-out-back" data-aos-mirror="true"
                        data-aos-once="false">
                        <a  href="../services/index.html" class="btn btn-services">CONOZCALOS AQUÍ</a>
                    </div>
                </div>
            </section>
            <section class="section special-services-section">
                <div class="content">
                    <div style="width: 100%;height: 100vh;position: absolute;overflow: hidden;">
                        <video autoplay muted loop class="backgroundVideo">
                            <source src="{{ asset('assets/video/Ciberseguridad.mp4')}}" type="video/mp4">
                        </video>
                    </div>
                    <div class="text-box" data-aos="fade-down" data-aos-offset="0" data-aos-delay="50" data-aos-duration="1000"
                        data-aos-easing="ease-in-out-back" data-aos-mirror="true" data-aos-once="false">
                        <p class="text p">Incluyendo</p>
                        <p class="text p">SERVICIOS ESPECIALES EN</p>
                        <p class="text">CIBERSEGURIDAD</p>
                        <p class="text p">Para preservar así su integridad y llevar los riesgos a su más mínima expresión.</p>
                    </div>
                </div>
                <div class="separator"></div>
            </section>
            <section class="section ally-section">
                <div class="text-box" data-aos="fade-up" data-aos-offset="0" data-aos-delay="50" data-aos-duration="1000"
                    data-aos-easing="ease-in-out-back" data-aos-mirror="true" data-aos-once="false">
                    <p class="text">SOMOS</p>
                    <p class="text">SU MEJOR ALIADO</p>
                    <p class="text">Nuestra calidad e impecable gestión, son nuestra mayor garantía.</p>
                </div>
                <div class="img-box">
                    <img src="{{ asset('assets/img/abogado3.png') }}" alt="abogado">
                </div>
            </section>
      
            <section class="section contact-section">
                
            </section>
            </div>
            </div>
            </div>        
        </div>
    
    <footer id="footer" >
        <div class="container">
            <div class="text-box" data-aos="fade-down" data-aos-offset="0" data-aos-delay="50" data-aos-duration="1000"
                data-aos-easing="ease-in-out-back" data-aos-mirror="true" data-aos-once="false">
                <h2 class="title">GRUPO DE INVESTIGACIONES ESPECIALIZADO</h2>
                <p class="text">Profesionales en Investigación críminal y Ciencias Forenses, asesoría integral y
                    orientación.</p>
            </div>
            <div class="text-box" data-aos="fade-down" data-aos-offset="0" data-aos-delay="50" data-aos-duration="1000"
                data-aos-easing="ease-in-out-back" data-aos-mirror="true" data-aos-once="false">
                <h2 class="title">SEDE CENTRO</h2>
                <p class="text"><i class="fas fa-building"></i> Av Jimenez # 8A - 77, Edif. Seguros Universal, Bogotá
                </p>
                <p class="text">204 - Oficina Administrativa y contable</p>
                <p class="text">301 - Laboratorio de Ciberseguridad e informática Forense</p>
                <p class="text">303 - Coordinación de Investigaciones</p>
                <p class="text"><i class="fas fa-building"></i> Carrera 9 # 13-36 Edif. Colombia, Bogotá</p>
                <p class="text">602 - Capacitaciones GIE</p>
            </div>
            <div class="text-box" data-aos="fade-up" data-aos-offset="0" data-aos-delay="50" data-aos-duration="1000"
                data-aos-easing="ease-in-out-back" data-aos-mirror="true" data-aos-once="false">
                <h2 class="title">CONTACTO</h2>
                <p class="text"><i class="fas fa-envelope"></i> juanmiguelangarita@gmail.com</p>
                <p class="text"><i class="fas fa-phone-alt"></i> (+57) 310 511 0889</p>
                <div class="social-box">
                    <span class="follow">SÍGUENOS</span>
                    <a href="#" class="social-item"><i class="fab fa-facebook-f"></i></a>
                    <a href="#" class="social-item"><i class="fab fa-twitter"></i></a>
                    <a href="#" class="social-item"><i class="fab fa-instagram"></i></a>
                </div>
            </div>
        </div>
        <span class="copyright">Copyright <i class="far fa-copyright"></i> 2019 Juan Miguel Angarita <i
                class="far fa-registered"></i>. All rights reserved.</span>
        <span class="author">Diseñado Por HumanCode</span>
    </footer>
    
@stop

@section('javascript')
    <script async="" src="{{ asset('assets/Simple _Creature_files/main.min.1575465493.js.descarga') }}"></script>

@stop
