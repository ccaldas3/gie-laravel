@extends('blog.layouts.app')

@section('content')
    @component('blog.particals.jumbotron')
        <h3>{{ config('blog.article.title') }}</h3>
        <img src="{{asset('assets/img/Logo-blanco.png')}}" alt="Logo">

        <h6><a href="{{ config('blog.article.description') }}" title="Pagina-principal">{{ config('blog.article.description') }}</a></h6>
    @endcomponent

    @include('blog.widgets.article')

    {{ $articles->links('blog.pagination.default') }}

@endsection