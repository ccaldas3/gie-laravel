<div class="card card-info">
    <div class="card-header">{{ Lang::get('Settings') }}</div>

    <div class="list-group list-group-flush">
        <a href="{{ url('blog/setting') }}" class="list-group-item {{ isActive('setting.index') }}">
            <i class="fas fa-key"></i>{{ Lang::get('Account Setting') }}
        </a>
        @if(config('blog.mail_notification'))
        <a href="{{ url('blog/setting/notification') }}" class="list-group-item {{ isActive('setting.notification') }}">
            <i class="far fa-bell"></i>{{ Lang::get('Notification Setting') }}
        </a>
        @endif
        <a href="{{ url('blog/setting/binding') }}" class="list-group-item {{ isActive('setting.binding') }}">
            <i class="fas fa-lock"></i>{{ Lang::get('Account Binding') }}
        </a>
    </div>
</div>