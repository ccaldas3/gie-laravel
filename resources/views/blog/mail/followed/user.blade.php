@component('mail::message')
**{{ Lang::get('Dear') }} {{ $username }}：**

{{ $message }}

<a href="{{ $url }}" target="_blank">{{ Lang::get('View') }}</a>

Thanks,<br>
{{ config('app.name') }}
@endcomponent
