@component('mail::message')
**{{ Lang::get('Dear') }} {{ $username }}：**

{{ $message }}

{{ $content }}

@component('mail::button', ['url' => $url])
{{ Lang::get('View') }}
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
