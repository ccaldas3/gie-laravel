@extends('blog.layouts.app')

@section('content')
    @component('blog.particals.jumbotron')
        <h4>{{ request()->get('q') }}</h4>

        <h6>what you want to search.</h6>
    @endcomponent

    @include('blog.widgets.article')

@endsection