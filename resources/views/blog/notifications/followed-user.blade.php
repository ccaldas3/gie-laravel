<?php
$user = App\User::find($notification->data['id']);
?>

<li :class="'{{ empty($notification->read_at) }}' ? 'bold' : ''">
    @if ($user)
        <a class="text-info" href="{{ url('user', [ 'username' => $user->name ]) }}">{{ $user->name }}</a> {{ Lang::get('Followed') }}
    @else
        <s>{{ Lang::get('User') }} {{ Lang::get('Deleted') }}</s>
    @endif
</li>
