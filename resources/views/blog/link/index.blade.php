@extends('blog.layouts.app')

@section('content')
    @component('blog.particals.jumbotron')
        <h3>{{ Lang::get('Links') }}</h3>
    @endcomponent

    <div class="container">
        <div class="row">
            <div class="col-md-6 offset-md-3">
                <ul class="links text-center">
                    @forelse($links as $link)
                        <li><a href="{{ $link->link }}">{{ $link->name }}</a></li>
                    @empty
                        <li><h3>{{ Lang::get('Nothing') }}</h3></li>
                    @endforelse
                </ul>
            </div>
        </div>
    </div>
@endsection
