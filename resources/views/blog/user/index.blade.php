@extends('blog.layouts.app')

@section('content')
    @include('blog.user.particals.info')
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div class="card card-default">
                    <div class="card-header">{{ Lang::get('Recent Discussions') }}</div>

                    @include('blog.user.particals.discussions')

                </div>
            </div>
            <div class="col-md-6">
                <div class="card card-default">
                    <div class="card-header">{{ Lang::get('Recent Comments') }}</div>

                    @include('blog.user.particals.comments')

                </div>
            </div>
        </div>
    </div>
@endsection