@extends('blog.layouts.app')

@section('content')
    @include('blog.user.particals.info')

    <div class="container">
        <div class="row">
            <div class="col-md-8 offset-md-2">
                <div class="card card-default">
                    <div class="card-header">{{ Lang::get('Your Discussions') }} ( {{ $discussions->count() }} )</div>

                    @include('blog.user.particals.discussions')

                </div>
            </div>
        </div>
    </div>
@endsection