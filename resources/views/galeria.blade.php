@extends('layouts.home')

@section('title', 'Page Title')

@section('sidebar')
    @parent

    <p>This is appended to the master sidebar.</p>
@stop

@section('content')

	<div id="light-box">
		
	</div>

<br><br><br>
<h2 class="text-center">Galeria</h2>
<br><br><br>
<div class="container-fluid">
  <div class="row instagram-feed1" id="instagram-feed1">
  	
  	
  </div>
</div>

<script
        src="https://code.jquery.com/jquery-3.4.1.min.js"
        integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
        crossorigin="anonymous"></script>
{{-- <script src="{{ asset('assets/js/InstagramFeed.js')}}" ></script> --}}


<script>
    // (function(){
    //     new InstagramFeed({
    //         'username': 'humancodecorp',
    //         'container': document.getElementById("instagram-feed1"),
    //         'display_profile': true,
    //         'display_biography': true,
    //         'display_gallery': true,
    //         'callback': null,
    //         'styling': true,
    //         'items': 8,
    //         'items_per_row': 4,
    //         'margin': 1,
    //         'lazy_load': true,
    //         'on_error': console.error
    //     });
    // })();
    
    var image_size = 640;
	var lazy_load = false;

    var image_sizes = {
        "150": 0,
        "240": 1,
        "320": 2,
        "480": 3,
        "640": 4
    };

    var styles = {
                    'profile_container': "",
                    'profile_image': "",
                    'profile_name': "",
                    'profile_biography': "",
                    'gallery_image': ""
                };

	console.log("hola desde galeria");

	var USER_AGENT = 'Mozilla/5.0 (Linux; Android 6.0.1; Moto G (4)) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.89 Mobile Safari/537.36';
	headers3 = {
            "Access-Control-Allow-Headers" : "Content-Type",
            "Access-Control-Allow-Origin": "https://github.com",
            "Access-Control-Allow-Methods": "OPTIONS,POST,GET",
            "userAgent": USER_AGENT
        };
     var username = 'gieopinion';
     var data = {};
	function getPhotosFromInstagram(){
		// var url = this.is_tag ? this.options.host + "explore/tags/" + this.options.tag + "/" : this.options.host + this.options.username + "/",
  //               xhr = new XMLHttpRequest();
  		var url = 'https://www.instagram.com/' + username + "/",
                xhr = new XMLHttpRequest();
            var _this = this;
            var datosss = {};
            xhr.onload = function(e) {
                if (xhr.readyState === 4) {
                    if (xhr.status === 200) {
                        try{
                            var data = xhr.responseText.split("window._sharedData = ")[1].split("<\/script>")[0];
                        }catch(error){
                            console.log("InstagramFeed: It looks like the profile you are trying to fetch is age restricted. See https://github.com/jsanahuja/InstagramFeed/issues/26", 2);
                            return;
                        }
                        data = JSON.parse(data.substr(0, data.length - 1));
                        data = data.entry_data.ProfilePage || data.entry_data.TagPage;
                        if(typeof data === "undefined"){
                            console.log("InstagramFeed: It looks like YOUR network has been temporary banned because of too many requests. See https://github.com/jsanahuja/jquery.instagramFeed/issues/25", 4);
                            return;
                        }
                        data = data[0].graphql.user || data[0].graphql.hashtag;
                        datosss = data;
                        // console.log(data);
                        // callback(data, _this);
                    } else {
                        console.log("InstagramFeed: Unable to fetch the given user/tag. Instagram responded with the status code: " + xhr.statusText, 5);
                    }
                }
            };
            
            

            console.log("Acá esta la data");
            xhr.open("GET", url, false);
            xhr.send(null);
            console.log(xhr.responseText);
            // console.log(datosss);
            data = datosss;
            html();
            return datos;
	}

	function html(){


		var image_index = typeof image_sizes[image_size] !== "undefined" ? image_sizes[image_size] : image_sizes[640];

                if (typeof data.is_private !== "undefined" && data.is_private === true) {
                    html += "<p class='instagram_private'><strong>This profile is private</strong></p>";
                } else {
                	console.log("conteo img:", data.edge_owner_to_timeline_media.edges.length, data.edge_owner_to_timeline_media);
				var imgs = (data.edge_owner_to_timeline_media || data.edge_hashtag_to_media).edges;
		            max = imgs.length;

		            html = "";
		            

		            for (var i = 0; i < max; i++) {

		            	html += "<div class='col-xs-3 col-md-3'><div class='instagram_gallery'>";

		                var url = "https://www.instagram.com/p/" + imgs[i].node.shortcode,
		                    image, type_resource,
		                    caption = escape_string(parse_caption(imgs[i], data));

		                switch (imgs[i].node.__typename) {
		                    case "GraphSidecar":
		                        type_resource = "sidecar"
		                        image = imgs[i].node.thumbnail_resources[image_index].src;
		                        break;
		                    case "GraphVideo":
		                        type_resource = "video";
		                        image = imgs[i].node.thumbnail_src
		                        break;
		                    default:
		                        type_resource = "image";
		                        image = imgs[i].node.thumbnail_resources[image_index].src;
		                }

		                if (this.is_tag) data.username = '';
		                html += "<a href='" + url + "' class='instagram-" + type_resource + "' title='" + caption + "' rel='noopener' target='_blank'>";
		                html += "<img" + (lazy_load ? " loading='lazy'" : '')  + " src='" + image + "' alt='" + caption + "'" + styles.gallery_image + " />";
		                html += "</a>";
		                html += "</div></div>";
		            }

		            
		        }

            $( ".instagram-feed1" ).append( html );
	}
	function escape_string(str){
        return str.replace(/[&<>"'`=\/]/g, function (char) {
            return escape_map[char];
        });
    }

	parse_caption = function(igobj, data) {
            if (
                typeof igobj.node.edge_media_to_caption.edges[0] !== "undefined" && 
                typeof igobj.node.edge_media_to_caption.edges[0].node !== "undefined" && 
                typeof igobj.node.edge_media_to_caption.edges[0].node.text !== "undefined" && 
                igobj.node.edge_media_to_caption.edges[0].node.text !== null
            ) {
                return igobj.node.edge_media_to_caption.edges[0].node.text;
            }

            if (
                typeof igobj.node.title !== "undefined" &&
                igobj.node.title !== null &&
                igobj.node.title.length != 0
            ) {
                return igobj.node.title;
            }

            if (
                typeof igobj.node.accessibility_caption !== "undefined" &&
                igobj.node.accessibility_caption !== null &&
                igobj.node.accessibility_caption.length != 0
            ) {
                return igobj.node.accessibility_caption;
            }
            return (this.is_tag ? data.name : data.username) + " image ";
        }

	function mostrarData(){
		console.log(datos);
	}
	$( "#btn-muestradata" ).click(function() {
	  mostrarData();
	});
	  

	    console.log(getPhotosFromInstagram());
</script>


@stop