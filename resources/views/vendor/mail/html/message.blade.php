@component('mail::layout')
{{-- Header --}}
@slot('header')
@component('mail::header', ['url' => config('app.url')])
{{ config('app.name') }}
@endcomponent
@endslot

{{-- Body --}}

<table class="panel" width="100%" cellpadding="0" cellspacing="0" role="presentation">
<tr>
<td class="panel-content">
<table width="100%" cellpadding="0" cellspacing="0" role="presentation">
<tr>
<td class="panel-item">
	Nombres: {{$names}}
</td>
</tr>
<tr>
<td class="panel-item">
	Apellidos: {{$lastNames}}
</td>
</tr>
<tr>
<td class="panel-item">
	email: {{$email}}
</td>
</tr>
<tr>
<td class="panel-item">
	Teléfono: {{$phone}}
</td>
</tr>
<tr>
<td class="panel-item">
	Mensaje: {{$message}}
</td>
</tr>
<tr>
<td class="panel-item">
	¿Acepto terminos y condiciones: {{$acepto}}
</td>
</tr>

</table>
</td>
</tr>
</table>

{{-- {{ $slot }} --}}

{{-- Subcopy --}}
@isset($subcopy)
@slot('subcopy')
@component('mail::subcopy')
{{ $subcopy }}
@endcomponent
@endslot
@endisset

{{-- Footer --}}
@slot('footer')
@component('mail::footer')
© {{ date('Y') }} {{ config('app.name') }}. @lang('All rights reserved.')
@endcomponent
@endslot
@endcomponent
