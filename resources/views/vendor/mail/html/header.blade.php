<tr>
<td class="header">
<a href="{{ $url }}" style="display: inline-block;">
@if (trim($slot) === 'Laravel')
<img src="https://juanmiguelangarita.com/wp-content/uploads/2019/09/Logo_Blanco-05-1024x597.png" class="logo" alt="Gie logo">
@else
{{ $slot }}
@endif
</a>
</td>
</tr>
