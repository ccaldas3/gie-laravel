@extends('layouts.home')

@section('title', 'Noticias')


@section('styles')
    
    <style type="text/css" media="screen">
        .iframe-twitts{
            /*position: static; */
            /*visibility: visible; */
            /*width: 550px; */
            height: 38rem; 
            /*display: block; */
            /*flex-grow: 1;*/
            transition: all .2s ease-in-out;
        }
        .iframe-twitts:hover{
            transform: scale(1.05);    
        }
        
    </style>

@stop

@section('content')

	

<br><br><br>
<h2 class="text-center">Noticias</h2>
<br><br><br>
<div class="container-fluid">
  <div class="row" >
  	 
        @foreach($twitts as $twitt)
        <div class="col-md-3">
            <iframe id="twitter-widget-0" class="iframe-twitts" scrolling="no" frameborder="0" allowtransparency="true" allowfullscreen="true"  title="Twitter Tweet" src="https://platform.twitter.com/embed/index.html?dnt=false&amp;embedId=twitter-widget-0&amp;frame=false&amp;hideCard=false&amp;hideThread=false&amp;id={{$twitt->id}}&amp;lang=en&amp;origin=https%3A%2F%2Fdeveloper.twitter.com%2Fen%2Fdocs%2Ftwitter-for-websites%2Fembedded-tweets%2Foverview&amp;theme=light&amp;widgetsVersion=223fc1c4%3A1596143124634&amp;width=550px" data-tweet-id="{{$twitt->id}}">

            </iframe>    
        </div>
        @endforeach
     
  </div>
</div>

<script
        src="https://code.jquery.com/jquery-3.4.1.min.js"
        integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
        crossorigin="anonymous"></script>


@stop