<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="author" content="Human Code Corp">
    <title>Grupo de Investigaciones Especializado - Juan Miguel Angarita @yield('title')</title>
    
    <meta name="theme-color" content="#000">
    <link rel="icon" type="image/png" href="{{ asset('icon (2).png') }}">


    <link rel="stylesheet" href="{{ asset('assets/css/home.css') }}">
    
    <link rel="stylesheet" href="{{ asset('assets/css/aos.css' ) }}">
    <link rel="stylesheet" href="{{ asset('assets/css/global.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/main.css') }}">

    <link rel="stylesheet" href="{{ asset('assets/css/menu-principal.css') }}">

    {{-- <link href="https://unpkg.com/tailwindcss@^1.0/dist/tailwind.min.css" rel="stylesheet"> --}}

    
    <script src="https://kit.fontawesome.com/a0e13c3407.js" crossorigin="anonymous"></script>
    <style>
            section.hero h1>div {
                 overflow: unset; 
            }
            section.hero h1 {
                font-size: 50px;
            }
            .hero-container h1 .text-box .text {
                margin: 0;
            }

            .hero-container h1 .text-box .text-line {
                color: transparent;
                font-size: 1.3em;
                -webkit-text-stroke-width: 2px;
                -webkit-text-stroke-color: #F1DA00;
            }
            body{
                touch-action: pan-y !important;
                user-select: pan-y !important;
                -webkit-user-drag: pan-y !important;
            }


            .instagram-feed1 img:hover{
                filter: brightness(0.2);    
            }
            .instagram-feed1 .col-sm-3{
                margin: 10px 0px;    
            }

        </style>
        @yield('styles')
    <!-- <script src="main.min.js" type="text/javascript"></script> -->
    <!-- <script  src="animacion.js"></script> -->
</head>


<body onscroll="scroll()">
    <ul class="transition">
      <li></li>
      <li></li>
      <li></li>
      <li></li>
      <li></li>
    </ul>
    
    <div class="cargando" id="cargando" style="display: none">
        <div  id="fondo-cargando">      
        </div>
        
        <div id="caja-contador" style="
            width: 100%;
            display: flex;
            justify-content: center;
            top: 50vh;
            position: relative;
            /*margin-left: 11vw;*/
            align-items: center;">
            <div  id="contador"></div>
            <div class="txt-cargando" id="cargado" style="display: none">
                Bienvenido a la firma de Investigadores más prestigiosa de Colombia y Latinoamerica
            </div>  
        </div>
    </div>
    <header class="header">
        <a href="{{route('home')}}" title="Home">
            <img id="logo" class="logo logo-img" src="{{ asset('assets/img/Logo-blanco.png')}}" alt="Logo">
            {{-- <img id="logo" class="logo logo-img" alt="Logo"> --}}
        </a>
        <i class="fas fa-times equis logo" ></i>
    </header>
    @include('menu')


    @yield('content')
    <script src="{{ asset('assets/js/hammer.min.js') }}"></script>
    <script src="{{ asset('assets/js/aos.js') }}"></script>
    <script src="{{ asset('assets/js/global.js') }}"></script>
    <script src="{{ asset('js/jquery-3.5.1.min.js') }}"></script>
    
    {{-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.js" ></script> --}}
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.9.1/jquery.min.js" type="text/javascript" ></script>

    
    <script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/2.1.3/TweenMax.min.js" ></script>
    <script src="{{ asset('assets/js/home.js')}}"></script>
    <script src="{{ asset('assets/js/cargando.js')}}"></script>
    

    <script src="https://unpkg.com/@barba/core"></script>
    <!-- GSAP for animation -->
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/gsap/3.2.4/gsap.min.js"></script>
    <script src="{{ asset('assets/js/barbaFunciones.js' )}}"></script>
    {{-- Menu --}}
    <script src="{{ asset('assets/js/menu-principal.js' )}}"></script>
    <progress id="health" value="60" max="100"></progress>
    <button>change progress value</button>
    <script>
        document.addEventListener("DOMContentLoaded", function(event) {
          /* DOM is ready, so we can query for its elements */
          var dragonHealth = document.getElementById("health").value;
          console.log("dragonHealth", dragonHealth);
          
          /*additional code for comment*/
          document.querySelector('button').addEventListener("click", function(event){
            document.getElementById("health").value += 5;
          });
        })
        $(document).ready(function() {
            // Muestra el botón de play
            setTimeout($('.watch').fadeTo("slow", 1), 2000);
            setTimeout($('.play').fadeTo("slow", 1), 2000);
            setTimeout($('.text').fadeTo("slow", 1), 2000);



            var request = null;

            var mouse = {
                x: 0,
                y: 0
              };
              var cx = window.innerWidth / 2;
              var cy = window.innerHeight / 2;

              $('section.hero').mousemove(function(event) {
                console.log("update");
                console.log("movimiento");
                mouse.x = event.pageX;
                mouse.y = event.pageY;

                cancelAnimationFrame(request);
                request = requestAnimationFrame(update);
              });

            function update() {
                console.log("update");
                dx = mouse.x - cx;
                dy = mouse.y - cy;

                tiltx = (dy / cy);
                tilty = -(dx / cx);
                radius = Math.sqrt(Math.pow(tiltx, 2) + Math.pow(tilty, 2));
                degree = (radius * 5);
                TweenLite.to(".hero-wrap", 1, {
                  transform: 'rotate3d(' + (-tiltx) + ', ' + (-tilty) + ', 0, ' + degree + 'deg)',
                  ease: Power2.easeOut
                });
            }

             /// Funciòn para movil 
          
            var touch = {
                x: 0,
                y: 0
            };

            window.addEventListener("deviceorientation",function(event) {
                alpha = Math.round(event.alpha);
                beta = Math.round(event.beta);
                gamma = Math.round(event.gamma);

                touch.x = cx * ((gamma + 100)/100);
                touch.y = cy * ((beta + 50)/100);

                function updateTouch() {

                    dx = touch.x - cx;
                    dy = touch.y - cy;

                    tiltx = (dy / cy);
                    tilty = -(dx / cx);
                    radius = Math.sqrt(Math.pow(tiltx, 2) + Math.pow(tilty, 2));
                    degree = (radius * 50);
                    TweenLite.to(".hero-wrap", 1, {
                      transform: 'rotate3d(' + (-tiltx) + ', ' + (-tilty) + ', 0, ' + degree + 'deg)',
                      ease: Power2.easeOut
                    });
                  }
                  updateTouch();

            }, true);

              $(window).resize(function() {
                cx = window.innerWidth / 2;
                cy = window.innerHeight / 2;
              });

        });
    </script>
    @yield('javascript')
    <script>
        const cursor =  document.querySelector(".cursor");
        document.addEventListener('mousemove', (e) => {
            cursor.style.left = e.pageX + 'px';
            cursor.style.top = e.pageY + 'px';
        })
    </script>
</body>

</html>