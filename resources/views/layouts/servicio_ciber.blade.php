<!DOCTYPE html>
<html lang="es" ng-app="phonecatApp">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Grupo de Investigaciones Especializado - Juan Miguel Angarita</title>
    <link rel="stylesheet" href="{{ asset('assets/css/aos.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/global.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/CiberService.css')}}">
    <link rel="icon" href="https://juanmiguelangarita.com/wp-content/uploads/2019/09/cropped-LOGO_FondoBlanco_Mesa-de-trabajo-1-32x32.png" sizes="32x32">
    <link rel="icon" href="https://juanmiguelangarita.com/wp-content/uploads/2019/09/cropped-LOGO_FondoBlanco_Mesa-de-trabajo-1-192x192.png" sizes="192x192">
    <link rel="apple-touch-icon-precomposed" href="https://juanmiguelangarita.com/wp-content/uploads/2019/09/cropped-LOGO_FondoBlanco_Mesa-de-trabajo-1-180x180.png">
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/flexboxgrid/6.3.1/flexboxgrid.css">
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/flexboxgrid/6.3.1/flexboxgrid.min.css">
    <!-- Link Swiper's CSS -->
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.5.1/css/swiper.min.css">
    
    <script src="https://kit.fontawesome.com/a0e13c3407.js" crossorigin="anonymous"></script>

    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/cyberbuttons.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/menu-circular.css') }}">


    <link rel="stylesheet" href="{{ asset('assets/css/menu-principal.css') }}">
</head>


    <body class="" cz-shortcut-listen="true" style="background-color: #fff" >
        @include('menu')
        @include('menu-servicios')


    @yield('content')
    <!-- <header class="header">
        <img class="logo" src="../assets/img/logo4.png" alt="Logo">
    </header> -->
    
   {{-- <script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js'></script> --}}
    {{-- <script src='{{ asset('assets/css/script2.js')}}'></script> --}}
    <script src='{{ asset('assets/js/cyberButtons.js')}}' async defer></script>

    <script src='{{ asset('assets/particles.js-master/particles.js')}}' async defer></script>
    <script src='{{ asset('assets/particles.js-master/demo/js/app.js')}}'></script>


    <script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/2.1.3/TweenMax.min.js" async defer ></script>
    
    <script>
        const cursor =  document.querySelector(".cursor");
        document.addEventListener('mousemove', (e) => {
            cursor.style.left = e.pageX + 'px';
            cursor.style.top = e.pageY + 'px';
        })
    </script>

    <script>

        function aparecerMenuCircular(){
            TweenMax.to('.grain', 1, { autoAlpha:1 } );
            TweenMax.to('.intro', 1, { autoAlpha:1 } );
        }

        $( "#btn_directrices" ).click(function() {
            console.log("btn-directrices");
            TweenMax.to('.game', 1, { scale: 1.9, x:'20%', y:'-10%', autoAlpha:0 } );
            aparecerMenuCircular();
            // TweenMax.to('.game', 1, { scale: 1.5, x:'20%', autoAlpha:0 } );
        })
        $( "#btn_derecho" ).click(function() {
            console.log("btn-btn_derecho");
            TweenMax.to('.game', 1, { scale: 1.9, autoAlpha:0 } );
            aparecerMenuCircular();
            // TweenMax.to('.game', 1, { scale: 1.5, x:'20%', autoAlpha:0 } );
        })
        $( "#btn_investigacion" ).click(function() {
            console.log("btn_investigacion");
            TweenMax.to('.game', 1, { scale: 1.9, x:'-20%', y:'-10%', autoAlpha:0 } );
            aparecerMenuCircular();
            // TweenMax.to('.game', 1, { scale: 1.5, x:'20%', autoAlpha:0 } );
        })


        
    </script>
    
</body>
</html>