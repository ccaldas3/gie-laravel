<!DOCTYPE html>

<html style="overflow-y: scroll;"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<title>Grupo de Investigaciones Especializado - Juan Miguel Angarita.</title>

<meta property="og:url" content="https://juanmiguelangarita.com">
<meta property="type" content="website">
<meta property="title" content="Grupo de INvestigaciones Especializado - Juan Miguel Angarita.">
<meta property="description" content="">
<link rel="icon" href="https://juanmiguelangarita.com/wp-content/uploads/2019/09/cropped-LOGO_FondoBlanco_Mesa-de-trabajo-1-32x32.png" sizes="32x32">
<link rel="icon" href="https://juanmiguelangarita.com/wp-content/uploads/2019/09/cropped-LOGO_FondoBlanco_Mesa-de-trabajo-1-192x192.png" sizes="192x192">
<link rel="apple-touch-icon-precomposed" href="https://juanmiguelangarita.com/wp-content/uploads/2019/09/cropped-LOGO_FondoBlanco_Mesa-de-trabajo-1-180x180.png">
<title>Grupo de Investigaciones Especializado - Juan Miguel Angarita</title>
<link rel="stylesheet" href="{{ asset('assets/css/home.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/aos.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/global.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/main.css') }}">

    <link rel="stylesheet" href="{{ asset('assets/css/menu-principal.css') }}">
<script src="https://kit.fontawesome.com/a0e13c3407.js" crossorigin="anonymous"></script>
    

<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1">


{{-- <link rel="stylesheet" href="{{ asset('assets/digitaldesigner._files/swiper.css')}}" type="text/css" media="screen"> --}}
<link rel="stylesheet" href="{{ asset('assets/digitaldesigner._files/style.css') }}" type="text/css" media="screen">
{{-- <link rel="stylesheet" href="{{ asset('assets/digitaldesigner._files/page.css') }}" type="text/css" media="screen"> --}}

<script type="text/javascript" async="" src="{{ asset('assets/digitaldesigner._files/analytics.js.descarga') }}"></script><script src="{{ asset('assets/digitaldesigner._files/TweenMax.min.js.descarga')}}"></script>
<script src="{{ asset('assets/digitaldesigner._files/jquery.min.js.descarga')}}"></script>


<script type="text/javascript" src="{{ asset('assets/digitaldesigner._files/morph.js.descarga') }}"></script>
<script type="text/javascript" src="{{ asset('assets/digitaldesigner._files/ScrollToPlugin.js.descarga') }}"></script>
<script type="text/javascript" src="{{ asset('assets/digitaldesigner._files/CustomEase.js.descarga') }}"></script>
<script type="text/javascript" src="{{ asset('assets/digitaldesigner._files/swiper.min.js.descarga') }}"></script>
<script type="text/javascript" src="{{ asset('assets/digitaldesigner._files/jquery.touchSwipe.min.js.descarga') }}"></script>
<!-- <script type="text/javascript" src="js/lethargy.min.js"></script> -->
<script type="text/javascript" src="{{ asset('assets/digitaldesigner._files/js.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/digitaldesigner._files/menu.js.descarga') }}"></script>
<script type="text/javascript" src="{{ asset('assets/digitaldesigner._files/sPreloader.js.descarga') }}"></script>
<link href="{{ asset('assets/digitaldesigner._files/css') }}" rel="stylesheet">


<style>
    body{
        touch-action: pan-y !important;
        user-select: pan-y !important;
        -webkit-user-drag: pan-y !important;
    }
</style>
<script type="text/javascript">
 begin=0;
 f="i";
</script>


</head>

<body class="" cz-shortcut-listen="true" style="background-color: #fff">

	@include('menu')


    @yield('content')
    <script>
        const cursor =  document.querySelector(".cursor");
        document.addEventListener('mousemove', (e) => {
            cursor.style.left = e.pageX + 'px';
            cursor.style.top = e.pageY + 'px';
        })
    </script>

<script type="text/javascript">$('body').jpreLoader();</script>

<script src="{{ asset('assets/js/aos.js') }}"></script>
 <script src="{{ asset('assets/js/global.js') }}"></script>
 <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.js" ></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/2.1.3/TweenMax.min.js" ></script>
 <script src="{{ asset('assets/js/cargando.js')}}"></script>
</body><div id="ci-extension-div"></div></html>