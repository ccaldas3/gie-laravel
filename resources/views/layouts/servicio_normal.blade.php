<!DOCTYPE html>
<html lang="es" ng-app="phonecatApp">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Grupo de Investigaciones Especializado - Juan Miguel Angarita</title>
    <link rel="stylesheet" href="{{ asset('assets/css/menu-principal.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/aos.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/global.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/service.css')}}">

    <link rel="icon" href="https://juanmiguelangarita.com/wp-content/uploads/2019/09/cropped-LOGO_FondoBlanco_Mesa-de-trabajo-1-32x32.png" sizes="32x32">
    <link rel="icon" href="https://juanmiguelangarita.com/wp-content/uploads/2019/09/cropped-LOGO_FondoBlanco_Mesa-de-trabajo-1-192x192.png" sizes="192x192">
    <link rel="apple-touch-icon-precomposed" href="https://juanmiguelangarita.com/wp-content/uploads/2019/09/cropped-LOGO_FondoBlanco_Mesa-de-trabajo-1-180x180.png">
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/flexboxgrid/6.3.1/flexboxgrid.css">
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/flexboxgrid/6.3.1/flexboxgrid.min.css">
    <!-- Link Swiper's CSS -->
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.5.1/css/swiper.min.css">

    
    <script src="https://kit.fontawesome.com/a0e13c3407.js" crossorigin="anonymous"></script>

    @yield('styles')

</head>
<body>
    @include('menu')

    @include('menu-servicios')


    @yield('content')



 <footer class="footer">
        <span class="copyright">Copyright <i class="far fa-copyright"></i> 2019 Juan Miguel Angarita <i class="far fa-registered"></i>. All rights reserved.</span>
        <span class="author">Diseñado Por HumanCode</span>
    </footer>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.js" ></script>
    <script src="{{ asset('assets/js/aos.js')}}"></script>
    <script src="{{ asset('assets/js/global.js') }}"></script>
    <script src="{{ asset('assets/js/service.js')}}"></script>
    <!-- Barba js -->
    
    <!-- GSAP for animation -->
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/gsap/3.2.4/gsap.min.js"></script>

    <!-- Swiper -->
     
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.5.1/js/swiper.js"></script>

    <script src="https://unpkg.com/scroll-out/dist/scroll-out.min.js"></script>
    <script src="https://unpkg.com/angular/angular.js"></script>
    <script src="{{ asset('assets/js/app.js')}}"></script>
    <script>
        // Boton buscar
        
        const input = document.getElementById("search-input");
        const searchBtn = document.getElementById("search-btn");

        const expand = () => {
          searchBtn.classList.toggle("close");
          input.classList.toggle("square");
        };

        searchBtn.addEventListener("click", expand);

 
        //Filter angular
        
        var $injector = angular.injector(['ng']);
        var $filter = $injector.get("$filter");
        var obj = {a:1};
        console.log($filter("json")(obj));
        
        // menu
        var menuSactive = false;
        let menuSectionServices = document.getElementById('menu-section-services');
        let menuTo = document.getElementById('menu-section-services');
        let btnMenuService =  document.getElementById("btn-menu-service");
        let sideBarMenuRight =  document.getElementById("side-bar-menu-right");
        
        menuSectionServices
        function showMenuServices(){
            console.log("menuSactive", menuSactive);
            if(menuSactive){
                menuSactive = !menuSactive;
                menuSectionServices.classList.add('isVisible');
                btnMenuService.classList.add('isActive');
                sideBarMenuRight.style.display="none";
            }else{
                menuSactive = !menuSactive
                menuSectionServices.classList.remove('isVisible');
                btnMenuService.classList.remove('isActive');
                sideBarMenuRight.style.display="block";
            }
            

        }

        document.getElementById("btn-menu-service").addEventListener("click", showMenuServices);


        // swiper
        var swiper = new Swiper('#swiper-container-1', {
          speed: 1500,
          slidesPerView: 4,
          centeredSlides: true,
          spaceBetween: 30,
          grabCursor: true,
          autoplay: {
            delay: 3000,
            
          },
          
        });
        var swiper = new Swiper('#swiper-container-2', {
          speed: 1500,
          slidesPerView: 4,
          centeredSlides: true,
          spaceBetween: 30,
          grabCursor: true,
          autoplay: {
            delay: 3000,
            
          },
        });
        var swiper = new Swiper('#swiper-container-3', {
          speed: 1500,
          slidesPerView: 8,
          // centeredSlides: true,
          spaceBetween: 30,
          grabCursor: true,
          autoplay: {
            delay: 2000,
            
          },
          // effect: 'fade',
        });
        var swiper = new Swiper('#swiper-container-4', {
          speed: 1500,
          slidesPerView: 3,
          // centeredSlides: true,
          spaceBetween: 30,
          grabCursor: true,
          autoplay: {
            delay: 2000,
            
          },
          // effect: 'fade',
        });
        var swiper = new Swiper('#swiper-container-5', {
          speed: 1500,
          slidesPerView: 4,
          // centeredSlides: true,
          spaceBetween: 30,
          grabCursor: true,
          autoplay: {
            delay: 2000,
            
          },
          // effect: 'fade',
        });
        var swiper = new Swiper('#swiper-container-6', {
          speed: 1500,
          slidesPerView: 4,
          // centeredSlides: true,
          spaceBetween: 30,
          grabCursor: true,
          autoplay: {
            delay: 2000,
            
          },
          // effect: 'fade',
        });
        // Lazy images
        
         if ('loading' in HTMLImageElement.prototype) {
            // body... 
            console.log("1 loading");
            const images = document.querySelectorAll('img[loading="lazy"]')
            images.forEach(img => {
                img.src = img.dataset.src;
            })
            
        }else{
            console.log("2 loading");
            const script = document.createElement('script')
            script.src = 'https://cdnjs.cloudflare.com/ajax/libs/lazysizes/4.1.8/lazysizes.min.js';
            document.body.appendChild(script);
            const images = document.querySelectorAll('img[loading="lazy"]')
            images.forEach(img => {
                img.src = img.dataset.src;
            })
        }



        ScrollOut({
            targets: 'video, .fadingEffect',
            threshold: .2
        });

        // grab elements as array, rather than as NodeList
        var elements = document.querySelectorAll("video");
        elements = Array.prototype.slice.call(elements);

        // and then make each element do something on scroll
        elements.forEach(function(element) {
          var observer = new MutationObserver(function(mutations) {
              mutations.forEach(function(mutation) {
                if (mutation.type == "attributes") {
                  stopPlay(mutation.target.id);
                }
              });
            });

            observer.observe(element, {
              attributes: true //configure it to listen to attribute changes
            });

            function stopPlay(id){
                video = document.getElementById(id);

                var data_scroll = video.getAttribute("data-scroll");
                  if(data_scroll == "in"){
                    video.play();
                    video.playbackRate   = 2.5;
                  }else{
                    video.pause();
                    video.currentTime = 0;
                  }
            }
          
        });
    </script>
    <script>
      const cursor =  document.querySelector(".cursor");
        document.addEventListener('mousemove', (e) => {
            cursor.style.left = e.pageX + 'px';
            cursor.style.top = e.pageY + 'px';
        })
    </script>
</body>
</html>