@extends('layouts.home')

@section('title', 'Autorización para el tratamiento de datos personales sensibles')

@section('sidebar')
    @parent

    <p>This is appended to the master sidebar.</p>
@stop
@section('styles')
    <link rel="stylesheet" type="text/css" href="{{ asset('fonts/gordita/style.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/material-style-form.css')}}">
    
    <style type="text/css" media="screen">
        html{
            background-color: #252523;
        }
        body{
            background-color:  #252523 !important;
        }
        .section{
            background-color:  #252523 !important;
            height: auto;
            display: block;
            padding: 0% 3% 10% 3%;
        }
        .form-contact{
            /*margin-top: 25%;*/
            position: relative;
        }
        .card-form{
            margin-top: 15%;
            margin-bottom: 15%;
            background-color: #313330;
            top: 0;
            border-radius: 10px;
            width: 90%;
            /*position: absolute;*/
            color:  #F1DA00;
            height: 810px;
            position: relative;
            overflow: hidden;
            box-shadow: -3px -3px 2px rgb(88 88 88 / 30%),
                        5px 5px 5px rgba(0,0,0,0.2),
                        15px 15px 15px rgba(0,0,0,0.2);

        }
        .card-form .content{
            padding: 11rem 60px;
            width: 70%;
            float: left;
            display: inline-grid;
            box-sizing: border-box;
        }
        .title-page{
            position: absolute;
            font-size: 13rem;
            left: 20%;
            z-index: 2;
            font-family: auto;
            opacity: 0.2;
            color: #54523e;
            top: -30px;
        }
        .title{
            font-size: 1.7rem;
            margin: 0px;
        }
        .sub-title{
            /*color: #8e7235;*/
            color: #FEC900;
            opacity: .6;
        }
        .card-form .map{
            width: 29%;
            float: right;
            height: 100%;
            display: inline-flex;
        }
        .card-info{
            font-family: gordita,Liberation Sans,sans-serif;
            background-color: #F7F7F7;
            top: 0;
            padding: 80px 60px;
            border-radius: 2px;
            width: 46%;
            font-size: 1.3rem;
            position: absolute;
        }
        
        .video-contacto{
            width: 70%;
            filter: saturate(0.1);
            right: 0px;
            top: 160px;
            border-radius: 3px;
            overflow: hidden;
            position: absolute;
        }
        .video-contacto video{
            
            width: 100%;
            left: -3px;
            position: relative;
        }

        .info-contact{
            height: auto;
            position: relative;
        }
        .left{
            width: 50%;
            display: inline-grid;
            left: 0px;
            position: relative;
        }
        .right{
            width: 48%;
            right: 0px;
            margin: 0px;
            display: inline-grid;
            position: relative;
        }
        .checkbox label{
            color: #999;
        }
        .form-group span, .checkbox span{
            color: red;
        }
        
        label a, label a:hover{
            color: #FEC900;
        }
    </style>
@stop
@section('content')

    <section class="section form-contact">
        
                <div class="card-form" class="text" data-aos="fade-up" data-aos-offset="0" data-aos-delay="50"  data-aos-duration="1000" data-aos-easing="ease-in-out-back" data-aos-mirror="true" data-aos-once="false">
                    <h1 class="title-page">Contacto</h1>
                    <div class="content">
                        <h2 class="title">Politica de tratamiento de datos!</h2>
                        <p class="sub-title">Leea detenidamente</p>
                        <br><br>
                        <p>
                        Declaro de manera libre, expresa, inequívoca e informada, que AUTORIZO a GRUPO DE INVESTIGACIONES ESPECIALIZADO - Juan Miguel Angarita para que, en los términos del literal a) del artículo 6 de la Ley 1581 de 2012, realice la recolección, almacenamiento, uso, circulación, supresión, y en general, tratamiento de mis datos personales, incluyendo datos sensibles, como mis huellas digitales, fotografías, videos y demás datos que puedan llegar a ser considerados como sensibles de conformidad con la Ley, para que dicho Tratamiento se realice con el fin de lograr las finalidades relativas a ejecutar el control, seguimiento, monitoreo, vigilancia y, en general, garantizar la seguridad de sus instalaciones; así como para documentar las actividades gremiales. Declaro que se me ha informado de manera clara y comprensible que tengo derecho a conocer, actualizar y rectificar los datos personales proporcionados, a solicitar prueba de esta autorización, a solicitar información sobre el uso que se le ha dado a mis datos personales, a presentar quejas ante la Superintendencia de Industria y Comercio por el uso indebido de mis datos personales, a revocar esta autorización o solicitar la supresión de los datos personales suministrados y a acceder de forma gratuita a los mismos. Declaro que conozco y acepto el Manual de Tratamiento de Datos Personales de GRUPO DE INVESTIGACIONES ESPECIALIZADO - Juan Miguel Angarita, y que la información por mí proporcionada es veraz, completa, exacta, actualizada y verificable. Mediante la firma del presente documento, manifiesto que reconozco y acepto que cualquier consulta o reclamación relacionada con el Tratamiento de mis datos personales podrá ser elevada verbalmente o por escrito ante GRUPO DE INVESTIGACIONES ESPECIALIZADO - Juan Miguel Angarita, como Responsable del Tratamiento, cuya página web es: www.juanmiguelangarita.com y su teléfono de atención es 310 511 0889
                    </p>
                    </div>
                    
                    <div class="map">
                        <div class="mapouter">
                            <div class="gmap_canvas">
                                <iframe width="100%" height="100%" id="gmap_canvas" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3976.6215181644816!2d-74.11003248190615!3d4.661387453285049!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x456afcf242a07c3!2sCentro%20Empresarial%20Arrecife!5e0!3m2!1ses-419!2sco!4v1599595358221!5m2!1ses-419!2sco" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe>
                            </div>
                            <style>.mapouter{position:relative;text-align:right;height:100%;width:100%;}.gmap_canvas {overflow:hidden;background:none!important;height:100%;width:100%;}
                            </style>
                        </div>
                    </div> 
                </div>
            
            
                
        <br><br><br>        
    </section>
    
@stop
