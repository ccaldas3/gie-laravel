
@extends('layouts.servicios')

@section('title', 'Page Title')

@section('sidebar')
    @parent

    <p>This is appended to the master sidebar.</p>
@stop

@section('content')
   
<div class="cta_content cta out" style="top: 92%;"><span style="top: 40px;">VIEW SKILL</span><div class="under" style="left: 100%;"></div></div>  
<div class="hud" style="width: 100vh; height: initial; visibility:hidden; cursor: pointer">
{{-- <div class="hud" style="width: 100vh; height: initial;"> --}}

  <div class="cta_back_mobile" style="left: -30px; left: 10vw; top: 10vh; z-index: 12; position: relative;">
<svg x="0px" y="0px" width="36px" height="20px" viewBox="0 0 45 30" stroke="none">
<defs>
</defs>
<path d="M14,0L0,15l14,15l4-4L8,15L18,4L14,0z"></path>
</svg>
</div>

	{{-- <header class="header">
        <a href="../home/home.html" title="">
            <img class="logo" src="{{ asset('assets/img/logo3.png') }}" alt="Logo">
        </a>
    </header>
    <aside class="side-bar">
        <input class="tgl tgl-ios" id="btn-menu" type="checkbox" />
        <label class="tgl-btn" for="btn-menu">
            <span id="icon-menu" class='hamburger'></span>
        </label>
        <ul class="social-list" id="social-list-side">
            <li class="follow">
                <span>SÍGUENOS</span>
            </li>
            <li class="social-item">
                <a href="#"><i class="fab fa-facebook-f"></i></a>
            </li>
            <li class="social-item">
                <a href="#"><i class="fab fa-twitter"></i></a>
            </li>
            <li class="social-item">
                <a href="#"><i class="fab fa-instagram"></i></a>
            </li>
        </ul>
    </aside>
    <div id="menu-section" class="menu-section menu-home">
        <div class="separator">
            <div class="title">
                <h1>NUESTOS <br>SERVICIOS</h1>
            </div>
            <p class="hashtag-text">#ProfesionalismoGIE</p>
            <span class="follow-container">
                <button class="follow">SÍGUENOS</button>
            </span>
            <div class="social-list">
                <div class="social-item">
                    <a href="#">
                        <i class="fab fa-facebook-f"></i>
                    </a>
                </div>
                <div class="social-item">
                    <a href="#">
                        <i class="fab fa-twitter"></i>
                    </a>
                </div>
                <div class="social-item">
                    <a href="#">
                        <i class="fab fa-instagram"></i>
                    </a>
                </div>
            </div>
        </div>
        <!-- <div class="content"> -->
            <div class="navigation">
                <ul>
                    <li><a href="#" data-text="Home">Home</a></li>
                    <li><a href="../services/index.html" data-text="Servicios">Servicios</a></li>
                    <li><a href="#" data-text="Blog">Blog</a></li>
                    <li><a href="#" data-text="Vídeos">Vídeos</a></li>
                    <li><a href="#" data-text="Contacto">Contacto</a></li>
                </ul>
            </div>
        <!-- </div> -->
    </div> --}}
  <!-- <div class="big_menu " style="display: none;">
    <div class="cta_contact cta out"><span style="top: 40px;">Contact</span><div class="under" style="left: 100%;"></div></div>

    <div class="menu_turn center">
      <div class="menu_links" style="transform: matrix3d(0.999999, 0, 0.0017017, 0, 0, 1, 0, 0, -0.0017017, 0, 0.999999, 0, 0, 0, 0, 1);">
       <div class="link"><span class="atoz" style="top: -100px;">A to Z</span></div>
       <div class="link"><span class="bt_skills" style="top: -100px;">Skills</span></div>
       <div class="link"><span class="bt_project" style="top: -100px;">Projects</span></div>  
       <div class="link"><span class="bt_previously" style="top: -100px;">Previously</span></div> 
       <div class="link"><span class="bt_about" style="top: -100px;">About</span></div>   
     </div>

   </div>

   <div class="over_link"><svg id="screen_svg" width="1349" height="625" style="display: none;"><path id="over_svg" d="M550 75 C668 65 668 65 788 75 V155 C668 174.5 668 174.5 550 155 V75Z" fill="#fff"></path></svg></div>

   <div class="line line1" style="margin-top: 0%; height: 0px;"></div>
   <div class="line line2" style="margin-top: 0%; height: 0px;"></div>
   <div class="line line3" style="margin-top: 0%; height: 0px;"></div>
 </div> -->

 <div class="big_side" style="height: 576px;">  

   <div class="side_title" style="left: -30px;"><div class="side"><span></span></div></div>
   <div class="side_title_r" style="right: -30px;"><div class="side"><span></span></div></div>

   <div class="arrow a_left lk" style="left: -50px;">
    <svg width="40px" height="40px">
      <path fill-rule="evenodd" stroke="rgb(255, 255, 255)" stroke-width="4px" stroke-linecap="butt" stroke-linejoin="miter" fill="rgb(255, 255, 255)" class="a1" d="M2,2 L2,18"></path>

      <path fill-rule="evenodd" stroke="rgb(255, 255, 255)" stroke-width="4px" stroke-linecap="butt" stroke-linejoin="miter" fill="rgb(255, 255, 255)" class="a2" d="M4,16 L4,16"></path>

      <path fill-rule="evenodd" stroke="rgb(255, 255, 255)" stroke-width="4px" stroke-linecap="butt" stroke-linejoin="miter" fill="rgb(255, 255, 255)" class="a3" d="M2,16 L2,30"></path>
    </svg>
  </div>


  <div class="arrow a_right lk" style="right: -50px;">
    <svg width="40px" height="40px">
      <path fill-rule="evenodd" stroke="rgb(255, 255, 255)" stroke-width="4px" stroke-linecap="butt" stroke-linejoin="miter" fill="rgb(255, 255, 255)" class="r1" d="M38,2 L38,18"></path>

      <path fill-rule="evenodd" stroke="rgb(255, 255, 255)" stroke-width="4px" stroke-linecap="butt" stroke-linejoin="miter" fill="rgb(255, 255, 255)" class="r2" d="M36,16 L36,16"></path>

      <path fill-rule="evenodd" stroke="rgb(255, 255, 255)" stroke-width="4px" stroke-linecap="butt" stroke-linejoin="miter" fill="rgb(255, 255, 255)" class="r3" d="M38,16 L38,30"></path>
    </svg>
  </div>

</div>  

<!-- <div class="menu lk" style="display: block;">
  <svg class="burger" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="39px" height="39px" style="margin-left: 0px; width: 39px;">
   <path class="l1" fill-rule="evenodd" stroke-width="4px" stroke-linecap="butt" stroke-linejoin="miter" fill="none" d="M11,19 L28,19"></path>
   <path class="l2" fill-rule="evenodd" stroke-width="4px" stroke-linecap="butt" stroke-linejoin="miter" fill="none" d="M11,19 L28,19"></path>
   <path class="l3" fill-rule="evenodd" stroke-width="4px" stroke-linecap="butt" stroke-linejoin="miter" fill="none" d="M11,19 L28,19"></path>
 </svg>
</div> -->

<div class="scroll_msg" style="top: 70vh !important"><span style="top: 1px;">SCROLL</span><div class="trait" style="top: 27px;"></div></div>
</div>



  <div class="rtt" style="margin-left: 2px; transform: matrix3d(0.99964, 0, 0.0268312, 0, 0.000100683, 0.999993, -0.0037511, 0, -0.026831, 0.00375245, 0.999633, 0, 0, 0, 0, 1); display: block;">
    <div class="main">
      <div class="big_title center cta_page" style="margin-left: -3px; display: block; margin-top: 0px;"><span style="top: 0px;" class=""></span></div>
      <div class="small_title center cta_page" style="margin-left: -2px; display: block;"><span style="top: -20px;" class=""></span></div>
      <div class="extra center cta_page extra_alpha" style="margin-left: -2px; opacity: 1; display: block; margin-top: -10px;"></div>
      <div class="barre middle center cta_page" style="opacity: 1; height: 45px; top: 50%; width: 239px; display: block; margin-top: 0px;">
        <div class="cache" style="top: 0%; height: 140%; color: yellow !important"></div>     
        <div class="bkg" style="transform: matrix(1, 0, 0, 1, 0, 0); top: 0px;"></div>
   
              
<div class="alpha2" style="opacity: 1; top: -16px;">
	<span class="first">...</span>
	<span>Investigación&nbsp;para&nbsp;la&nbsp;defensa&nbsp;Técnica</span>
	<span>Investigación&nbsp;privada&nbsp;empresarial</span>
	<span>Laboratorio&nbsp;de&nbsp;Ciberseguridad</span>
	<span>Investigación&nbsp;Financiera&nbsp;Forense</span>
	<span>Psicologia&nbsp;Jurídica&nbsp;Empresarial</span>
	
	<span class="first">...</span>
</div>
</div>
<div class="title1 center win"><span style="top: -51px;"><img class="" src="{{ asset('assets/img/logo3.png') }}" alt="Logo" style="width: 210px;"> </span></div>
<div class="title2 center win"><span style="top: -20px;">Nuestros Servicios</span></div>
<div class="over_title center" style="display: block;color: white"></div>


<div class="alpha" style="top: 252px; opacity: 0; display: none;">
	<span style="opacity: 1;">...</span>
	<span>Investigación&nbsp;para&nbsp;la&nbsp;defensa&nbsp;Técnica</span>
	<span>Investigación&nbsp;privada&nbsp;empresarial</span>
	<span>Laboratorio&nbsp;de&nbsp;Ciberseguridad</span>
	<span>Investigación&nbsp;Financiera&nbsp;Forense</span>
	<span>Psicologia&nbsp;Jurídica&nbsp;Empresarial</span>
	<!-- <span style="opacity: 1;">F</span>
	<span style="opacity: 1;">G</span>
	<span style="opacity: 1;">H</span>
	<span style="opacity: 1;">I</span>
	<span style="opacity: 1;">J</span>
	<span style="opacity: 1;">K</span>
	<span style="opacity: 1;">L</span>
	<span style="opacity: 1;">M</span>
	<span style="opacity: 1;">N</span>
	<span style="opacity: 1;">O</span>
	<span style="opacity: 1;">P</span>
	<span style="opacity: 1;">Q</span>
	<span style="opacity: 1;">R</span>
	<span style="opacity: 1;">S</span>
	<span style="opacity: 1;">T</span>
	<span style="opacity: 1;">U</span>
	<span style="opacity: 1;">V</span>
	<span style="opacity: 1;">W</span>
	<span style="opacity: 1;">X</span>
	<span style="opacity: 1;">Y</span>
	<span style="opacity: 1;">Z</span> -->
	{{-- <span style="opacity: 1;">.CLICK ME..</span> --}}
</div>
</div>
<!-- <div class="intro_atoz" style="margin-left: -3px;"><img src="http://digitaldesigner.cool/img/atoz/0.png" alt=""></div> -->
<div class="background" style="margin-left: 9px;">
      <div class="bkl fs2" data-speed="1.7590318140149193" style="top: 535px; left: 2568px; margin-left: 0px; opacity: 1;">
        <span>E</span>
      </div>
      <div class="bkl fs3" data-speed="1.5564459252676532" style="top: 113px; left: 2445px; margin-left: 0px; opacity: 1;">
        <span>E</span>
      </div>
      <div class="bkl fs5" data-speed="1.769809126591423" style="top: -498px; left: 1085px; margin-left: 0px; opacity: 1;">
        <span>Z</span>
      </div>
      <div class="bkl fs2" data-speed="2.2211805307592565" style="top: -341px; left: 2400px; margin-left: 0px; opacity: 1;">
        <span>I</span>
      </div>
      <div class="bkl fs2" data-speed="1.1946814243974688" style="top: 265px; left: 1779px; margin-left: 0px; opacity: 1;">
        <span>Z</span>
      </div>
<div class="bkl fs3" data-speed="1.9942428923524806" style="top: 484px; left: 2099px; margin-left: 0px; opacity: 1;">
  <span>Y</span>
</div>
<div class="bkl fs4" data-speed="1.2975956861875235" style="top: 436px; left: 457px; margin-left: 0px; opacity: 1;">
  <span>P</span>
</div>
<div class="bkl fs2" data-speed="2.291419588804386" style="top: -380px; left: 1907px; margin-left: 0px; opacity: 1;">
  <span>M</span>
</div>
<div class="bkl fs2" data-speed="1.8925193296004856" style="top: 318px; left: 1910px; margin-left: 0px; opacity: 1;">
  <span>L</span>
</div>
<div class="bkl fs4" data-speed="2.703829731936718" style="top: 236px; left: 451px; margin-left: 0px; opacity: 1;">
  <span>N</span>
</div>

<div class="bkl fs4" data-speed="1.5835969277579185" style="top: 621px; left: 1747px; margin-left: 0px; opacity: 1;"><span>E</span></div><div class="bkl fs5" data-speed="2.2034831259238183" style="top: 15px; left: 2630px; margin-left: 0px; opacity: 1;"><span>Z</span></div><div class="bkl fs1" data-speed="1.248010392772009" style="top: -515px; left: 1025px; margin-left: 0px; opacity: 1;"><span>F</span></div><div class="bkl fs4" data-speed="1.0430437732974305" style="top: 117px; left: 1606px; margin-left: 0px; opacity: 1;"><span>C</span></div><div class="bkl fs3" data-speed="2.4906696389451866" style="top: 6px; left: 2692px; margin-left: 0px; opacity: 1;"><span>V</span></div><div class="bkl fs5" data-speed="1.2010920642800196" style="top: -440px; left: 1124px; margin-left: 0px; opacity: 1;"><span>A</span></div><div class="bkl fs3" data-speed="2.4355258460225264" style="top: 480px; left: 255px; margin-left: 0px; opacity: 1;"><span>W</span></div><div class="bkl fs3" data-speed="2.223484483493934" style="top: 397px; left: 1219px; margin-left: 0px; opacity: 1;"><span>V</span></div><div class="bkl fs3" data-speed="2.425561511923499" style="top: -34px; left: 835px; margin-left: 0px; opacity: 1;"><span>V</span></div><div class="bkl fs5" data-speed="2.9635006139719478" style="top: 61px; left: 738px; margin-left: 0px; opacity: 1;"><span>K</span></div><div class="bkl fs2" data-speed="1.3045035777414715" style="top: -152px; left: 114px; margin-left: 0px; opacity: 1;"><span>K</span></div><div class="bkl fs1" data-speed="1.2471546603532873" style="top: -316px; left: 2247px; margin-left: 0px; opacity: 1;"><span>F</span></div><div class="bkl fs3" data-speed="1.0114723330980904" style="top: -516px; left: 778px; margin-left: 0px; opacity: 1;"><span>I</span></div><div class="bkl fs3" data-speed="2.9383133322917048" style="top: -388px; left: 682px; margin-left: 0px; opacity: 1;"><span>J</span></div><div class="bkl fs3" data-speed="2.766314820698197" style="top: 371px; left: 1012px; margin-left: 0px; opacity: 1;"><span>Z</span></div><div class="bkl fs3" data-speed="2.1952046447165747" style="top: -62px; left: 1997px; margin-left: 0px; opacity: 1;"><span>U</span></div><div class="bkl fs2" data-speed="2.983127336246637" style="top: 185px; left: 1289px; margin-left: 0px; opacity: 1;"><span>P</span></div><div class="bkl fs4" data-speed="1.2474349178600939" style="top: -520px; left: 2502px; margin-left: 0px; opacity: 1;"><span>L</span></div><div class="bkl fs1" data-speed="2.053563225283792" style="top: -555px; left: 1173px; margin-left: 0px; opacity: 1;"><span>A</span></div><div class="bkl fs3" data-speed="1.067470136809443" style="top: 447px; left: 1079px; margin-left: 0px; opacity: 1;"><span>J</span></div><div class="bkl fs3" data-speed="2.9440645520153104" style="top: -423px; left: 753px; margin-left: 0px; opacity: 1;"><span>M</span></div><div class="bkl fs3" data-speed="1.4518744541094688" style="top: -71px; left: 1783px; margin-left: 0px; opacity: 1;"><span>T</span></div><div class="bkl fs4" data-speed="1.3820834169632699" style="top: -517px; left: 1316px; margin-left: 0px; opacity: 1;"><span>Z</span></div><div class="bkl fs4" data-speed="2.7214505985956117" style="top: 383px; left: 126px; margin-left: 0px; opacity: 1;"><span>A</span></div><div class="bkl fs5" data-speed="1.5991257881662886" style="top: 484px; left: 2499px; margin-left: 0px; opacity: 1;"><span>L</span></div><div class="bkl fs2" data-speed="1.4641394596790396" style="top: -110px; left: 1721px; margin-left: 0px; opacity: 1;"><span>I</span></div></div>
</div>

@stop

