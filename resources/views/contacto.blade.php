@extends('layouts.home')

@section('title', 'Contacto')

@section('sidebar')
    @parent

    <p>This is appended to the master sidebar.</p>
@stop
@section('styles')
    <link rel="stylesheet" type="text/css" href="{{ asset('fonts/gordita/style.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/material-style-form.css')}}">
    
    <style type="text/css" media="screen">
        html{
            background-color: #252523;
        }
        body{
            background-color:  #252523 !important;
        }
        .section{
            background-color:  #252523 !important;
            height: auto;
            display: block;
            padding: 0% 3% 10% 3%;
        }
        .form-contact{
            /*margin-top: 25%;*/
            position: relative;
        }
        .card-form{
            margin-top: 15%;
            margin-bottom: 15%;
            background-color: #313330;
            top: 0;
            border-radius: 10px;
            width: 90%;
            /*position: absolute;*/
            color:  #F1DA00;
            height: 810px;
            position: relative;
            overflow: hidden;
            box-shadow: -3px -3px 2px rgb(88 88 88 / 30%),
                        5px 5px 5px rgba(0,0,0,0.2),
                        15px 15px 15px rgba(0,0,0,0.2);

        }
        .card-form .content{
            padding: 11rem 60px;
            width: 70%;
            float: left;
            display: inline-grid;
            box-sizing: border-box;
        }
        .title-page{
            position: absolute;
            font-size: 13rem;
            left: 20%;
            z-index: 2;
            font-family: auto;
            opacity: 0.2;
            color: #54523e;
            top: -30px;
        }
        .title{
            font-size: 1.7rem;
            margin: 0px;
        }
        .sub-title{
            /*color: #8e7235;*/
            color: #FEC900;
            opacity: .6;
        }
        .card-form .map{
            width: 29%;
            float: right;
            height: 100%;
            display: inline-flex;
        }
        .card-info{
            font-family: gordita,Liberation Sans,sans-serif;
            background-color: #F7F7F7;
            top: 0;
            padding: 80px 60px;
            border-radius: 2px;
            width: 46%;
            font-size: 1.3rem;
            position: absolute;
        }
        
        .video-contacto{
            width: 70%;
            filter: saturate(0.1);
            right: 0px;
            top: 160px;
            border-radius: 3px;
            overflow: hidden;
            position: absolute;
        }
        .video-contacto video{
            
            width: 100%;
            left: -3px;
            position: relative;
        }

        .info-contact{
            height: auto;
            position: relative;
        }
        .left{
            width: 50%;
            display: inline-grid;
            left: 0px;
            position: relative;
        }
        .right{
            width: 48%;
            right: 0px;
            margin: 0px;
            display: inline-grid;
            position: relative;
        }
        .checkbox label{
            color: #999;
        }
        .form-group span, .checkbox span{
            color: red;
        }
        
        label a, label a:hover{
            color: #FEC900;
        }
    </style>
@stop
@section('content')

    <section class="section form-contact">
        
                <div class="card-form" class="text" data-aos="fade-up" data-aos-offset="0" data-aos-delay="50"  data-aos-duration="1000" data-aos-easing="ease-in-out-back" data-aos-mirror="true" data-aos-once="false">
                    <h1 class="title-page">Contacto</h1>
                    <div class="content">
                        <h2 class="title">¡Consultenos!</h2>
                        <p class="sub-title">Escribanos sobre su requerimento y nos pondremos en contacto con usted.</p>
                        <fieldset id="field_form">
                        <form>
                            <div class="left">
                                <div class="form-group">
                                  <input type="text" id="nombres" required="required"/>
                                  <label for="input" class="control-label">Nombres</label><i class="bar"></i>
                                  <span id="nombres_span"></span>
                                </div>
                            </div>
                            <div class="right">
                                <div class="form-group">
                                  <input type="text" id="apellidos" required="required"/>
                                  <label for="input" class="control-label">Apellidos</label><i class="bar"></i>
                                  <span id="apellidos_span"></span>
                                </div>
                            </div>
                            <div class="left">
                                <div class="form-group">
                                  <input type="email" id="correo" required="required"/>
                                  <label for="input" class="control-label">Correo</label><i class="bar"></i>
                                  <span id="correo_span"></span>
                                </div>
                            </div>
                            <div class="right">
                                
                                <div class="form-group">
                                  <input type="text" id="telefono" required="required"/>
                                  <label for="input" class="control-label">Número de telefono</label><i class="bar"></i>
                                  <span id="telefono_span"></span>
                                </div>
                            </div>
                            
                            <div class="form-group">
                                  <textarea required="required" id="mensaje" ></textarea>
                                  <label for="textarea" class="control-label">Mensaje</label><i class="bar"></i>
                                  <span id="mensaje_span"></span>
                                </div>    
                            <div class="checkbox">
                              <label>
                                <input type="checkbox" id="acepto" /><i class="helper"></i>Acepto
                                <a href="{{ route('tratamiento_de_datos')}}" target="_blank" title="Aceptar tratamiento de datos">tratamiento de datos</a>
                              </label>
                              <span id="acepto_span"></span>
                            </div>
                            <div class="button-container">
                                <button type="button" id="btn_enviar_button" href="javascript:;" onclick="enviar()" class="button"><span id="btn_enviar_span">Enviar</span></button>
                            </div>
                        </form>
                        </fieldset>
                    </div>
                    <div class="map">
                        <div class="mapouter">
                            <div class="gmap_canvas">
                                <iframe width="100%" height="100%" id="gmap_canvas" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3976.6215181644816!2d-74.11003248190615!3d4.661387453285049!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x456afcf242a07c3!2sCentro%20Empresarial%20Arrecife!5e0!3m2!1ses-419!2sco!4v1599595358221!5m2!1ses-419!2sco" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe>
                            </div>
                            <style>.mapouter{position:relative;text-align:right;height:100%;width:100%;}.gmap_canvas {overflow:hidden;background:none!important;height:100%;width:100%;}
                            </style>
                        </div>
                    </div> 
                </div>
            
        
    {{-- </section>
    <br><br><br><br><br><br>
    <section class="section info-contact"> --}}
        {{-- <figure class="_1ZFXeT-WUb2wJ5KS61iE_1_0"> --}}
            
                <div class="info-contact">
                    <div class="card-info rellax" data-rellax-speed="-2">
                        +57 310 511 0889
                        <br>
                        juanmiguelangarita@gmail.com

                        <br><br> <br><br><br>
                        Ac. 26 # 69D-91,<br> Centro Empresarial Arrecife<br> Oficinas 602 - 603<br>Bogotá
                    </div>
                    {{-- <div data-aos="fade-up" data-aos-offset="0" data-aos-delay="50" data-aos-duration="1000"
                                data-aos-easing="ease-in-out-back" data-aos-mirror="true" data-aos-once="false" class="video-contacto" data-anim="parallax" data-parallax="0.3" data-animated="true" > --}}
                    <div class="rellax video-contacto" data-rellax-speed="1">
                        <video autoplay="true" playsinline="true" webkit-playsinline="" x-webkit-airplay="" preload="" loop="loop" muted="muted" class=""><source src="{{ asset('assets/video/Home.mp4')}}" type="video/mp4">
                        </video>
                    </div>
                </div>
        <br><br><br>        
    </section>

    <section class=" margen" style="opacity: 0 !important;">
        <br><br><br>        
        <div style="height: 500px">
            
        </div>
        <br><br><br>        
    </section>
    <br><br><br>
    {{-- <div > --}}
            
{{-- 
            <section class="section experience-section">
                <div class="text-box">
                    <p class="text" data-aos="fade-up" data-aos-offset="0" data-aos-delay="50" data-aos-duration="1000"
                        data-aos-easing="ease-in-out-back" data-aos-mirror="true" data-aos-once="false">EXPERIENCIA</p>
                    <p class="text" data-aos="fade-up" data-aos-offset="0" data-aos-delay="50" data-aos-duration="1000"
                        data-aos-easing="ease-in-out-back" data-aos-mirror="true" data-aos-once="false">Y PROFESIONALISMO</p>
                </div>
                <div class="svg-container">
                    <svg viewBox="0 0 400 800" xmlns="http://w3.org/2000/svg" class="svg-first-ellipse">
                        <path class="path-ellipse" id="path-1"
                            d="M 400 100 C 300 0 250 50 200 100 C 100 200 0 400 0 550 C 0 700 200 850 400 700 "></path>
                    </svg>
                    <svg viewBox="0 0 400 800" xmlns="http://w3.org/2000/svg" class="svg-first-ellipse">
                        <path class="path-ellipse" id="path-2" d="M 400 150 C 250 200 100 100 50 200 C 0 350 100 750 400 750 ">
                        </path>
                    </svg>
                    <svg viewBox="0 0 400 800" xmlns="http://w3.org/2000/svg" class="svg-first-ellipse">
                        <path class="path-animated" id="path-3"
                            d="M 400 100 C 300 0 250 50 200 100 C 100 200 0 400 0 550 C 0 700 200 850 400 700 "></path>
                    </svg>
                    <svg viewBox="0 0 400 800" xmlns="http://w3.org/2000/svg" class="svg-first-ellipse">
                        <path class="path-animated" id="path-4" d="M 400 150 C 250 200 100 100 50 200 C 0 350 100 750 400 750 ">
                        </path>
                    </svg>
                </div>
            </section> --}}
            
           
                   
        {{-- </div> --}}
    
    {{-- <footer id="footer" >
        <div class="container">
            <div class="text-box" data-aos="fade-down" data-aos-offset="0" data-aos-delay="50" data-aos-duration="1000"
                data-aos-easing="ease-in-out-back" data-aos-mirror="true" data-aos-once="false">
                <h2 class="title">GRUPO DE INVESTIGACIONES ESPECIALIZADO</h2>
                <p class="text">Profesionales en Investigación críminal y Ciencias Forenses, asesoría integral y
                    orientación.</p>
            </div>
            <div class="text-box" data-aos="fade-down" data-aos-offset="0" data-aos-delay="50" data-aos-duration="1000"
                data-aos-easing="ease-in-out-back" data-aos-mirror="true" data-aos-once="false">
                <h2 class="title">SEDE CENTRO</h2>
                <p class="text"><i class="fas fa-building"></i> Av Jimenez # 8A - 77, Edif. Seguros Universal, Bogotá
                </p>
                <p class="text">204 - Oficina Administrativa y contable</p>
                <p class="text">301 - Laboratorio de Ciberseguridad e informática Forense</p>
                <p class="text">303 - Coordinación de Investigaciones</p>
                <p class="text"><i class="fas fa-building"></i> Carrera 9 # 13-36 Edif. Colombia, Bogotá</p>
                <p class="text">602 - Capacitaciones GIE</p>
            </div>
            <div class="text-box" data-aos="fade-up" data-aos-offset="0" data-aos-delay="50" data-aos-duration="1000"
                data-aos-easing="ease-in-out-back" data-aos-mirror="true" data-aos-once="false">
                <h2 class="title">CONTACTO</h2>
                <p class="text"><i class="fas fa-envelope"></i> juanmiguelangarita@gmail.com</p>
                <p class="text"><i class="fas fa-phone-alt"></i> (+57) 310 511 0889</p>
                <div class="social-box">
                    <span class="follow">SÍGUENOS</span>
                    <a href="#" class="social-item"><i class="fab fa-facebook-f"></i></a>
                    <a href="#" class="social-item"><i class="fab fa-twitter"></i></a>
                    <a href="#" class="social-item"><i class="fab fa-instagram"></i></a>
                </div>
            </div>
        </div>
        <span class="copyright">Copyright <i class="far fa-copyright"></i> 2019 Juan Miguel Angarita <i
                class="far fa-registered"></i>. All rights reserved.</span>
        <span class="author">Diseñado Por HumanCode</span>
    </footer> --}}
    
@stop

@section('javascript')

    <script src="{{ asset('js/rellax.min.js')}}" type="text/javascript" charset="utf-8" ></script>
    <script>
    // Accepts any class name
      var rellax = new Rellax('.rellax');
    </script>
    <script>
    
        var enviar = function() {
            console.log("hola");

            // Limpio los span
            $('#nombres_span').html("");
            $('#apellidos_span').html("");
            $('#correo_span').html("");
            $('#telefono_span').html("");
            $('#mensaje_span').html("");

            var nombres     = $('#nombres').val()
            var apellidos   = $('#apellidos').val()
            var correo      = $('#correo').val()
            var telefono    = $('#telefono').val()
            var mensaje     = $('#mensaje').val()
            var acepto     = $('#acepto').is(":checked")

            var valited = true;


            if(nombres == ""){
                $('#nombres_span').html("Ingrese sus nombres"); valited = false;
            }
            if(apellidos == ""){
                $('#apellidos_span').html("Ingrese sus apellidos"); valited = false;
            }
            if(correo == ""){
                $('#correo_span').html("Ingrese su correo electrónico"); valited = false;
            }
            if(telefono == ""){
                $('#telefono_span').html("Ingrese su teléfono"); valited = false;
            }
            if(mensaje == ""){
                $('#mensaje_span').html("Ingrese un mensaje"); valited = false;
            }
            if(!acepto){
                $('#acepto_span').html("Debe aceptar el tratamiento de datos"); valited = false;
            }

            

            emailRegex = /^(?:[^<>()[\].,;:\s@"]+(\.[^<>()[\].,;:\s@"]+)*|"[^\n"]+")@(?:[^<>()[\].,;:\s@"]+\.)+[^<>()[\]\.,;:\s@"]{2,63}$/i;
            //Se muestra un texto a modo de ejemplo, luego va a ser un icono
            if (!emailRegex.test(correo)) {
              $('#correo_span').html("El correo no es valido");
            }

            if(valited){


                $.ajax({
                    type: "POST",
                    url: "{{ route('send_menssage') }}",
                    data: {
                        "_token": "{{ csrf_token() }}",
                        "nombres": nombres,
                        "apellidos": apellidos,
                        "correo": correo,
                        "telefono": telefono,
                        "mensaje": mensaje,
                        "acepto": acepto,
                        },
                    beforeSend: function( xhr ) {
                        $('#btn_enviar_button').addClass('sending');
                        $('#btn_enviar_span').html("Enviando...");

                            
                    }
                }).done(function(msg) {
                    console.log(msg);
                    $('#btn_enviar_button').removeClass('sending');
                    $('#btn_enviar_span').html("Enviado &#10004;");
                    $('#field_form input').addClass('disabled').prop('disabled', true);
                    $('#field_form input').removeClass('disabled').prop('disabled', false);
                    vaciarCampos();
                  })
                  .fail(function() {
                    $('#btn_enviar_span').html("Error de envío");
                  })
            }
        }

        function vaciarCampos(){
            $('#nombres').val("");
            $('#apellidos').val("")
            $('#correo').val("")
            $('#telefono').val("")
            $('#mensaje').val("")
            
        }
        
    </script>
@stop