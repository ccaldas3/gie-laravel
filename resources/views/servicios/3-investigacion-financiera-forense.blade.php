
@extends('layouts.servicio_normal')

@section('title', 'Page Title')

@section('sidebar')
    @parent

    <p>This is appended to the master sidebar.</p>
@stop

@section('content')



<!-- <section class="section service-banner claro"> -->
    <section class="section service-banner oscuro">
        <div class="breadcrumb-div">
            <ul class="breadcrumb">
                <li class="breacrumb-item"><a href="../services/services.html">Servicios</a> / </li>
                <li class="breacrumb-item breacrumb-item_active"> Investigación Financiera Forense</li>
            </ul>
        </div>
        <div class="row">
            <div class="col-md-6 title">
                    <h1 class="title-word title-word_first">Investigación Financiera</h1>
                    <h1 class="title-word">Forense</h1>
            </div>
            <div class="col-md-6 ">
                <img  class="title-image" data-src="{{ asset('assets/img/1-service-7.jpg') }}" alt="lazy" loading="lazy" >
            </div>
        </div>
        
    </section>
     <section class="section service-intro">
        <div class="row">
            <div class="col-md-4">
                <div class="sub-title">
                    <p class="description-text">¿ De qué se trata ?</p>
                </div>
            </div>
            <div class="col-md-6">
                <div class="description">
                    <p class="description-text" style="font-size: 1.2rem"><b>Esta área es útil y necesaria en aquellos procesos que demanden de la interpretación, cuantificación y/o análisis de especialistas en las áreas financiera, económica, contable, presupuestal o monetaria.</b>
En general el área financiera (que engloba las áreas antes anotadas) es la única ciencia forense que puede actuar en todas los estadios básicos de un proceso penal o civil.

Los campos en los cuales se puede asesorar son variados y entre ellos tenemos:</p>
                </div>
                <br><br>
                Scroll
            </div>
            <div class="col-md-2">
            </div>
        </div>
    </section>
    
    <section class="section service-subservice2">
        <div class="row">
            <div class="col-md-4">
                <div class="sub-description">
                    <p class="description-text">
                        Prestamos un servicio de consultoría y <b>asesoría especializada en temas de seguridad, identificación e investigación de casos,</b> dando las recomendaciones adecuadas.
                    </p>
                </div>
            </div>
            
            <div class="col-md-8">
                <div class="sub-title-service right">
                    <video id="v_4" src="{{ asset('assets/effect/smoke.mp4') }}" autoplay  muted class="smoke-black"></video>
                    <h2>Consultoría y Asesorías en Seguridad</h2>
                    <div class="fadingEffect black"></div>
                </div>
                <div class="img-service right">
                    <div class="number-service left">
                        <p>02</p>
                    </div>
                    <div class="photobox">
                        <img data-src="{{ asset('assets/img/1-service-4.jpg') }}" alt="lazy" loading="lazy" >

                    </div>
                </div>
            </div>
        </div>
        <div id="swiper-container-3" class="swiper-container">
            <div class="items-subservice swiper-wrapper">
            
            <div class="swiper-slide ">
                    <!-- <h3>Analisis del Escrito de Acusación y Observaciones al Descubrimiento</h3> -->
                    <div class="items-description">
                        <p class="description-text">Apoyo en <b>instauración de denuncias</b>
                    </div>
                    <div class="items-img photobox">
                        <img data-src="{{ asset('assets/img/1-service-4-1.jpg') }}" alt="lazy" loading="lazy" >    
                    </div>
                </div>
                <div class="swiper-slide">
                    <!-- <h3>Sustentación en Jucio Oral y las Actuaciones Judiciales que lo Requieran</h3> -->
                    <div class="items-description">
                        <p class="description-text">
                            <b>Identificación</b> de riesgos
                    </div>
                    <div class="items-img photobox">
                        <img data-src="{{ asset('assets/img/1-service-4-2.jpg')}}" alt="lazy" loading="lazy" >    
                    </div>
                </div>
                <div class="swiper-slide">
                    <!-- <h3>Analisis Documental</h3> -->
                    <div class="items-description">
                        <p class="description-text">
                            <b>Seguimiento</b> de procesos judiciales
                        </p>
                    </div>
                    <div class="items-img photobox">
                        <img data-src="{{ asset('assets/img/1-service-4-3.jpg')}}" alt="lazy" loading="lazy" >     
                    </div>
                </div>
                <div class="swiper-slide">
                    <!-- <h3>Analisis Documental</h3> -->
                    <div class="items-description">
                        <p class="description-text">
                            <b>Estudios</b> de seguridad a instalaciones
                        </p>
                    </div>
                    <div class="items-img photobox">
                        <img data-src="{{ asset('assets/img/1-service-4-4.jpg')}}" alt="lazy" loading="lazy" >     
                    </div>
                </div>
                <div class="swiper-slide">
                    <!-- <h3>Analisis Documental</h3> -->
                    <div class="items-description">
                        <p class="description-text">
                            <b>Manejo</b> de crisis
                        </p>
                    </div>
                    <div class="items-img photobox">
                        <img data-src="{{ asset('assets/img/nn.jpg')}}" alt="lazy" loading="lazy" >     
                    </div>
                </div>
                <div class="swiper-slide">
                    <!-- <h3>Analisis Documental</h3> -->
                    <div class="items-description">
                        <p class="description-text">
                            <b>Prevención </b>de lavado de activos
                        </p>
                    </div>
                    <div class="items-img photobox">
                        <img data-src="{{ asset('assets/img/nn.jpg')}}" alt="lazy" loading="lazy" >     
                    </div>
                </div>
                <div class="swiper-slide">
                    <!-- <h3>Analisis Documental</h3> -->
                    <div class="items-description">
                        <p class="description-text">
                            <b>Estudios </b>financieros
                        </p>
                    </div>
                    <div class="items-img photobox">
                        <img data-src="{{ asset('assets/img/nn.jpg')}}" alt="lazy" loading="lazy" >     
                    </div>
                </div>
            </div>

        </div>
    </section>
    <section class="section service-subservice1">
        <div class="row">
            <div class="col-md-8">
                <div class="sub-title-service">
                    <video id="v_3" src="{{ asset('assets/effect/smoke.mp4') }}" autoplay  muted class="smoke-white"></video>
                    <h2>Investigación Empresarial</h2>
                    <div class="fadingEffect"></div>
                </div>
                <div class="img-service">
                    <div class="number-service">
                        <p>03</p>
                    </div>
                    <div class="photobox">
                        <img data-src="{{ asset('assets/img/1-service-3.png')}}" alt="lazy" loading="lazy" >
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="sub-description">
                    <p class="description-text">
                        Desarrollamos investigación interna y externa de eventos que se presentan en la compañía, por medio de la aplicación de programas metodológicos y apoyados con entidades judiciales
                    </p>
                </div>
            </div>
            <div class="col-md-2">
            </div>
        </div>
        <div id="swiper-container-2" class="swiper-container">
            <div class="items-subservice swiper-wrapper">
            <div class="swiper-slide ">
                <h3>Solicitudes a entidades</h3>
                <div class="items-description">
                    <p class="description-text">Se deben realizar solicitudes de información a entidades públicas y privadas.</p>
                </div>
                <div class="items-img photobox">
                    <img data-src="{{ asset('assets/img/1-service-3-1.jpg')}}" alt="lazy" loading="lazy" >
                </div>
            </div>
            <div class="swiper-slide ">
                <h3>Documentación del usuario</h3>
                <div class="items-description">
                    <p class="description-text">Se analiza con base en la documentación que aporte el usuario necesidad de volver a solicitar información para autenticar la ya existente.</p>
                </div>
                <div class="items-img photobox">
                    <img data-src="{{ asset('assets/img/1-service-3-2.jpg')}}" alt="lazy" loading="lazy" >
                </div>
            </div>
            <div class="swiper-slide ">
                <h3>Analisis Documental</h3>
                <div class="items-description">
                    <p class="description-text">Surge con fines de orientación técnica e informativa, representa la información de un documento en un registro estructurado, reduce todos los datos descriptivos físicos y de contenido en un esquema inequívoco, mediante el cual se identificará su favorabilidad para la Defensa Técnica del caso. Se desarrolla a partir de: (Indización, Indicativos, Informativo, Selectivo, Conclusiones, entre otros).</p>
                </div>
                <div class="items-img photobox">
                    <img data-src="{{ asset('assets/img/1-service-3-3.jpg')}}" alt="lazy" loading="lazy" >
                </div>
            </div>
            </div>
        </div>
    </section>
    
    

@stop