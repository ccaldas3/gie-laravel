
@extends('layouts.servicio_ciber')

@section('title', 'Page Title')

@section('sidebar')
    @parent

    <p>This is appended to the master sidebar.</p>
@stop

@section('content')



    <!-- <section class="section service-banner claro"> -->
    <section class="section service-banner oscuro">
        <div class="breadcrumb-div">
            <ul class="breadcrumb">
                <li class="breacrumb-item"><a href="../../services/services.html">Servicios</a> / </li>
                <li class="breacrumb-item breacrumb-item_active">Ciberseguridad e Informática Forense</li>
            </ul>
        </div>
        <div class="row">
            <div class="col-md-6 title">
                    <h1 class="title-word title-word_first">Ciberseguridad e</h1>
                    <h1 class="title-word">Informatica Forense</h1>
            </div>
            <div class="col-md-6 ">
                <div id="div1" >
                    <div id="div2" >
                    </div>
                    <img  class="title-image" data-src="{{ asset('assets/img/4-banner2.png')}}" alt="lazy" loading="lazy" style="margin-left: auto;margin-right: auto;display: block;margin: 10px auto; z-index: 0; position: relative;" >
                </div> 
                
            </div>
        </div>
        
    </section>
    <div class="col-md-7" style="height: 0px;">
        <img class="title-image text-center" src="{{ asset('assets/img/ciberGie1.png')}}" style="opacity: 0.07;position: relative;max-width: 100%;z-index: 1;top: -190px;">
    </div>
     <section class="section service-intro service-banner oscuro" >
        <div class="row">
            <div class="col-md-6">
                <div class="title text-center">
                    <h2 class="title">¿Qué Hacemos?</h2>
                </div>
            </div>
            <div class="col-md-5">
                <div class="description">
                    <p class="description-text" style="font-size: 1.3rem">
                    <b>Coadyuvar las investigaciones realizadas por el Departamento de Investigación de Campo, en las líneas de investigación digital que requieran para su operación equipos de tecnología de punta y talento humano entrenado para el hallazgo, preservación y presentación de evidencias digitales. </b> Desde la óptica de Ciberseguridad, generar en las plataformas e infraestructuras de nuestros clientes, condiciones que lleven al riesgo a su mínima expresión.
                    </p>
                </div>
                <br><br>
                Scroll
            </div>
            <div class="col-md-2">
            </div>
        </div>
    </section>
    <section class="section service-subservice1">
        
        <canvas class='grain'></canvas>
        <div class='game'>
          
         
          <div class='game_characterCreation shadow'>
            <div class='game_characterCreation_inner'>
              <div class='characterForm_title'>
                <h3>¿ Cómo lo hacemos ?</h3>
              </div>
              <div class="characterForm_bottom">
                  <div class="characterForm_bottom__left" >
                    <!-- <div class="rowHeading car">
                      <h4>Desde la Directrices</h4>
                      <p>Your past life is important</p>
                    </div> -->
                    <div class="js-career" id="btn_directrices">
                      <div class="career ciber-button" data-attr="stealth" data-attrchange="5">
                        <div class="career_header">
                          <div class="cIcon" style="background-position:-500% center"></div>
                          <h5>Desde las Directrices</h5>
                        </div>
                        <div class="career_text">
                          <p>Según requerimentos que se necesiten.</p>
                        </div>
                        <div class="career_attributes">
                          <span class="value">+5</span> to <span class="attr">stealth</span>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="characterForm_bottom__right">
                    <div class="js-career" id="btn_derecho">
                      <div class="career ciber-button" data-attr="stealth" data-attrchange="5">
                        <div class="career_header">
                          <div class="cIcon" style="background-position:-500% center"></div>
                          <h5>Óptica del Derecho Penal</h5>
                        </div>
                        <div class="career_text">
                          <p>Para la presentación de evidencias digitales, según los procesos lo requieran</p>
                        </div>
                        <div class="career_attributes">
                          <span class="value">+5</span> to <span class="attr">stealth</span>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="characterForm_bottom__end">
                    <div class="js-career" id="btn_investigacion">
                      <div class="career ciber-button" data-attr="stealth" data-attrchange="5">
                        <div class="career_header">
                          <div class="cIcon" style="background-position:-500% center"></div>
                          <h5>Para la Investigación Privada</h5>
                        </div>
                        <div class="career_text">
                          <p>Pentesting Y Análisis De Vulnerabilidades Informaticas.</p>
                        </div>
                        <div class="career_attributes">
                          <span class="value">+5</span> to <span class="attr">stealth</span>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              
              <div class='js-createCharacter'  style="border: none; background: #0b1b19; padding: 0px;bottom: -14px;" >
                Elija un servicio
              </div>
            </div>
          </div>
        </div>

        @include('sub-modulos.menu-circular-ciber')
    </section>
   
   
 
    {{-- @section('scripts') --}}
    
    
    
{{-- @stop --}}
    <footer class="footer">
        <span class="copyright">Copyright <i class="far fa-copyright"></i> 2019 Juan Miguel Angarita <i class="far fa-registered"></i>. All rights reserved.</span>
        <span class="author">Diseñado Por HumanCode</span>
    </footer>

    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="{{ asset('js/jquery.circlemenu.min.js') }}" type="text/javascript" ></script>
    <script type="text/javascript">
    $(document).ready(function () {

      var e_start = '';
      var e_move = '';
      var e_stop = '';

      e_start = e_start + 'mousedown ';
      // e_start = e_start + 'touchstart ';
      e_move = e_move + 'mousemove ';
      // e_move = e_move + 'touchmove ';
      e_stop = e_stop + 'mouseup ';
      // e_stop = e_stop + 'touchend touchcancel ';

      rotation = 0;
      rotation_sum = 40;
      position_mouse = 0;
      var transform = 'translateY(40vh) translateZ(-870px) rotateX(69deg) rotateY(-26deg) rotatez(-647deg) scale(0.9) ';

      // $(document).on('pageinit', '#index', function () {
      
      
      $('.intro').on(e_start, function ($this) {
          $('.intro').on(e_move, function(e) {
            console.log("Entro");
            
            
            rotation =  getGrades(e);


            console.log(e_move, rotation);
            $(".center").css('transform', 'rotate(' + rotation + 'deg)');
            var rotationString = 'rotate(' + rotation + 'deg)';
            var prev = $('.center').css('transform');
            

            $('.center').css('transform', transform + " " + rotationString);
            e.preventDefault();
          // });
          });
          $(window).one(e_stop, function() {
            console.log("termino");
            $('.intro').off(e_move);
          });

      });

          // Stop event bubbling from the drag
        $('.intro').on(e_start, function(e) {


          console.log("terminó", rotation);
          rotation =  getGrades(e);


          e.stopPropagation();
        });
      });

     var offset = $('.intro').offset();
    function getGrades(e){
                  var center_x = (offset.left) + ($('.intro').width()/2);

            var center_y = (offset.top) + ($('.intro').height()/2);

            var mouse_x = e.pageX; var mouse_y = e.pageY;

            var radians = Math.atan2(mouse_x - center_x, mouse_y - center_y);

            var degree = (radians * (180 / Math.PI) * -1) + 90; 

            var x = event.pageX - $('.intro').offset().left;
            var y = event.pageY - $('.intro').offset().top;

            // position_mouse_tmp = (x + y )/2;
            position_mouse_tmp = degree;


            if(position_mouse_tmp > position_mouse){
              rotation_sum = position_mouse_tmp - position_mouse;
              rotation = rotation + rotation_sum;  
            }else{
              rotation_sum = position_mouse - position_mouse_tmp;
              rotation = rotation - rotation_sum;  
            }

            position_mouse = position_mouse_tmp;

            return degree;
    }
  </script>
    <script src="{{ asset('assets/js/aos.js') }}"></script>
    <script src="{{ asset('assets/js/global.js') }}"></script>
    <script src="{{ asset('assets/js/service.js') }}"></script>
    <!-- Barba js -->
    
    <!-- GSAP for animation -->
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/gsap/3.2.4/gsap.min.js"></script>

    <!-- Swiper -->
     
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.5.1/js/swiper.js"></script>

    <script src="https://unpkg.com/scroll-out/dist/scroll-out.min.js"></script>
    <script src="https://unpkg.com/angular/angular.js"></script>
    <script src="{{ asset('assets/js/app.js') }}"></script>

    <script>
        // Boton buscar
        
        const input = document.getElementById("search-input");
        const searchBtn = document.getElementById("search-btn");

        const expand = () => {
          searchBtn.classList.toggle("close");
          input.classList.toggle("square");
        };

        searchBtn.addEventListener("click", expand);

 
        //Filter angular
        
        var $injector = angular.injector(['ng']);
        var $filter = $injector.get("$filter");
        var obj = {a:1};
        console.log($filter("json")(obj));
        
        // menu
        var menuSactive = false;
        let menuSectionServices = document.getElementById('menu-section-services');
        let btnMenuService =  document.getElementById("btn-menu-service");
        let sideBarMenuRight =  document.getElementById("side-bar-menu-right");

        function showMenuServices(){
            console.log("menuSactive", menuSactive);
            if(menuSactive){
                menuSactive = !menuSactive;
                menuSectionServices.classList.add('isVisible');
                btnMenuService.classList.add('isActive');
                sideBarMenuRight.style.display="none";
            }else{
                menuSactive = !menuSactive
                menuSectionServices.classList.remove('isVisible');
                btnMenuService.classList.remove('isActive');
                sideBarMenuRight.style.display="block";
            }
            

        }

        document.getElementById("btn-menu-service").addEventListener("click", showMenuServices);


        // swiper
        var swiper = new Swiper('#swiper-container-1', {
          speed: 1500,
          slidesPerView: 8,
          centeredSlides: true,
          spaceBetween: 30,
          grabCursor: true,
          autoplay: {
            delay: 3000,
            
          },
          
        });
        var swiper = new Swiper('#swiper-container-2', {
          speed: 1500,
          slidesPerView: 4,
          centeredSlides: true,
          spaceBetween: 30,
          grabCursor: true,
          autoplay: {
            delay: 3000,
            
          },
        });
        var swiper = new Swiper('#swiper-container-3', {
          speed: 1500,
          slidesPerView: 8,
          // centeredSlides: true,
          spaceBetween: 30,
          grabCursor: true,
          autoplay: {
            delay: 2000,
            
          },
          // effect: 'fade',
        });
        var swiper = new Swiper('#swiper-container-4', {
          speed: 1500,
          slidesPerView: 3,
          // centeredSlides: true,
          spaceBetween: 30,
          grabCursor: true,
          autoplay: {
            delay: 2000,
            
          },
          // effect: 'fade',
        });
        var swiper = new Swiper('#swiper-container-5', {
          speed: 1500,
          slidesPerView: 4,
          // centeredSlides: true,
          spaceBetween: 30,
          grabCursor: true,
          autoplay: {
            delay: 2000,
            
          },
          // effect: 'fade',
        });
        var swiper = new Swiper('#swiper-container-6', {
          speed: 1500,
          slidesPerView: 4,
          // centeredSlides: true,
          spaceBetween: 30,
          grabCursor: true,
          autoplay: {
            delay: 2000,
            
          },
          // effect: 'fade',
        });
        // Lazy images
        
         if ('loading' in HTMLImageElement.prototype) {
            // body... 
            console.log("1 loading");
            const images = document.querySelectorAll('img[loading="lazy"]')
            images.forEach(img => {
                img.src = img.dataset.src;
            })
            
        }else{
            console.log("2 loading");
            const script = document.createElement('script')
            script.src = 'https://cdnjs.cloudflare.com/ajax/libs/lazysizes/4.1.8/lazysizes.min.js';
            document.body.appendChild(script);
            const images = document.querySelectorAll('img[loading="lazy"]')
            images.forEach(img => {
                img.src = img.dataset.src;
            })
        }



        ScrollOut({
            targets: 'video, .fadingEffect',
            threshold: .2
        });

        // grab elements as array, rather than as NodeList
        var elements = document.querySelectorAll("video");
        elements = Array.prototype.slice.call(elements);

        // and then make each element do something on scroll
        elements.forEach(function(element) {
          var observer = new MutationObserver(function(mutations) {
              mutations.forEach(function(mutation) {
                if (mutation.type == "attributes") {
                  stopPlay(mutation.target.id);
                }
              });
            });

            observer.observe(element, {
              attributes: true //configure it to listen to attribute changes
            });

            function stopPlay(id){
                video = document.getElementById(id);

                var data_scroll = video.getAttribute("data-scroll");
                  if(data_scroll == "in"){
                    video.play();
                    video.playbackRate   = 2.5;
                  }else{
                    video.pause();
                    video.currentTime = 0;
                  }
            }
          
        });
    </script>


@stop

