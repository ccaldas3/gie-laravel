
@extends('layouts.servicio_normal')

@section('title', 'Page Title')

@section('sidebar')
    @parent

    <p>This is appended to the master sidebar.</p>
@stop

@section('content')



<!-- <section class="section service-banner claro"> -->
    <section class="section service-banner oscuro">
        <div class="breadcrumb-div">
            <ul class="breadcrumb">
                <li class="breacrumb-item"><a href="../services/services.html">Servicios</a> / </li>
                <li class="breacrumb-item breacrumb-item_active">Investigación Empresarial</li>
            </ul>
        </div>
        <div class="row">
            <div class="col-md-6 title">
                    <h1 class="title-word title-word_first">Investigación Privada</h1>
                    <h1 class="title-word">Empresarial</h1>
            </div>
            <div class="col-md-6 ">
                <img  class="title-image" data-src="{{ asset('assets/img/2-banner2.png') }}" alt="lazy" loading="lazy" >
            </div>
        </div>
        
    </section>
     <section class="section service-intro">
        <div class="row">
            <div class="col-md-4">
                <div class="sub-title">
                    <p class="description-text">¿ Cómo lo hacemos ?</p>
                </div>
            </div>
            <div class="col-md-6">
                <div class="description">
                    <p class="description-text" style="font-size: 1.5rem">El Grupo de Investigaciones Especializado, se convierte en un aliado de su seguridad, asesorando, investigando y realizando actividades que mitiguen los riesgos que causan grandes daños al patrimonio de la empresa</p>
                </div>
                <br><br>
                Scroll
            </div>
            <div class="col-md-2">
            </div>
        </div>
    </section>
    <section class="section service-subservice1">
        <div class="row">
            <div class="col-md-8">

                <div class="sub-title-service">
                    <video  id="v_1" src="{{ asset('assets/effect/smoke.mp4') }}" autoplay  muted class="smoke-white"></video>
                    <h2>
                        Asesorías y Actividades Prevención Documental
                    </h2>
                    <div class="fadingEffect"></div>
                </div>
                <div class="img-service">
                    <div class="number-service">
                        <p>01</p>
                    </div>
                    <div class="photobox">
                        <img data-src="{{ asset('assets/img/1-service-1.png')}}" alt="lazy" loading="lazy" >
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="sub-description">
                    <p class="description-text">
                        El objetivo principal es realizar actividades con el fin de <b>tomar medidas anticipadas</b> que mitiguen posibles riesgos.
                    </p>
                </div>
            </div>
            <div class="col-md-2">
            </div>
        </div>
        <!-- <div class="row swiper-container items-subservice"> -->
        <div id="swiper-container-1" class="swiper-container">
                
            
        <div class="items-subservice swiper-wrapper">
            
            <div class="swiper-slide ">
                <!-- <h3>Estudios de Seguridad</h3> -->
                <div class="items-description">
                    <p class="description-text">
                    Estudios de seguridad para procesos de ingreso de personal, personal activo, contratistas y clientes.
                    </p>
                </div>
                <div class="items-img photobox">
                    <img data-src="{{ asset('assets/img/1-service-1-1.jpg')}}" alt="lazy" loading="lazy" >    
                </div>
            </div>
            <div class="swiper-slide ">
                <!-- <h3>Procedimientos Technical Sweep</h3> -->
                <div class="items-description">
                    <p class="description-text">Procedimientos Technical Sweep o Barrido Electrónico</p>
                </div>
                <div class="items-img photobox">
                    <img data-src="{{ asset('assets/img/1-service-1-2.jpg')}}" alt="lazy" loading="lazy" >    
                </div>
            </div>
            <div class="swiper-slide ">
                <!-- <h3>Analisis Documental</h3> -->
                <div class="items-description">
                    <p class="description-text">Medidas de protección para directivos</p>
                </div>
                <div class="items-img photobox">
                    <img data-src="{{ asset('assets/img/1-service-1-3.jpg') }}" alt="lazy" loading="lazy" >     
                </div>
            </div>
            <div class="swiper-slide ">
                <!-- <h3>Analisis Documental</h3> -->
                <div class="items-description">
                    <p class="description-text">Capacitación en prevención y seguridad</p>
                </div>
                <div class="items-img photobox">
                    <img data-src="{{ asset('assets/img/1-service-1-3.jpg')}}" alt="lazy" loading="lazy" >     
                </div>
            </div>
            <div class="swiper-slide ">
                <!-- <h3>Analisis Documental</h3> -->
                <div class="items-description">
                    <p class="description-text">Recolección de información en campo</p>
                </div>
                <div class="items-img photobox">
                    <img data-src="{{ asset('assets/img/1-service-1-3.jpg') }}" alt="lazy" loading="lazy" >     
                </div>
            </div>
            <div class="swiper-slide ">
                <!-- <h3>Analisis Documental</h3> -->
                <div class="items-description">
                    <p class="description-text">Auditorías de procesos</p>
                </div>
                <div class="items-img photobox">
                    <img data-src="{{ asset('assets/img/1-service-1-3.jpg') }}" alt="lazy" loading="lazy" >     
                </div>
            </div>
            <div class="swiper-slide ">
                <!-- <h3>Analisis Documental</h3> -->
                <div class="items-description">
                    <p class="description-text">Pruebas de poligrafía pre empleo y/o específicas</p>
                </div>
                <div class="items-img photobox">
                    <img data-src="{{ asset('assets/img/1-service-1-3.jpg') }}" alt="lazy" loading="lazy" >     
                </div>
            </div>
        </div>
        <!-- Add Pagination -->
        </div>
    </section>
    <section class="section service-subservice2">
        <div class="row">
            <div class="col-md-4">
                <div class="sub-description">
                    <p class="description-text">
                        Prestamos un servicio de consultoría y <b>asesoría especializada en temas de seguridad, identificación e investigación de casos,</b> dando las recomendaciones adecuadas.
                    </p>
                </div>
            </div>
            
            <div class="col-md-8">
                <div class="sub-title-service right">
                    <video id="v_4" src="{{ asset('assets/effect/smoke.mp4') }}" autoplay  muted class="smoke-black"></video>
                    <h2>Consultoría y Asesorías en Seguridad</h2>
                    <div class="fadingEffect black"></div>
                </div>
                <div class="img-service right">
                    <div class="number-service left">
                        <p>02</p>
                    </div>
                    <div class="photobox">
                        <img data-src="{{ asset('assets/img/1-service-4.jpg') }}" alt="lazy" loading="lazy" >

                    </div>
                </div>
            </div>
        </div>
        <div id="swiper-container-3" class="swiper-container">
            <div class="items-subservice swiper-wrapper">
            
            <div class="swiper-slide ">
                    <!-- <h3>Analisis del Escrito de Acusación y Observaciones al Descubrimiento</h3> -->
                    <div class="items-description">
                        <p class="description-text">Apoyo en <b>instauración de denuncias</b>
                    </div>
                    <div class="items-img photobox">
                        <img data-src="{{ asset('assets/img/1-service-4-1.jpg') }}" alt="lazy" loading="lazy" >    
                    </div>
                </div>
                <div class="swiper-slide">
                    <!-- <h3>Sustentación en Jucio Oral y las Actuaciones Judiciales que lo Requieran</h3> -->
                    <div class="items-description">
                        <p class="description-text">
                            <b>Identificación</b> de riesgos
                    </div>
                    <div class="items-img photobox">
                        <img data-src="{{ asset('assets/img/1-service-4-2.jpg')}}" alt="lazy" loading="lazy" >    
                    </div>
                </div>
                <div class="swiper-slide">
                    <!-- <h3>Analisis Documental</h3> -->
                    <div class="items-description">
                        <p class="description-text">
                            <b>Seguimiento</b> de procesos judiciales
                        </p>
                    </div>
                    <div class="items-img photobox">
                        <img data-src="{{ asset('assets/img/1-service-4-3.jpg')}}" alt="lazy" loading="lazy" >     
                    </div>
                </div>
                <div class="swiper-slide">
                    <!-- <h3>Analisis Documental</h3> -->
                    <div class="items-description">
                        <p class="description-text">
                            <b>Estudios</b> de seguridad a instalaciones
                        </p>
                    </div>
                    <div class="items-img photobox">
                        <img data-src="{{ asset('assets/img/1-service-4-4.jpg')}}" alt="lazy" loading="lazy" >     
                    </div>
                </div>
                <div class="swiper-slide">
                    <!-- <h3>Analisis Documental</h3> -->
                    <div class="items-description">
                        <p class="description-text">
                            <b>Manejo</b> de crisis
                        </p>
                    </div>
                    <div class="items-img photobox">
                        <img data-src="{{ asset('assets/img/nn.jpg')}}" alt="lazy" loading="lazy" >     
                    </div>
                </div>
                <div class="swiper-slide">
                    <!-- <h3>Analisis Documental</h3> -->
                    <div class="items-description">
                        <p class="description-text">
                            <b>Prevención </b>de lavado de activos
                        </p>
                    </div>
                    <div class="items-img photobox">
                        <img data-src="{{ asset('assets/img/nn.jpg')}}" alt="lazy" loading="lazy" >     
                    </div>
                </div>
                <div class="swiper-slide">
                    <!-- <h3>Analisis Documental</h3> -->
                    <div class="items-description">
                        <p class="description-text">
                            <b>Estudios </b>financieros
                        </p>
                    </div>
                    <div class="items-img photobox">
                        <img data-src="{{ asset('assets/img/nn.jpg')}}" alt="lazy" loading="lazy" >     
                    </div>
                </div>
            </div>

        </div>
    </section>
    <section class="section service-subservice1">
        <div class="row">
            <div class="col-md-8">
                <div class="sub-title-service">
                    <video id="v_3" src="{{ asset('assets/effect/smoke.mp4') }}" autoplay  muted class="smoke-white"></video>
                    <h2>Investigación Empresarial</h2>
                    <div class="fadingEffect"></div>
                </div>
                <div class="img-service">
                    <div class="number-service">
                        <p>03</p>
                    </div>
                    <div class="photobox">
                        <img data-src="{{ asset('assets/img/1-service-3.png')}}" alt="lazy" loading="lazy" >
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="sub-description">
                    <p class="description-text">
                        Desarrollamos investigación interna y externa de eventos que se presentan en la compañía, por medio de la aplicación de programas metodológicos y apoyados con entidades judiciales
                    </p>
                </div>
            </div>
            <div class="col-md-2">
            </div>
        </div>
        <div id="swiper-container-2" class="swiper-container">
            <div class="items-subservice swiper-wrapper">
            <div class="swiper-slide ">
                <h3>Solicitudes a entidades</h3>
                <div class="items-description">
                    <p class="description-text">Se deben realizar solicitudes de información a entidades públicas y privadas.</p>
                </div>
                <div class="items-img photobox">
                    <img data-src="{{ asset('assets/img/1-service-3-1.jpg')}}" alt="lazy" loading="lazy" >
                </div>
            </div>
            <div class="swiper-slide ">
                <h3>Documentación del usuario</h3>
                <div class="items-description">
                    <p class="description-text">Se analiza con base en la documentación que aporte el usuario necesidad de volver a solicitar información para autenticar la ya existente.</p>
                </div>
                <div class="items-img photobox">
                    <img data-src="{{ asset('assets/img/1-service-3-2.jpg')}}" alt="lazy" loading="lazy" >
                </div>
            </div>
            <div class="swiper-slide ">
                <h3>Analisis Documental</h3>
                <div class="items-description">
                    <p class="description-text">Surge con fines de orientación técnica e informativa, representa la información de un documento en un registro estructurado, reduce todos los datos descriptivos físicos y de contenido en un esquema inequívoco, mediante el cual se identificará su favorabilidad para la Defensa Técnica del caso. Se desarrolla a partir de: (Indización, Indicativos, Informativo, Selectivo, Conclusiones, entre otros).</p>
                </div>
                <div class="items-img photobox">
                    <img data-src="{{ asset('assets/img/1-service-3-3.jpg')}}" alt="lazy" loading="lazy" >
                </div>
            </div>
            </div>
        </div>
    </section>
    <section class="section service-subservice2">
        <div class="row">
            <div class="col-md-4">
                <div class="sub-description">
                    <p class="description-text"><strong>Se realizará el acompañamiento integral al Togado de la Defensa Técnica</strong>, desde la planeación, elaboración, y todo el marco contextual de las peticiones a elevar ante el Juzgado Penal Municipal con Función de Control de Garantías, teniendo en cuenta los parámetros técnicos y procedimentales desde el área investigativa para llevar a cabo dicha actuación judicial.</p>
                </div>
            </div>
            
            <div class="col-md-8">
                <div class="sub-title-service right">
                    <video id="v_4" src="{{ asset('assets/effect/smoke.mp4') }}" autoplay  muted class="smoke-black"></video>
                    <h2>Visita Domiciliaria</h2>
                    <div class="fadingEffect black"></div>
                </div>
                <div class="img-service right">
                    <div class="number-service left">
                        <p>04</p>
                    </div>
                    <div class="photobox">
                        <img data-src="{{ asset('assets/img/1-service-4.jpg')}}" alt="lazy" loading="lazy" >

                    </div>
                </div>
            </div>
        </div>
        <div id="swiper-container-3" class="swiper-container">
            <div class="items-subservice swiper-wrapper">
            
            <div class="swiper-slide ">
                    <!-- <h3>Analisis del Escrito de Acusación y Observaciones al Descubrimiento</h3> -->
                    <div class="items-description">
                        <p class="description-text"><b>Elaboración de las respectivas notificaciones a las partes en el proceso, </b>con el fin de cumplir con el requisito establecido denominado “notificación en debida forma”.</p>
                    </div>
                    <div class="items-img photobox">
                        <img data-src="{{ asset('assets/img/1-service-4-1.jpg')}}" alt="lazy" loading="lazy" >    
                    </div>
                </div>
                <div class="swiper-slide">
                    <!-- <h3>Sustentación en Jucio Oral y las Actuaciones Judiciales que lo Requieran</h3> -->
                    <div class="items-description">
                        <p class="description-text">
                            <b>Elaboración de la Orden de Trabajo con membrete del Grupo de Investigaciones Especializado,</b> en la que se plasma la normatividad vigente, el marco contextual del caso, y las peticiones especificas con destino a las respectivas entidades.</p>
                    </div>
                    <div class="items-img photobox">
                        <img data-src="img/1-service-4-2.jpg" alt="lazy" loading="lazy" >    
                    </div>
                </div>
                <div class="swiper-slide">
                    <!-- <h3>Analisis Documental</h3> -->
                    <div class="items-description">
                        <p class="description-text">
                            <b>Organización y señalización en los elementos materiales probatorios </b> que se usaran como sustento del origen de la información a solicitar al Juez Constitucional, por parte del Defensor de Confianza.
                        </p>
                    </div>
                    <div class="items-img photobox">
                        <img data-src="img/1-service-4-3.jpg" alt="lazy" loading="lazy" >     
                    </div>
                </div>
                <div class="swiper-slide">
                    <!-- <h3>Analisis Documental</h3> -->
                    <div class="items-description">
                        <p class="description-text">
                            <b>Acreditación como Investigador Criminalístico Privado </b> facultado para el recaudo de la información legalmente obtenida.
                        </p>
                    </div>
                    <div class="items-img photobox">
                        <img data-src="img/1-service-4-4.jpg" alt="lazy" loading="lazy" >     
                    </div>
                </div>
                <div class="swiper-slide">
                    <!-- <h3>Analisis Documental</h3> -->
                    <div class="items-description">
                        <p class="description-text">
                            <b>Acompañamiento continuo en cada una de las audiencias </b> con relación a esta Búsqueda Selectiva en Bases de Datos, tales como la inicial que se denomina “Control Previo”, la segunda en dado caso que las entidades no den respuesta en los primeros quince (15) días calendario, se deberá solicitar una Audiencia de “Prórroga”, por una única vez en un plazo de quince (15) días calendario, y en el mismo sentido, una tercera audiencia que se denomina “Control de legalidad a los resultados obtenidos”.
                        </p>
                    </div>
                    <div class="items-img photobox">
                        <img data-src="img/nn.jpg" alt="lazy" loading="lazy" >     
                    </div>
                </div>
                <div class="swiper-slide">
                    <!-- <h3>Analisis Documental</h3> -->
                    <div class="items-description">
                        <p class="description-text">
                            <b>Toda la logística de los oficios de radicación, supervisión en los tramites y recaudo en las respectivas entidades,</b> para dar legalidad en las 36 horas siguientes, una vez recibida la información.
                        </p>
                    </div>
                    <div class="items-img photobox">
                        <img data-src="img/nn.jpg" alt="lazy" loading="lazy" >     
                    </div>
                </div>
                <div class="swiper-slide">
                    <!-- <h3>Analisis Documental</h3> -->
                    <div class="items-description">
                        <p class="description-text">
                            <b>Realización de los respectivos Informes de Investigador de Campo,</b> con el fin de dar un Control de Legalidad a los resultados obtenidos.
                        </p>
                    </div>
                    <div class="items-img photobox">
                        <img data-src="img/nn.jpg" alt="lazy" loading="lazy" >     
                    </div>
                </div>
            </div>

        </div>
    </section>
    <section class="section service-subservice1">
        <div class="row">
            <div class="col-md-8">
                <div class="sub-title-service">
                    <video id="v_5" src="{{ asset('assets/effect/smoke.mp4') }}" autoplay  muted class="smoke-white"></video>
                    <h2>Verificación y Referencia Personal</h2>
                    <div class="fadingEffect"></div>
                </div>
                <div class="img-service">
                    <div class="number-service">
                        <p>05</p>
                    </div>
                    <div class="photobox">
                        <img data-src="img/1-service-5.jpg" alt="lazy" loading="lazy" >
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="sub-description">
                    <p class="description-text">
                        <strong>Asistencia como testigo de acreditación, y la incorporación de los elementos materiales probatorios recaudados en el desarrollo del proceso, </strong>debido a que toda actividad investigativa efectuada se sustenta en Despachos Fiscales y en los Estrados Judiciales, cabe resaltar que estas actuaciones son de estricto cumplimiento.
                    </p>
                </div>
            </div>
            <div class="col-md-2">
            </div>
        </div>
        
    </section>
    <section class="section service-subservice2">
        <div class="row">
            <div class="col-md-4">
                <div class="sub-description">
                    <p class="description-text">
                        <b>Absolutamente todas las labores Investigativas y Forenses adelantadas por personal adscrito al GRUPO DE INVESTIGADORES ESPECIALIZADO llevan su sustento en la presentación en tiempos de un Informe Técnico,</b> ya sea de Análisis, de labores de campo e incluso aquellos que ostentan resorte pericial.
                    </p>
                </div>
            </div>
            
            <div class="col-md-8">
                <div class="sub-title-service right">
                    <video id="v_6" src="{{ asset('assets/effect/smoke.mp4') }}" autoplay  muted class="smoke-black"></video>
                    <h2>Verificación de Conflictos</h2>
                    <div class="fadingEffect black"></div>
                </div>
                <div class="img-service right">
                    <div class="number-service left">
                        <p>06</p>
                    </div>
                    <div class="photobox">
                        <img data-src="img/1-service-6.png" alt="lazy" loading="lazy" >

                    </div>
                </div>
            </div>
        </div>
        <div id="swiper-container-4" class="swiper-container">
            
            <div class="items-subservice swiper-wrapper">
                <div class="swiper-slide">
                    <div class="col-md-12">
                        <h3>Informes <b>Ejecutivos</b></h3>
                        <!-- <div class="items-description">
                            <p class="description-text"><b>Elaboración de las respectivas notificaciones a las partes en el proceso, </b>con el fin de cumplir con el requisito establecido denominado “notificación en debida forma”.</p>
                        </div>
                        <div class="items-img photobox">
                            <img data-src="img/nn.jpg" alt="lazy" loading="lazy" >    
                        </div> -->
                    </div>
                    <div class="col-md-12">
                        <h3>Informes de <b>Marcos Contextuales</b></h3>
                        <!-- <div class="items-description">
                            <p class="description-text">
                                <b>Elaboración de la Orden de Trabajo con membrete del Grupo de Investigaciones Especializado,</b> en la que se plasma la normatividad vigente, el marco contextual del caso, y las peticiones especificas con destino a las respectivas entidades.</p>
                        </div>
                        <div class="items-img photobox">
                            <img data-src="img/nn.jpg" alt="lazy" loading="lazy" >    
                        </div> -->
                    </div>
                    <div class="col-md-12">
                        <h3>Informes de <b>Investigador de Campo</b></h3>
                        <!-- <div class="items-description">
                            <p class="description-text">
                                <b>Organización y señalización en los elementos materiales probatorios </b> que se usaran como sustento del origen de la información a solicitar al Juez Constitucional, por parte del Defensor de Confianza.
                            </p>
                        </div>
                        <div class="items-img photobox">
                            <img data-src="img/nn.jpg" alt="lazy" loading="lazy" >     
                        </div> -->
                    </div>
                    <div class="col-md-12">
                        <h3>Registros <b>Fotográficos</b></h3>
                        <!-- <div class="items-description">
                            <p class="description-text">
                                <b>Acreditación como Investigador Criminalístico Privado </b> facultado para el recaudo de la información legalmente obtenida.
                            </p>
                        </div>
                        <div class="items-img photobox">
                            <img data-src="img/nn.jpg" alt="lazy" loading="lazy" >     
                        </div> -->
                    </div>
                </div>
                <div class="swiper-slide">
                    <div class="col-md-12">
                        <h3>informes de <b>Analista en Evidencia Digital</b></h3>
                        <!-- <div class="items-description">
                            <p class="description-text">
                                <b>Acompañamiento continuo en cada una de las audiencias </b> con relación a esta Búsqueda Selectiva en Bases de Datos, tales como la inicial que se denomina “Control Previo”, la segunda en dado caso que las entidades no den respuesta en los primeros quince (15) días calendario, se deberá solicitar una Audiencia de “Prórroga”, por una única vez en un plazo de quince (15) días calendario, y en el mismo sentido, una tercera audiencia que se denomina “Control de legalidad a los resultados obtenidos”.
                            </p>
                        </div>
                        <div class="items-img photobox">
                            <img data-src="img/nn.jpg" alt="lazy" loading="lazy" >     
                        </div> -->
                    </div>
                    <div class="col-md-12">
                        <h3>Análisis de <b>Relevancia</b></h3>
                        <!-- <div class="items-description">
                            <p class="description-text">
                                <b>Toda la logística de los oficios de radicación, supervisión en los tramites y recaudo en las respectivas entidades,</b> para dar legalidad en las 36 horas siguientes, una vez recibida la información.
                            </p>
                        </div>
                        <div class="items-img photobox">
                            <img data-src="img/nn.jpg" alt="lazy" loading="lazy" >     
                        </div> -->
                    </div>
                    <div class="col-md-12">
                        <h3>Informes de <b>Exploración</b></h3>
                        <!-- <div class="items-description">
                            <p class="description-text">
                                <b>Realización de los respectivos Informes de Investigador de Campo,</b> con el fin de dar un Control de Legalidad a los resultados obtenidos.
                            </p>
                        </div>
                        <div class="items-img photobox">
                            <img data-src="img/nn.jpg" alt="lazy" loading="lazy" >     
                        </div> -->
                    </div>
                    <div class="col-md-12">
                        <h3>Cotejo de <b>E.M.P</b></h3>
                        <!-- <div class="items-description">
                            <p class="description-text">
                                <b>Realización de los respectivos Informes de Investigador de Campo,</b> con el fin de dar un Control de Legalidad a los resultados obtenidos.
                            </p>
                        </div>
                        <div class="items-img photobox">
                            <img data-src="img/nn.jpg" alt="lazy" loading="lazy" >     
                        </div> -->
                    </div>
                    
                </div>
                <div class="swiper-slide">
                    <div class="col-md-12">
                        <h3>Informes <b>Cuantitativos y Cualitativos</b></h3>
                        <!-- <div class="items-description">
                            <p class="description-text">
                                <b>Realización de los respectivos Informes de Investigador de Campo,</b> con el fin de dar un Control de Legalidad a los resultados obtenidos.
                            </p>
                        </div>
                        <div class="items-img photobox">
                            <img data-src="img/nn.jpg" alt="lazy" loading="lazy" >     
                        </div> -->
                    </div>
                    <div class="col-md-12">
                        <h3><b>Búsqueda Privada</b> de información</h3>
                        <!-- <div class="items-description">
                            <p class="description-text">
                                <b>Realización de los respectivos Informes de Investigador de Campo,</b> con el fin de dar un Control de Legalidad a los resultados obtenidos.
                            </p>
                        </div>
                        <div class="items-img photobox">
                            <img data-src="img/nn.jpg" alt="lazy" loading="lazy" >     
                        </div> -->
                </div>
            </div>
        </div>
    </section>
    <section class="section service-subservice1">
        <div class="row">
            <div class="col-md-8">
                <div class="sub-title-service">
                    <video id="v_7" src="{{ asset('assets/effect/smoke.mp4') }}" autoplay  muted class="smoke-white"></video>
                    <h2>Verificación Financiera</h2>
                    <div class="fadingEffect"></div>
                </div>
                <div class="img-service">
                    <div class="number-service">
                        <p>07</p>
                    </div>
                    <div class="photobox">
                        <img data-src="img/1-service-7.jpg" alt="lazy" loading="lazy" >
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="sub-description">
                    <p class="description-text">
                        Los gastos que se generen a través de los desplazamientos de los Investigadores, el usuario asumirá el costo de estos, así:
                        <br><br>
                            <b>Desplazamientos a nivel nacional o internacional.</b>
                        <br><br>
                            <b>Viáticos de Alimentación, Hospedaje, Transporte terrestre o aéreo.</b>
                        
                    </p>
                </div>
            </div>
            <div class="col-md-2">
            </div>
        </div>
        
    </section>
    <section class="section service-subservice2">
        <div class="row">
            <div class="col-md-4">
                <div class="sub-description">
                    <p class="description-text">
                        El Grupo de Investigaciones Especializado sugiere los siguientes profesionales con la experticia idónea en cada área, así:
                    </p>
                </div>
            </div>
            
            <div class="col-md-8">
                <div class="sub-title-service right">
                    <video id="v_8" src="{{ asset('assets/effect/smoke.mp4') }}" autoplay  muted class="smoke-black"></video>
                    <h2>Poligrafías</h2>
                    <div class="fadingEffect black"></div>
                </div>
                <div class="img-service right">
                    <div class="number-service left">
                        <p>08</p>
                    </div>
                    <div class="photobox">
                        <img data-src="img/1-service-8.jpg" alt="lazy" loading="lazy" >

                    </div>
                </div>
            </div>
        </div>
        <div class="swiper-container" id="swiper-container-5">
            <div class="items-subservice swiper-wrapper">
                <div class="swiper-slide">
                    <h3>Investigadores Criminalísticos Privados: </h3>
                    <div class="items-description">
                        <p class="description-text">
                            Se encargarán de las labores de campo, recaudo de información legalmente obtenida a través de los diferentes mecanismos como lo son derechos de petición, búsquedas selectivas, extracción de información digital. De igual forma ellos realizaran las entrevistas pertinentes del caso, la obtención de información que repose como archivo comercial, empresarial, personal, el análisis de la información y los programas metodológicos en cada etapa del proceso con la directriz de la Defensa Técnica. De lo anterior presentara los Informes de Investigador de Campo correspondientes
                        </p>
                    </div>
                    <div class="items-img photobox">
                        <img data-src="img/1-service-8-1.jpg" alt="lazy" loading="lazy" >    
                    </div>
                </div>
                <div class="swiper-slide">
                    <h3>Perito – Analista en Evidencia Digital:</h3>
                    <div class="items-description">
                        <p class="description-text">
                            Quien a través de él se recaudará toda la información por medio del Laboratorio de Informática Forense, de aquello que se encuentre almacenado en dispositivos celulares, USB, discos duros, DVD, CD, PC. De igual modo realizara la obtención de Origen, Procedimiento técnico de aseguramiento, sellado digital de correos electrónico-enviados o recibidos entre las partes, y de la mensajería de texto, o WhatsApp. De lo anterior presentara los Informes de tipo Pericial del Laboratorio de Informática Forense.    
                        </p>
                    </div>
                    <div class="items-img photobox">
                        <img data-src="img/1-service-8-2.jpg" alt="lazy" loading="lazy" >    
                    </div>
                </div>
                <div class="swiper-slide">
                    <h3>Emisión De Dictamenes Periciales:</h3>
                    <div class="items-description">
                        <p class="description-text">
                            Consecuentemente todos los procedimientos técnicos mencionados anteriormente, cuentan con todo el soporte del GRUPO DE INVESTIGACIONES ESPECIALIZADO, toda vez que siempre se generan informes técnicos y/o dictámenes periciales que soportan la labor adelantada y posteriormente nuestros Informáticos Forenses, sustentaran ante un Juez de la República, sus procedimientos.
                        </p>
                    </div>
                    <div class="items-img photobox">
                        <img data-src="img/1-service-8-3.jpg" alt="lazy" loading="lazy" >     
                    </div>
                </div>
                <div class="swiper-slide">
                    <h3>El Laboratorio de Ciberseguridad, Informática Forense y Análisis de Evidencia Digital</h3>
                    <div class="items-description">
                        <p class="description-text">
                            procederá a someter los medios ópticos que contienen las interceptaciones descubiertas por el Ente Persecutor del Estado, iniciando con la obtención de imágenes forenses (copia espejo) con el objeto de recaudar los valores de huella hash y suma de verificación de archivos MD5, garantes de los principios de originalidad, mismidad e integridad cumplimiento con lo consagrado en la Ley 906 de 2004 referente a temas relacionados con el debido descubrimiento y la norma ISO / IEC 27037 que regula los procedimientos de obtención, tratamiento y preservación de evidencia digital. Lo anterior, servirá de insumo a la defensa técnica cuando valorando el estado de la evidencia digital, decida solicitar exclusión, rechazo, inadmisión o en su defecto la presentación en juicio de un dictamen pericial que logre derruir lo planteado por la Fiscalía General de la Nación.
                        </p>
                    </div>
                    <div class="items-img photobox">
                        <img data-src="img/1-service-8-4.jpg" alt="lazy" loading="lazy" >     
                    </div>
                </div>
                
            </div>

        </div>
    </section>

@stop