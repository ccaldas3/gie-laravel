
@extends('layouts.servicio_normal')

@section('title', 'Page Title')

@section('sidebar')
    @parent

    <p>This is appended to the master sidebar.</p>
@stop

@section('content')

 <!-- <section class="section service-banner claro"> -->
    <section class="section service-banner oscuro">
        <div class="breadcrumb-div">
            <ul class="breadcrumb">
                <li class="breacrumb-item"><a href="{{ route('ser')}}">Servicios</a> / </li>
                <li class="breacrumb-item breacrumb-item_active">Investigación para defensa técnica y/o representación de victimas</li>
            </ul>
        </div>
        <div class="row">
            <div class="col-md-6 title">
                    <h1 class="title-word title-word_first">Investigación para defensa técnica y/o </h1>
                    <h1 class="title-word">representación de victimas</h1>
            </div>
            <div class="col-md-6 ">
                <img  class="title-image" data-src="{{ asset('assets/img/1-banner2.png') }}" alt="lazy" loading="lazy" >
            </div>
        </div>
        
    </section>
     <section class="section service-intro">
        <div class="row">
            <div class="col-md-4">
                <div class="sub-title">
                    <p class="description-text">¿ cómo lo hacemos ?</p>
                </div>
            </div>
            <div class="col-md-6">
                <div class="description">
                    <p class="description-text">Tenemos un completo servicio que abarca todas las actividades, que fortalecen y cubren un total acompañamiento</p>
                </div>
                <br><br>
                Scroll
            </div>
            <div class="col-md-2">
            </div>
        </div>
    </section>
    <section class="section service-subservice1">
        <div class="row">
            <div class="col-md-8">

                <div class="sub-title-service">
                    <video  id="v_1" src="{{ asset('assets/effect/smoke.mp4') }}" autoplay  muted class="smoke-white"></video>
                    <h2>
                        Recaudo Documental
                    </h2>
                    <div class="fadingEffect"></div>
                </div>
                <div class="img-service">
                    <div class="number-service">
                        <p>01</p>
                    </div>
                    <div class="photobox">
                        <img data-src="{{ asset('assets/img/1-service-1.png') }}" alt="lazy" loading="lazy" >
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="sub-description">
                    <p class="description-text">En su sentido más próximo, se entiende al documento como “todo aquello que represente una idea”, entendiéndose este precepto, el recaudo documental con las formalidades propias de la cadena de custodia, se convierte entonces en un <b>pilar que puede llegar a controvertir y/o aprobar una determinada situación</b>, y de esta forma su valor probatorio.</p>
                </div>
            </div>
            <div class="col-md-2">
            </div>
        </div>
        <!-- <div class="row swiper-container items-subservice"> -->
        <div id="swiper-container-1" class="swiper-container">
                
            
        <div class="items-subservice swiper-wrapper">
            
            <div class="swiper-slide ">
                <h3>Analisis del Escrito de Acusación y Observaciones al Descubrimiento</h3>
                <div class="items-description">
                    <p class="description-text">Como uno de los servicios que mayor gusto genera en nuestros clientes y en los Profesionales del Derecho, se encuentra no solo el acompañamiento y apoyo Técnico en las diversas audiencias, sino además el análisis del Escrito de Acusación, y la participación activa (previa autorización del Togado) en el descubrimiento probatorio, con el fin no solamente de generar un control en lo que el Ente Persecutor del Estado entrega y descubre a la defensa, sino también con el objetivo de proyectar el debido Informe Técnico de observaciones al descubrimiento.</p>
                </div>
                <div class="items-img photobox">
                    <img data-src="{{ asset('assets/img/1-service-1-1.jpg') }}" alt="lazy" loading="lazy" >    
                </div>
            </div>
            <div class="swiper-slide ">
                <h3>Sustentación en Jucio Oral y las Actuaciones Judiciales que lo Requieran</h3>
                <div class="items-description">
                    <p class="description-text">Como bien es sabido, la Ley 906 de 2004, es un Sistema Penal Oral Acusatorio, enfocándose siempre e incluso de manera redundante en la Oralidad, así las cosas, todos nuestros Investigadores Criminalísticos Privados y Expertos Forenses, gozan de amplia experiencia en sustentación en Juicios Orales, convirtiéndose el talento humano del GRUPO DE INVESTIGACIONES ESPECIALIZADO, en la herramienta generadora de éxito en compañía del Togado.</p>
                </div>
                <div class="items-img photobox">
                    <img data-src="{{ asset('assets/img/1-service-1-2.jpg') }}" alt="lazy" loading="lazy" >    
                </div>
            </div>
            <div class="swiper-slide ">
                <h3>Analisis Documental</h3>
                <div class="items-description">
                    <p class="description-text">Surge con fines de orientación técnica e informativa, representa la información de un documento en un registro estructurado, reduce todos los datos descriptivos físicos y de contenido en un esquema inequívoco, mediante el cual se identificará su favorabilidad para la Defensa Técnica del caso. Se desarrolla a partir de: (Indización, Indicativos, Informativo, Selectivo, Conclusiones, entre otros).</p>
                </div>
                <div class="items-img photobox">
                    <img data-src="{{ asset('assets/img/1-service-1-3.jpg') }}" alt="lazy" loading="lazy" >     
                </div>
            </div>
            
        </div>
        <!-- Add Pagination -->
        </div>
    </section>
    <section class="section service-subservice2">
        <div class="row">
            <div class="col-md-4">
                <div class="sub-description">
                    <p class="description-text">Consiste en recaudar por el medio idóneo (audio, video, escrito) y dependiendo del tipo de entrevista y persona a entrevistar, las manifestaciones realizadas, y que funcionan como mecanismo jurídico para refrescar memoria y/o impugnar credibilidad, sin embargo, <b>es tanto el valor</b> <b>probatorio que puede tener una</b> <b>entrevista, que podría llegar a ser considerada prueba de referencia</b>, toda vez que la misma, es una manifestación realizada fuera del juicio oral y sirve para probar o excluir uno o varios elementos del presunto delito.</p>
                </div>
            </div>
            
            <div class="col-md-8">
                <div class="sub-title-service right">
                    <video id="v_2" src="effect/smoke.mp4" autoplay  muted class="smoke-black"></video>
                    <h2>Realización de Entrevistas</h2>
                    <div class="fadingEffect black"></div>
                </div>
                <div class="img-service right">
                    <div class="number-service left">
                        <p>02</p>
                    </div>
                    <div class="photobox">
                        <img data-src="{{ asset('assets/img/1-service-2.jpg') }}" alt="lazy" loading="lazy" >

                    </div>
                </div>
            </div>
        </div>
        <div class="row item-subservice">
            <div class="col-md-1">
            </div>
            <div class="col-md-4 ">
                <div class="sub-description">
                    <div class="description-text">
                        <div class="photobox">
                            <img data-src="{{ asset('assets/img/1-service-2-1.jpg') }}" style="width: 100%" alt="lazy" loading="lazy" >    
                        </div>
                        <h4>Entrevista escrita </h4>
                    </div>
                </div>
            </div>
            <div class="col-md-1">
            </div>
            <div class="col-md-1">
            </div>
            <div class="col-md-4">
                <div class="sub-description">
                    <p class="description-text">Consiste en recaudar por el medio idóneo (audio, video, escrito) y dependiendo del tipo de entrevista y persona a entrevistar, las manifestaciones realizadas, y que funcionan como mecanismo jurídico para refrescar memoria y/o impugnar credibilidad, sin embargo, <b>es tanto el valor</b> <b>probatorio que puede tener una</b> <b>entrevista, que podría llegar a ser considerada prueba de referencia</b>, toda vez que la misma, es una manifestación realizada fuera del juicio oral y sirve para probar o excluir uno o varios elementos del presunto delito.</p>
                </div>
            </div>
            <div class="col-md-1">
            </div>
            
        </div>
    </section>
    <section class="section service-subservice1">
        <div class="row">
            <div class="col-md-8">
                <div class="sub-title-service">
                    <video id="v_3" src="effect/smoke.mp4" autoplay  muted class="smoke-white"></video>
                    <h2>Obtención de Información</h2>
                    <div class="fadingEffect"></div>
                </div>
                <div class="img-service">
                    <div class="number-service">
                        <p>03</p>
                    </div>
                    <div class="photobox">
                        <img data-src="{{ asset('assets/img/1-service-3.png') }}" alt="lazy" loading="lazy" >
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="sub-description">
                    <p class="description-text"><strong>Es la recolección de elementos materiales probatorios </strong>teniendo en cuenta los principios de cadena de custodia, (información en entidades públicas y privadas del orden nacional y donde se manejen bases de datos y archivos); así mismo en el recaudo de elementos materiales probatorios y/o evidencia física que aporte el usuario como su Archivo Personal.</p>
                </div>
            </div>
            <div class="col-md-2">
            </div>
        </div>
        <div id="swiper-container-2" class="swiper-container">
            <div class="items-subservice swiper-wrapper">
            <div class="swiper-slide ">
                <h3>Solicitudes a entidades</h3>
                <div class="items-description">
                    <p class="description-text">Se deben realizar solicitudes de información a entidades públicas y privadas.</p>
                </div>
                <div class="items-img photobox">
                    <img data-src="{{ asset('assets/img/1-service-3-1.jpg') }}" alt="lazy" loading="lazy" >
                </div>
            </div>
            <div class="swiper-slide ">
                <h3>Documentación del usuario</h3>
                <div class="items-description">
                    <p class="description-text">Se analiza con base en la documentación que aporte el usuario necesidad de volver a solicitar información para autenticar la ya existente.</p>
                </div>
                <div class="items-img photobox">
                    <img data-src="{{ asset('assets/img/1-service-3-2.jpg') }}" alt="lazy" loading="lazy" >
                </div>
            </div>
            <div class="swiper-slide ">
                <h3>Analisis Documental</h3>
                <div class="items-description">
                    <p class="description-text">Surge con fines de orientación técnica e informativa, representa la información de un documento en un registro estructurado, reduce todos los datos descriptivos físicos y de contenido en un esquema inequívoco, mediante el cual se identificará su favorabilidad para la Defensa Técnica del caso. Se desarrolla a partir de: (Indización, Indicativos, Informativo, Selectivo, Conclusiones, entre otros).</p>
                </div>
                <div class="items-img photobox">
                    <img data-src="{{ asset('assets/img/1-service-3-3.jpg') }}" alt="lazy" loading="lazy" >
                </div>
            </div>
            </div>
        </div>
    </section>
    <section class="section service-subservice2">
        <div class="row">
            <div class="col-md-4">
                <div class="sub-description">
                    <p class="description-text"><strong>Se realizará el acompañamiento integral al Togado de la Defensa Técnica</strong>, desde la planeación, elaboración, y todo el marco contextual de las peticiones a elevar ante el Juzgado Penal Municipal con Función de Control de Garantías, teniendo en cuenta los parámetros técnicos y procedimentales desde el área investigativa para llevar a cabo dicha actuación judicial.</p>
                </div>
            </div>
            
            <div class="col-md-8">
                <div class="sub-title-service right">
                    <video id="v_4" src="effect/smoke.mp4" autoplay  muted class="smoke-black"></video>
                    <h2>Audiencia de Búsqueda Selectiva</h2>
                    <div class="fadingEffect black"></div>
                </div>
                <div class="img-service right">
                    <div class="number-service left">
                        <p>04</p>
                    </div>
                    <div class="photobox">
                        <img data-src="{{ asset('assets/img/1-service-4.jpg') }}" alt="lazy" loading="lazy" >

                    </div>
                </div>
            </div>
        </div>
        <div id="swiper-container-3" class="swiper-container">
            <div class="items-subservice swiper-wrapper">
            
            <div class="swiper-slide ">
                    <!-- <h3>Analisis del Escrito de Acusación y Observaciones al Descubrimiento</h3> -->
                    <div class="items-description">
                        <p class="description-text"><b>Elaboración de las respectivas notificaciones a las partes en el proceso, </b>con el fin de cumplir con el requisito establecido denominado “notificación en debida forma”.</p>
                    </div>
                    <div class="items-img photobox">
                        <img data-src="{{ asset('assets/img/1-service-4-1.jpg') }}" alt="lazy" loading="lazy" >    
                    </div>
                </div>
                <div class="swiper-slide">
                    <!-- <h3>Sustentación en Jucio Oral y las Actuaciones Judiciales que lo Requieran</h3> -->
                    <div class="items-description">
                        <p class="description-text">
                            <b>Elaboración de la Orden de Trabajo con membrete del Grupo de Investigaciones Especializado,</b> en la que se plasma la normatividad vigente, el marco contextual del caso, y las peticiones especificas con destino a las respectivas entidades.</p>
                    </div>
                    <div class="items-img photobox">
                        <img data-src="{{ asset('assets/img/1-service-4-2.jpg') }}" alt="lazy" loading="lazy" >    
                    </div>
                </div>
                <div class="swiper-slide">
                    <!-- <h3>Analisis Documental</h3> -->
                    <div class="items-description">
                        <p class="description-text">
                            <b>Organización y señalización en los elementos materiales probatorios </b> que se usaran como sustento del origen de la información a solicitar al Juez Constitucional, por parte del Defensor de Confianza.
                        </p>
                    </div>
                    <div class="items-img photobox">
                        <img data-src="{{ asset('assets/img/1-service-4-3.jpg') }}" alt="lazy" loading="lazy" >     
                    </div>
                </div>
                <div class="swiper-slide">
                    <!-- <h3>Analisis Documental</h3> -->
                    <div class="items-description">
                        <p class="description-text">
                            <b>Acreditación como Investigador Criminalístico Privado </b> facultado para el recaudo de la información legalmente obtenida.
                        </p>
                    </div>
                    <div class="items-img photobox">
                        <img data-src="{{ asset('assets/img/1-service-4-4.jpg') }}" alt="lazy" loading="lazy" >     
                    </div>
                </div>
                <div class="swiper-slide">
                    <!-- <h3>Analisis Documental</h3> -->
                    <div class="items-description">
                        <p class="description-text">
                            <b>Acompañamiento continuo en cada una de las audiencias </b> con relación a esta Búsqueda Selectiva en Bases de Datos, tales como la inicial que se denomina “Control Previo”, la segunda en dado caso que las entidades no den respuesta en los primeros quince (15) días calendario, se deberá solicitar una Audiencia de “Prórroga”, por una única vez en un plazo de quince (15) días calendario, y en el mismo sentido, una tercera audiencia que se denomina “Control de legalidad a los resultados obtenidos”.
                        </p>
                    </div>
                    <div class="items-img photobox">
                        <img data-src="{{ asset('assets/img/nn.jpg') }}" alt="lazy" loading="lazy" >     
                    </div>
                </div>
                <div class="swiper-slide">
                    <!-- <h3>Analisis Documental</h3> -->
                    <div class="items-description">
                        <p class="description-text">
                            <b>Toda la logística de los oficios de radicación, supervisión en los tramites y recaudo en las respectivas entidades,</b> para dar legalidad en las 36 horas siguientes, una vez recibida la información.
                        </p>
                    </div>
                    <div class="items-img photobox">
                        <img data-src="{{ asset('assets/img/nn.jpg') }}" alt="lazy" loading="lazy" >     
                    </div>
                </div>
                <div class="swiper-slide">
                    <!-- <h3>Analisis Documental</h3> -->
                    <div class="items-description">
                        <p class="description-text">
                            <b>Realización de los respectivos Informes de Investigador de Campo,</b> con el fin de dar un Control de Legalidad a los resultados obtenidos.
                        </p>
                    </div>
                    <div class="items-img photobox">
                        <img data-src="{{ asset('assets/img/nn.jpg') }}" alt="lazy" loading="lazy" >     
                    </div>
                </div>
            </div>

        </div>
    </section>
    <section class="section service-subservice1">
        <div class="row">
            <div class="col-md-8">
                <div class="sub-title-service">
                    <video id="v_5" src="effect/smoke.mp4" autoplay  muted class="smoke-white"></video>
                    <h2>Comparencia</h2>
                    <div class="fadingEffect"></div>
                </div>
                <div class="img-service">
                    <div class="number-service">
                        <p>05</p>
                    </div>
                    <div class="photobox">
                        <img data-src="{{ asset('assets/img/1-service-5.jpg') }}" alt="lazy" loading="lazy" >
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="sub-description">
                    <p class="description-text">
                        <strong>Asistencia como testigo de acreditación, y la incorporación de los elementos materiales probatorios recaudados en el desarrollo del proceso, </strong>debido a que toda actividad investigativa efectuada se sustenta en Despachos Fiscales y en los Estrados Judiciales, cabe resaltar que estas actuaciones son de estricto cumplimiento.
                    </p>
                </div>
            </div>
            <div class="col-md-2">
            </div>
        </div>
        
    </section>
    <section class="section service-subservice2">
        <div class="row">
            <div class="col-md-4">
                <div class="sub-description">
                    <p class="description-text">
                        <b>Absolutamente todas las labores Investigativas y Forenses adelantadas por personal adscrito al GRUPO DE INVESTIGADORES ESPECIALIZADO llevan su sustento en la presentación en tiempos de un Informe Técnico,</b> ya sea de Análisis, de labores de campo e incluso aquellos que ostentan resorte pericial.
                    </p>
                </div>
            </div>
            
            <div class="col-md-8">
                <div class="sub-title-service right">
                    <video id="v_6" src="effect/smoke.mp4" autoplay  muted class="smoke-black"></video>
                    <h2>Informes Técnicos</h2>
                    <div class="fadingEffect black"></div>
                </div>
                <div class="img-service right">
                    <div class="number-service left">
                        <p>06</p>
                    </div>
                    <div class="photobox">
                        <img data-src="{{ asset('assets/img/1-service-6.png') }}" alt="lazy" loading="lazy" >

                    </div>
                </div>
            </div>
        </div>
        <div id="swiper-container-4" class="swiper-container">
            
            <div class="items-subservice swiper-wrapper">
                <div class="swiper-slide">
                    <div class="col-md-12">
                        <h3>Informes <b>Ejecutivos</b></h3>
                        <!-- <div class="items-description">
                            <p class="description-text"><b>Elaboración de las respectivas notificaciones a las partes en el proceso, </b>con el fin de cumplir con el requisito establecido denominado “notificación en debida forma”.</p>
                        </div>
                        <div class="items-img photobox">
                            <img data-src="{{ asset('assets/img/nn.jpg') }}" alt="lazy" loading="lazy" >    
                        </div> -->
                    </div>
                    <div class="col-md-12">
                        <h3>Informes de <b>Marcos Contextuales</b></h3>
                        <!-- <div class="items-description">
                            <p class="description-text">
                                <b>Elaboración de la Orden de Trabajo con membrete del Grupo de Investigaciones Especializado,</b> en la que se plasma la normatividad vigente, el marco contextual del caso, y las peticiones especificas con destino a las respectivas entidades.</p>
                        </div>
                        <div class="items-img photobox">
                            <img data-src="{{ asset('assets/img/nn.jpg') }}" alt="lazy" loading="lazy" >    
                        </div> -->
                    </div>
                    <div class="col-md-12">
                        <h3>Informes de <b>Investigador de Campo</b></h3>
                        <!-- <div class="items-description">
                            <p class="description-text">
                                <b>Organización y señalización en los elementos materiales probatorios </b> que se usaran como sustento del origen de la información a solicitar al Juez Constitucional, por parte del Defensor de Confianza.
                            </p>
                        </div>
                        <div class="items-img photobox">
                            <img data-src="{{ asset('assets/img/nn.jpg') }}" alt="lazy" loading="lazy" >     
                        </div> -->
                    </div>
                    <div class="col-md-12">
                        <h3>Registros <b>Fotográficos</b></h3>
                        <!-- <div class="items-description">
                            <p class="description-text">
                                <b>Acreditación como Investigador Criminalístico Privado </b> facultado para el recaudo de la información legalmente obtenida.
                            </p>
                        </div>
                        <div class="items-img photobox">
                            <img data-src="{{ asset('assets/img/nn.jpg') }}" alt="lazy" loading="lazy" >     
                        </div> -->
                    </div>
                </div>
                <div class="swiper-slide">
                    <div class="col-md-12">
                        <h3>informes de <b>Analista en Evidencia Digital</b></h3>
                        <!-- <div class="items-description">
                            <p class="description-text">
                                <b>Acompañamiento continuo en cada una de las audiencias </b> con relación a esta Búsqueda Selectiva en Bases de Datos, tales como la inicial que se denomina “Control Previo”, la segunda en dado caso que las entidades no den respuesta en los primeros quince (15) días calendario, se deberá solicitar una Audiencia de “Prórroga”, por una única vez en un plazo de quince (15) días calendario, y en el mismo sentido, una tercera audiencia que se denomina “Control de legalidad a los resultados obtenidos”.
                            </p>
                        </div>
                        <div class="items-img photobox">
                            <img data-src="{{ asset('assets/img/nn.jpg') }}" alt="lazy" loading="lazy" >     
                        </div> -->
                    </div>
                    <div class="col-md-12">
                        <h3>Análisis de <b>Relevancia</b></h3>
                        <!-- <div class="items-description">
                            <p class="description-text">
                                <b>Toda la logística de los oficios de radicación, supervisión en los tramites y recaudo en las respectivas entidades,</b> para dar legalidad en las 36 horas siguientes, una vez recibida la información.
                            </p>
                        </div>
                        <div class="items-img photobox">
                            <img data-src="{{ asset('assets/img/nn.jpg') }}" alt="lazy" loading="lazy" >     
                        </div> -->
                    </div>
                    <div class="col-md-12">
                        <h3>Informes de <b>Exploración</b></h3>
                        <!-- <div class="items-description">
                            <p class="description-text">
                                <b>Realización de los respectivos Informes de Investigador de Campo,</b> con el fin de dar un Control de Legalidad a los resultados obtenidos.
                            </p>
                        </div>
                        <div class="items-img photobox">
                            <img data-src="{{ asset('assets/img/nn.jpg') }}" alt="lazy" loading="lazy" >     
                        </div> -->
                    </div>
                    <div class="col-md-12">
                        <h3>Cotejo de <b>E.M.P</b></h3>
                        <!-- <div class="items-description">
                            <p class="description-text">
                                <b>Realización de los respectivos Informes de Investigador de Campo,</b> con el fin de dar un Control de Legalidad a los resultados obtenidos.
                            </p>
                        </div>
                        <div class="items-img photobox">
                            <img data-src="{{ asset('assets/img/nn.jpg') }}" alt="lazy" loading="lazy" >     
                        </div> -->
                    </div>
                    
                </div>
                <div class="swiper-slide">
                    <div class="col-md-12">
                        <h3>Informes <b>Cuantitativos y Cualitativos</b></h3>
                        <!-- <div class="items-description">
                            <p class="description-text">
                                <b>Realización de los respectivos Informes de Investigador de Campo,</b> con el fin de dar un Control de Legalidad a los resultados obtenidos.
                            </p>
                        </div>
                        <div class="items-img photobox">
                            <img data-src="{{ asset('assets/img/nn.jpg') }}" alt="lazy" loading="lazy" >     
                        </div> -->
                    </div>
                    <div class="col-md-12">
                        <h3><b>Búsqueda Privada</b> de información</h3>
                        <!-- <div class="items-description">
                            <p class="description-text">
                                <b>Realización de los respectivos Informes de Investigador de Campo,</b> con el fin de dar un Control de Legalidad a los resultados obtenidos.
                            </p>
                        </div>
                        <div class="items-img photobox">
                            <img data-src="{{ asset('assets/img/nn.jpg') }}" alt="lazy" loading="lazy" >     
                        </div> -->
                </div>
            </div>
        </div>
    </section>
    <section class="section service-subservice1">
        <div class="row">
            <div class="col-md-8">
                <div class="sub-title-service">
                    <video id="v_7" src="effect/smoke.mp4" autoplay  muted class="smoke-white"></video>
                    <h2>Gastos Operativos Adicionales</h2>
                    <div class="fadingEffect"></div>
                </div>
                <div class="img-service">
                    <div class="number-service">
                        <p>07</p>
                    </div>
                    <div class="photobox">
                        <img data-src="{{ asset('assets/img/1-service-7.jpg') }}" alt="lazy" loading="lazy" >
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="sub-description">
                    <p class="description-text">
                        Los gastos que se generen a través de los desplazamientos de los Investigadores, el usuario asumirá el costo de estos, así:
                        <br><br>
                            <b>Desplazamientos a nivel nacional o internacional.</b>
                        <br><br>
                            <b>Viáticos de Alimentación, Hospedaje, Transporte terrestre o aéreo.</b>
                        
                    </p>
                </div>
            </div>
            <div class="col-md-2">
            </div>
        </div>
        
    </section>
    <section class="section service-subservice2">
        <div class="row">
            <div class="col-md-4">
                <div class="sub-description">
                    <p class="description-text">
                        El Grupo de Investigaciones Especializado sugiere los siguientes profesionales con la experticia idónea en cada área, así:
                    </p>
                </div>
            </div>
            
            <div class="col-md-8">
                <div class="sub-title-service right">
                    <video id="v_8" src="effect/smoke.mp4" autoplay  muted class="smoke-black"></video>
                    <h2>Conclusiones de Actividades de Audiencia Preparatoria</h2>
                    <div class="fadingEffect black"></div>
                </div>
                <div class="img-service right">
                    <div class="number-service left">
                        <p>08</p>
                    </div>
                    <div class="photobox">
                        <img data-src="{{ asset('assets/img/1-service-8.jpg') }}" alt="lazy" loading="lazy" >

                    </div>
                </div>
            </div>
        </div>
        <div class="swiper-container" id="swiper-container-5">
            <div class="items-subservice swiper-wrapper">
                <div class="swiper-slide">
                    <h3>Investigadores Criminalísticos Privados: </h3>
                    <div class="items-description">
                        <p class="description-text">
                            Se encargarán de las labores de campo, recaudo de información legalmente obtenida a través de los diferentes mecanismos como lo son derechos de petición, búsquedas selectivas, extracción de información digital. De igual forma ellos realizaran las entrevistas pertinentes del caso, la obtención de información que repose como archivo comercial, empresarial, personal, el análisis de la información y los programas metodológicos en cada etapa del proceso con la directriz de la Defensa Técnica. De lo anterior presentara los Informes de Investigador de Campo correspondientes
                        </p>
                    </div>
                    <div class="items-img photobox">
                        <img data-src="{{ asset('assets/img/1-service-8-1.jpg') }}" alt="lazy" loading="lazy" >    
                    </div>
                </div>
                <div class="swiper-slide">
                    <h3>Perito – Analista en Evidencia Digital:</h3>
                    <div class="items-description">
                        <p class="description-text">
                            Quien a través de él se recaudará toda la información por medio del Laboratorio de Informática Forense, de aquello que se encuentre almacenado en dispositivos celulares, USB, discos duros, DVD, CD, PC. De igual modo realizara la obtención de Origen, Procedimiento técnico de aseguramiento, sellado digital de correos electrónico-enviados o recibidos entre las partes, y de la mensajería de texto, o WhatsApp. De lo anterior presentara los Informes de tipo Pericial del Laboratorio de Informática Forense.    
                        </p>
                    </div>
                    <div class="items-img photobox">
                        <img data-src="{{ asset('assets/img/1-service-8-2.jpg') }}" alt="lazy" loading="lazy" >    
                    </div>
                </div>
                <div class="swiper-slide">
                    <h3>Emisión De Dictamenes Periciales:</h3>
                    <div class="items-description">
                        <p class="description-text">
                            Consecuentemente todos los procedimientos técnicos mencionados anteriormente, cuentan con todo el soporte del GRUPO DE INVESTIGACIONES ESPECIALIZADO, toda vez que siempre se generan informes técnicos y/o dictámenes periciales que soportan la labor adelantada y posteriormente nuestros Informáticos Forenses, sustentaran ante un Juez de la República, sus procedimientos.
                        </p>
                    </div>
                    <div class="items-img photobox">
                        <img data-src="{{ asset('assets/img/1-service-8-3.jpg') }}" alt="lazy" loading="lazy" >     
                    </div>
                </div>
                <div class="swiper-slide">
                    <h3>El Laboratorio de Ciberseguridad, Informática Forense y Análisis de Evidencia Digital</h3>
                    <div class="items-description">
                        <p class="description-text">
                            procederá a someter los medios ópticos que contienen las interceptaciones descubiertas por el Ente Persecutor del Estado, iniciando con la obtención de imágenes forenses (copia espejo) con el objeto de recaudar los valores de huella hash y suma de verificación de archivos MD5, garantes de los principios de originalidad, mismidad e integridad cumplimiento con lo consagrado en la Ley 906 de 2004 referente a temas relacionados con el debido descubrimiento y la norma ISO / IEC 27037 que regula los procedimientos de obtención, tratamiento y preservación de evidencia digital. Lo anterior, servirá de insumo a la defensa técnica cuando valorando el estado de la evidencia digital, decida solicitar exclusión, rechazo, inadmisión o en su defecto la presentación en juicio de un dictamen pericial que logre derruir lo planteado por la Fiscalía General de la Nación.
                        </p>
                    </div>
                    <div class="items-img photobox">
                        <img data-src="{{ asset('assets/img/1-service-8-4.jpg') }}" alt="lazy" loading="lazy" >     
                    </div>
                </div>
                
            </div>

        </div>
    </section>
    <section class="section service-subservice1">
        <div class="row">
            <div class="col-md-8">

                <div class="sub-title-service">
                    <video  id="v_9" src="effect/smoke.mp4" autoplay  muted class="smoke-white"></video>
                    <h2>
                        Actividades Complementarias
                    </h2>
                    <div class="fadingEffect"></div>
                </div>
                <div class="img-service">
                    <div class="number-service">
                        <p>09</p>
                    </div>
                    <!-- <div class="photobox">
                        <img data-src="{{ asset('assets/img/1-service-1.png') }}" alt="lazy" loading="lazy" >
                    </div> -->
                </div>
            </div>
            <div class="col-md-4">
                <!-- <div class="sub-description">
                    <p class="description-text">En su sentido más próximo, se entiende al documento como “todo aquello que represente una idea”, entendiéndose este precepto, el recaudo documental con las formalidades propias de la cadena de custodia, se convierte entonces en un <b>pilar que puede llegar a controvertir y/o aprobar una determinada situación</b>, y de esta forma su valor probatorio.</p>
                </div> -->
            </div>
            <div class="col-md-2">
            </div>
        </div>
        <!-- <div class="row swiper-container items-subservice"> -->
        <div class="swiper-container" id="swiper-container-6">
            <div class="items-subservice swiper-wrapper">
                
                <div class="swiper-slide ">
                    <h3>Informe de Arraigo</h3>
                    <div class="items-description">
                        <p class="description-text">
                             (Información personal, familiar, profesional, laboral, comercial, residencial, entre otras)
                        </p>
                    </div>
                    <div class="items-img photobox">
                        <img data-src="{{ asset('assets/img/1-service-8-4.jpg') }}" alt="lazy" loading="lazy" >    
                    </div>
                </div>
                <div class="swiper-slide ">
                    <h3>Informe de Reputación Web </h3>
                    <div class="items-description">
                        <p class="description-text">
                        (Consulta a través de la cédula de cada una de las personas con relación a las bases de datos de entidades públicas)
                        </p>
                    </div>
                    <div class="items-img photobox">
                        <img data-src="{{ asset('assets/img/1-service-8-1.jpg') }}" alt="lazy" loading="lazy" >    
                    </div>
                </div>
                <div class="swiper-slide ">
                    <!-- <h3>Realización de entrevistas</h3> -->
                    <div class="items-description">
                        <p class="description-text">
                            Realización de entrevistas en audio, por escrito o en video con el fin de recaudar prueba testimonial.
                        </p>
                    </div>
                    <div class="items-img photobox">
                        <img data-src="{{ asset('assets/img/1-service-8-2.jpg') }}" alt="lazy" loading="lazy" >     
                    </div>
                </div>
                <div class="swiper-slide ">
                    <h3>Con relación al escrito de acusación </h3>
                    <div class="items-description">
                        <p class="description-text">
                        se realizará el respectivo Informe de Observaciones, con el fin de establecer los elementos materiales probatorios faltantes e Incompletos en el descubrimiento realizado por la Fiscalía General de la Nación en el proceso de la referencia, efectuando un cotejo entre lo plasmado en el Escrito de Acusación, y en las diversas actuaciones de los Servidores de Policía Judicial, versus lo descubierto por el Ente Acusador.
                        </p>
                    </div>
                    <div class="items-img photobox">
                        <img data-src="{{ asset('assets/img/1-service-8-3.jpg') }}" alt="lazy" loading="lazy" >    
                    </div>
                </div>
                <div class="swiper-slide ">
                    <h3>Marco Legal </h3>
                    <div class="items-description">
                        <p class="description-text">
                        Identificar el Marco Legal de los procedimientos realizados por los Servidores de Policía Judicial, desde el punto de extralimitación en el desarrollo de las actividades a realizar versus las órdenes emitidas por el Despacho Fiscal.
                        </p>
                    </div>
                    <div class="items-img photobox">
                        <img data-src="{{ asset('assets/img/1-service-8-4.jpg') }}" alt="lazy" loading="lazy" >    
                    </div>
                </div>
                <!-- <div class="swiper-slide ">
                    <h3>Policía Judicial </h3>
                    <div class="items-description">
                        <p class="description-text">
                        Analizar si dentro de las actuaciones de Policía Judicial, se vulneraron derechos fundamentales, habeas data, reserva comercial, entre otros.
                        </p>
                    </div>
                    <div class="items-img photobox">
                        <img data-src="{{ asset('assets/img/nn.jpg') }}" alt="lazy" loading="lazy" >    
                    </div>
                </div>
                <div class="swiper-slide ">
                    <h3>Almacén de Evidencias de la Fiscalía General de la Nación </h3>
                    <div class="items-description">
                        <p class="description-text">
                        Solicitar y recaudar en debida forma en el Almacén de Evidencias de la Fiscalía General de la Nación, todo lo contentivo a CD, DVD´S, USB, DISCOS DUROS, AUDIOS, VIDEOS, INTERCEPTACIONES, entre otros, correspondientes a la Informática Forense, con el fin de garantizar la mismidad, autenticidad y originalidad de dichos elementos.
                        </p>
                    </div>
                    <div class="items-img photobox">
                        <img data-src="{{ asset('assets/img/nn.jpg') }}" alt="lazy" loading="lazy" >    
                    </div>
                </div>
                <div class="swiper-slide ">
                    <h3>Servidores de Policía Judicial, Peritos y demás personas </h3>
                    <div class="items-description">
                        <p class="description-text">
                        Establecer si la prueba testimonial de los Servidores de Policía Judicial, Peritos y demás personas, corresponden a los elementos documentales y digitales descubiertos por el Ente Acusador
                        </p>
                    </div>
                    <div class="items-img photobox">
                        <img data-src="{{ asset('assets/img/nn.jpg') }}" alt="lazy" loading="lazy" >    
                    </div>
                </div>
                <div class="swiper-slide ">
                    <h3>Construcción de la Defensa </h3>
                    <div class="items-description">
                        <p class="description-text">
                        Llevar a cabo actividades investigativas con el propósito de construir la Defensa de los aquí acusados desde tres líneas de acción, tales como, prueba testimonial, prueba documental y prueba de evidencia digital.
                        </p>
                    </div>
                    <div class="items-img photobox">
                        <img data-src="{{ asset('assets/img/nn.jpg') }}" alt="lazy" loading="lazy" >    
                    </div>
                </div>
                <div class="swiper-slide ">
                    <h3>Derechos de petición a diferentes entidades públicas y privadas </h3>
                    <div class="items-description">
                        <p class="description-text">
                        Realizar derechos de petición a diferentes entidades públicas y privadas, de ser el caso, solicitar audiencias de búsqueda selectiva en bases de datos, o audiencias de levantamiento de reserva.
                        </p>
                    </div>
                    <div class="items-img photobox">
                        <img data-src="{{ asset('assets/img/nn.jpg') }}" alt="lazy" loading="lazy" >    
                    </div>
                </div> 
                <div class="swiper-slide ">
                    <div class="items-description">
                        <p class="description-text">
                        Propuesta de solicitudes de exclusión, rechazo, inadmisión, ilegalidad e ilicitud con relación a todo lo descubierto por la Fiscalía General de la Nación.
                        </p>
                    </div>
                    <div class="items-img photobox">
                        <img data-src="{{ asset('assets/img/nn.jpg') }}" alt="lazy" loading="lazy" >    
                    </div>
                </div>
                <div class="swiper-slide ">
                    <h3>Listado de Descubrimiento Probatorio </h3>
                    <div class="items-description">
                        <p class="description-text">
                        Construir el Listado de Descubrimiento Probatorio de los elementos materiales recaudados por parte de la Defensa Material; a través de Derechos de Petición; mediante Audiencias celebradas en Juzgados Penales Municipales con Función de Control de Garantías. En el mismo sentido, el Listado de Prueba Testimonial, de Dictámenes Periciales, y de elementos en común con el Ente Persecutor.
                        </p>
                    </div>
                    <div class="items-img photobox">
                        <img data-src="{{ asset('assets/img/nn.jpg') }}" alt="lazy" loading="lazy" >    
                    </div>
                </div>
                <div class="swiper-slide ">
                    <h3>Entrega de los elementos materiales </h3>
                    <div class="items-description">
                        <p class="description-text">
                        Se hace entrega de los elementos materiales probatorios de la Defensa Técnica al Ente Acusador, conservando los protocolos de Cadena de Custodia.
                        </p>
                    </div>
                    <div class="items-img photobox">
                        <img data-src="{{ asset('assets/img/nn.jpg') }}" alt="lazy" loading="lazy" >    
                    </div>
                </div>
                <div class="swiper-slide ">
                    <h3>Informe de Reputación Web </h3>
                    <div class="items-description">
                        <p class="description-text">
                        (Consulta a través de la cédula de cada una de las personas con relación a las bases de datos de entidades públicas)
                        </p>
                    </div>
                    <div class="items-img photobox">
                        <img data-src="{{ asset('assets/img/nn.jpg') }}" alt="lazy" loading="lazy" >    
                    </div>
                </div>
                <div class="swiper-slide ">
                    <h3>Preparación y propuesta de los contrainterrogatorios </h3>
                    <div class="items-description">
                        <p class="description-text">
                        Preparación y propuesta de los contrainterrogatorios desde el área de procedimientos investigativos y periciales a los Servidores de Policía Judicial.
                        </p>
                    </div>
                    <div class="items-img photobox">
                        <img data-src="{{ asset('assets/img/nn.jpg') }}" alt="lazy" loading="lazy" >    
                    </div>
                </div>-->
            </div>
        </div>
    </section>
    <section class="section contact-section">
        <div class="container">
            <div class="text-box">
                <h2 class="title">GRUPO DE INVESTIGACIONES ESPECIALIZADO</h2>
                <p class="text">Profesionales en Investigación críminal y Ciencias Forenses, asesoría integral y orientación.</p>
            </div>
            <div class="text-box">
                <h2 class="title">SEDE CENTRO</h2>
                <p class="text"><i class="fas fa-building"></i> Av Jimenez # 8A - 77, Edif. Seguros Universal, Bogotá</p>
                <p class="text">204 - Oficina Administrativa y contable</p>
                <p class="text">301 - Laboratorio de Ciberseguridad e informática Forense</p>
                <p class="text">303 - Coordinación de Investigaciones</p>
                <p class="text"><i class="fas fa-building"></i> Carrera 9 # 13-36 Edif. Colombia, Bogotá</p>
                <p class="text">602 - Capacitaciones GIE</p>
            </div>
            <div class="text-box">
                <h2 class="title">CONTACTO</h2>
                <p class="text"><i class="fas fa-envelope"></i> juanmiguelangarita@gmail.com</p>
                <p class="text"><i class="fas fa-phone-alt"></i> (+57) 310 511 0889</p>
                <div class="social-box">
                    <span class="follow">SÍGUENOS</span>
                    <a href="#" class="social-item"><i class="fab fa-facebook-f"></i></a>
                    <a href="#" class="social-item"><i class="fab fa-twitter"></i></a>
                    <a href="#" class="social-item"><i class="fab fa-instagram"></i></a>
                </div>
            </div>
        </div>
    </section>





@stop