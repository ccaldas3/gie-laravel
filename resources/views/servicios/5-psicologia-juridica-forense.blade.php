
@extends('layouts.servicio_normal')

@section('title', 'Page Title')


@section('styles')
    <style type="text/css" media="screen">
        #sb01{
            height: 20vh;
        }
    </style>
@stop


@section('sidebar')
    @parent

    <p>This is appended to the master sidebar.</p>
@stop

@section('content')



<!-- <section class="section service-banner claro"> -->
    <section class="section service-banner oscuro">
        <div class="breadcrumb-div">
            <ul class="breadcrumb">
                <li class="breacrumb-item"><a href="../services/services.html">Servicios</a> / </li>
                <li class="breacrumb-item breacrumb-item_active">Psicologia Juridca Forense</li>
            </ul>
        </div>
        <div class="row">
            <div class="col-md-6 title">
                <br><br><br>
                    <h1 class="title-word title-word_first">Psicologia Juridca </h1>
                    <h1 class="title-word">Forense</h1>
            </div>
            <div class="col-md-6 ">
                <img  class="title-image" data-src="{{ asset('assets/img/1-service-2.jpg') }}" alt="lazy" loading="lazy" >
            </div>
        </div>
        
    </section>
     <section class="section service-intro">
        <div class="row">
            <div class="col-md-4">
                <div class="sub-title">
                    <p class="description-text">¿ Qúe es ?</p>
                </div>
            </div>
            <div class="col-md-6">
                <div class="description">
                    <p class="description-text" style="font-size: 1.1rem">Es el área encargada de brindar asesoría continua e ininterrumpida durante todos los momentos procesales, la aplicación de pruebas psicológicas con sus respectivos programas de software para su interpretación. De igual manera, desarrollan sesiones de trabajo que sean requeridas para la evaluación (dependiendo de las variables que se presenten). Se presentan Dictámenes Periciales y su posterior sustentación en Audiencia de Juicio oral con la correspondiente evidencia demostrativa.</p>
                </div>
                <br><br>
                Scroll
            </div>
            <div class="col-md-2">
            </div>
        </div>
    </section>
    <section class="section service-subservice1">
        <div class="row">
            <div class="col-md-8">

                <div class="sub-title-service" id="sb01">
                    <video  id="v_1" src="{{ asset('assets/effect/smoke.mp4') }}" autoplay  muted class="smoke-white"></video>
                    <h2>
                        Evaluación psicológica forense a la documentación de la presunta víctima
                    </h2>
                    <div class="fadingEffect"></div>
                </div>
                <div class="img-service">
                    <div class="number-service">
                        <p>01</p>
                    </div>
                    <div class="photobox">
                        <img data-src="{{ asset('assets/img/5-service-1.jpg')}}" alt="lazy" loading="lazy" >
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="sub-description">
                    <p class="description-text">
                        <b> En esta etapa se verifica y analiza la información proveniente de organismos de seguridad del estado, </b>relacionada con las anotaciones judiciales referente al evaluado.
                    </p>
                </div>
            </div>
            <div class="col-md-2">
            </div>
        </div>
        <div class="container" style="height: 100vh">
            <div class="row">
                <div class="col-md-2 ">
                </div>
                <div class="col-md-4 ">
                    <div class="sub-description">
                        <p class="description-text">
                        Dadas las condiciones y variables psicológicas inmersas en este proceso, se encuentra útil el adelantar procesos de evaluación psicológica forense a la documentación del procesado, con fines de refutación, e impugnación de credibilidad. Este tipo de abordaje del cual se encarga un profesional de la psicología con formación académica y experticia en el área jurídica y forense obedece al planteamiento riguroso que el modelo ideal ofrecido por la comunidad científica y la academia espera se desarrolle en contextos judiciales y en función de una labor forense
                        </p>
                    </div>
                </div>
                <div class="col-md-2"></div>
                <div class="col-md-4">
                    <img src="{{ asset('assets/img/5-service-1-2.jpg')}}" alt="">
                </div>
            </div>
            
        </div>
        <div class="container" style="height: 100vh">
            <div class="row">
                <div class="col-md-2 ">
                </div>
                <div class="col-md-4 ">
                    <img src="{{ asset('assets/img/5-service-1-3.jpg')}}" alt="">
                    
                </div>
                <div class="col-md-2"></div>
                <div class="col-md-4">
                    <div class="sub-description">
                        <p class="description-text">
                        Según Grisso (1986, 1987 citado en Ávila & Rodríguez – Sutil, 1998) es tarea prioritaria de la Psicología Forense: Establecer nuevos modelos conceptuales, diferentes de los que están en uso en la clínica. El psicólogo forense tiene que establecer los objetivos de la evaluación y construir procedimientos que sean legalmente relevantes. Se debe esforzar en traducir los conocimientos psicológicos para que sean útiles desde el punto de vista legal. Como es obvio, el psicólogo o psicóloga forenses debe poseer un conocimiento suficiente de las características del Sistema Jurídico en el que va a operar […] (p. 149)
                        </p>
                    </div>
                </div>
            </div>
        </div>
        <div class="container" style="height: 100vh">
            <div class="row">
                <div class="col-md-2 ">
                </div>
                <div class="col-md-4 ">
                    <div class="sub-description">
                        <p class="description-text">
                        En la EVALUACIÓN PSICOLÓGICA FORENSE se hace un despliegue de protocolos, técnicas e instrumentos de evaluación psicológica que respondan a las necesidades de la labor encomendada a través de recursos idóneos que cumplan con tal fin. Estos deben hacer parte de una planeación previa de dicho procedimiento con miras a responder a la pregunta judicial dependiendo de diferentes variables como edad del evaluado, condición dentro del proceso, tipo de proceso, constructos psicológicos a evaluar, entre otros.
                        </p>
                    </div>
                </div>
                <div class="col-md-2"></div>
                <div class="col-md-4">
                    <img src="{{ asset('assets/img/5-service-1-4.jpg')}}" alt="">
                </div>
            </div>
            
        </div>
        <div class="container">
            <div class="row">
                
                <div class="col-md-4 ">
                    <img src="{{ asset('assets/img/5-service-1-4.png')}}" alt="">
                </div>
                <div class="col-md-8">
                    <div class="sub-description">
                        <p class="description-text">
                        En la EVALUACIÓN PSICOLÓGICA FORENSE se hace un despliegue de protocolos, técnicas e instrumentos de evaluación psicológica que respondan a las necesidades de la labor encomendada a través de recursos idóneos que cumplan con tal fin. Estos deben hacer parte de una planeación previa de dicho procedimiento con miras a responder a la pregunta judicial dependiendo de diferentes variables como edad del evaluado, condición dentro del proceso, tipo de proceso, constructos psicológicos a evaluar, entre otros.
                        </p>
                    </div>
                </div>
            </div>
        </div>
        <div class="container" style="height: 100vh">
            <div class="row">
                <div class="col-md-2 ">
                </div>
                <div class="col-md-4 ">
                    <img src="{{ asset('assets/img/5-service-1-5.jpg')}}" alt="">
                    
                </div>
                <div class="col-md-1"></div>
                <div class="col-md-5">
                    <div class="sub-description">
                        <p class="description-text">
                        El Committee on Ethical Guidelines for Forensic Pshychologists (1991 citado en Hilterman & Andrés-Pueyo, 2005) señala: […] los evaluadores forenses deben emitir juicios rutinariamente acerca de la credibilidad de diversas fuentes de información, intentar conciliar informaciones contradictorias, y determinar si la información es o no suficientemente comprensible para permitir una toma de decisiones ajustada a la información de que se dispone. Se deben incluir y contrastar en un informe oral o por escrito las decisiones tomadas acerca de la precisión de la información recogida (p. 15). Este señalamiento confirma la rigurosidad exigida a un evaluador en ámbito forense y la necesidad de adelantar los pasos enumeradoscon anterioridad. 
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="section service-subservice2">
        <div class="row">
            <div class="col-md-4">
                <div class="sub-description">
                    <p class="description-text">
                        <b>En esta etapa se verifica y analiza la información proveniente de organismos de seguridad del estado, </b> relacionada con las anotaciones judiciales referente al evaluado.
                    </p>
                </div>
            </div>
            
            <div class="col-md-8">
                <div class="sub-title-service right">
                    <video id="v_4" src="{{ asset('assets/effect/smoke.mp4') }}" autoplay  muted class="smoke-black"></video>
                    <h2>Desarrollo de la Evaluación Conductual y Análisis Funcional</h2>
                    <div class="fadingEffect black"></div>
                </div>
                <div class="img-service right">
                    <div class="number-service left">
                        <p>02</p>
                    </div>
                    <div class="photobox">
                        <img data-src="{{ asset('assets/img/5-service-2.jpg') }}" alt="lazy" loading="lazy" >

                    </div>
                </div>
            </div>
        </div>
        <div id="swiper-container-3" class="swiper-container">
            <div class="items-subservice swiper-wrapper">
            
            <div class="swiper-slide ">
                    <!-- <h3>Analisis del Escrito de Acusación y Observaciones al Descubrimiento</h3> -->
                    <div class="items-description">
                        <p class="description-text">Apoyo en <b>instauración de denuncias</b>
                    </div>
                    <div class="items-img photobox">
                        <img data-src="{{ asset('assets/img/1-service-4-1.jpg') }}" alt="lazy" loading="lazy" >    
                    </div>
                </div>
                <div class="swiper-slide">
                    <!-- <h3>Sustentación en Jucio Oral y las Actuaciones Judiciales que lo Requieran</h3> -->
                    <div class="items-description">
                        <p class="description-text">
                            <b>Identificación</b> de riesgos
                    </div>
                    <div class="items-img photobox">
                        <img data-src="{{ asset('assets/img/1-service-4-2.jpg')}}" alt="lazy" loading="lazy" >    
                    </div>
                </div>
                <div class="swiper-slide">
                    <!-- <h3>Analisis Documental</h3> -->
                    <div class="items-description">
                        <p class="description-text">
                            <b>Seguimiento</b> de procesos judiciales
                        </p>
                    </div>
                    <div class="items-img photobox">
                        <img data-src="{{ asset('assets/img/1-service-4-3.jpg')}}" alt="lazy" loading="lazy" >     
                    </div>
                </div>
                <div class="swiper-slide">
                    <!-- <h3>Analisis Documental</h3> -->
                    <div class="items-description">
                        <p class="description-text">
                            <b>Estudios</b> de seguridad a instalaciones
                        </p>
                    </div>
                    <div class="items-img photobox">
                        <img data-src="{{ asset('assets/img/1-service-4-4.jpg')}}" alt="lazy" loading="lazy" >     
                    </div>
                </div>
                <div class="swiper-slide">
                    <!-- <h3>Analisis Documental</h3> -->
                    <div class="items-description">
                        <p class="description-text">
                            <b>Manejo</b> de crisis
                        </p>
                    </div>
                    <div class="items-img photobox">
                        <img data-src="{{ asset('assets/img/nn.jpg')}}" alt="lazy" loading="lazy" >     
                    </div>
                </div>
                <div class="swiper-slide">
                    <!-- <h3>Analisis Documental</h3> -->
                    <div class="items-description">
                        <p class="description-text">
                            <b>Prevención </b>de lavado de activos
                        </p>
                    </div>
                    <div class="items-img photobox">
                        <img data-src="{{ asset('assets/img/nn.jpg')}}" alt="lazy" loading="lazy" >     
                    </div>
                </div>
                <div class="swiper-slide">
                    <!-- <h3>Analisis Documental</h3> -->
                    <div class="items-description">
                        <p class="description-text">
                            <b>Estudios </b>financieros
                        </p>
                    </div>
                    <div class="items-img photobox">
                        <img data-src="{{ asset('assets/img/nn.jpg')}}" alt="lazy" loading="lazy" >     
                    </div>
                </div>
            </div>

        </div>
    </section>
    <section class="section service-subservice1">
        <div class="row">
            <div class="col-md-8">
                <div class="sub-title-service">
                    <video id="v_3" src="{{ asset('assets/effect/smoke.mp4') }}" autoplay  muted class="smoke-white"></video>
                    <h2>Investigación Empresarial</h2>
                    <div class="fadingEffect"></div>
                </div>
                <div class="img-service">
                    <div class="number-service">
                        <p>03</p>
                    </div>
                    <div class="photobox">
                        <img data-src="{{ asset('assets/img/1-service-3.png')}}" alt="lazy" loading="lazy" >
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="sub-description">
                    <p class="description-text">
                        Desarrollamos investigación interna y externa de eventos que se presentan en la compañía, por medio de la aplicación de programas metodológicos y apoyados con entidades judiciales
                    </p>
                </div>
            </div>
            <div class="col-md-2">
            </div>
        </div>
        <div id="swiper-container-2" class="swiper-container">
            <div class="items-subservice swiper-wrapper">
            <div class="swiper-slide ">
                <h3>Solicitudes a entidades</h3>
                <div class="items-description">
                    <p class="description-text">Se deben realizar solicitudes de información a entidades públicas y privadas.</p>
                </div>
                <div class="items-img photobox">
                    <img data-src="{{ asset('assets/img/1-service-3-1.jpg')}}" alt="lazy" loading="lazy" >
                </div>
            </div>
            <div class="swiper-slide ">
                <h3>Documentación del usuario</h3>
                <div class="items-description">
                    <p class="description-text">Se analiza con base en la documentación que aporte el usuario necesidad de volver a solicitar información para autenticar la ya existente.</p>
                </div>
                <div class="items-img photobox">
                    <img data-src="{{ asset('assets/img/1-service-3-2.jpg')}}" alt="lazy" loading="lazy" >
                </div>
            </div>
            <div class="swiper-slide ">
                <h3>Analisis Documental</h3>
                <div class="items-description">
                    <p class="description-text">Surge con fines de orientación técnica e informativa, representa la información de un documento en un registro estructurado, reduce todos los datos descriptivos físicos y de contenido en un esquema inequívoco, mediante el cual se identificará su favorabilidad para la Defensa Técnica del caso. Se desarrolla a partir de: (Indización, Indicativos, Informativo, Selectivo, Conclusiones, entre otros).</p>
                </div>
                <div class="items-img photobox">
                    <img data-src="{{ asset('assets/img/1-service-3-3.jpg')}}" alt="lazy" loading="lazy" >
                </div>
            </div>
            </div>
        </div>
    </section>
    <section class="section service-subservice2">
        <div class="row">
            <div class="col-md-4">
                <div class="sub-description">
                    <p class="description-text"><strong>Se realizará el acompañamiento integral al Togado de la Defensa Técnica</strong>, desde la planeación, elaboración, y todo el marco contextual de las peticiones a elevar ante el Juzgado Penal Municipal con Función de Control de Garantías, teniendo en cuenta los parámetros técnicos y procedimentales desde el área investigativa para llevar a cabo dicha actuación judicial.</p>
                </div>
            </div>
            
            <div class="col-md-8">
                <div class="sub-title-service right">
                    <video id="v_4" src="{{ asset('assets/effect/smoke.mp4') }}" autoplay  muted class="smoke-black"></video>
                    <h2>Visita Domiciliaria</h2>
                    <div class="fadingEffect black"></div>
                </div>
                <div class="img-service right">
                    <div class="number-service left">
                        <p>04</p>
                    </div>
                    <div class="photobox">
                        <img data-src="{{ asset('assets/img/1-service-4.jpg')}}" alt="lazy" loading="lazy" >

                    </div>
                </div>
            </div>
        </div>
        <div id="swiper-container-3" class="swiper-container">
            <div class="items-subservice swiper-wrapper">
            
            <div class="swiper-slide ">
                    <!-- <h3>Analisis del Escrito de Acusación y Observaciones al Descubrimiento</h3> -->
                    <div class="items-description">
                        <p class="description-text"><b>Elaboración de las respectivas notificaciones a las partes en el proceso, </b>con el fin de cumplir con el requisito establecido denominado “notificación en debida forma”.</p>
                    </div>
                    <div class="items-img photobox">
                        <img data-src="{{ asset('assets/img/1-service-4-1.jpg')}}" alt="lazy" loading="lazy" >    
                    </div>
                </div>
                <div class="swiper-slide">
                    <!-- <h3>Sustentación en Jucio Oral y las Actuaciones Judiciales que lo Requieran</h3> -->
                    <div class="items-description">
                        <p class="description-text">
                            <b>Elaboración de la Orden de Trabajo con membrete del Grupo de Investigaciones Especializado,</b> en la que se plasma la normatividad vigente, el marco contextual del caso, y las peticiones especificas con destino a las respectivas entidades.</p>
                    </div>
                    <div class="items-img photobox">
                        <img data-src="img/1-service-4-2.jpg" alt="lazy" loading="lazy" >    
                    </div>
                </div>
                <div class="swiper-slide">
                    <!-- <h3>Analisis Documental</h3> -->
                    <div class="items-description">
                        <p class="description-text">
                            <b>Organización y señalización en los elementos materiales probatorios </b> que se usaran como sustento del origen de la información a solicitar al Juez Constitucional, por parte del Defensor de Confianza.
                        </p>
                    </div>
                    <div class="items-img photobox">
                        <img data-src="img/1-service-4-3.jpg" alt="lazy" loading="lazy" >     
                    </div>
                </div>
                <div class="swiper-slide">
                    <!-- <h3>Analisis Documental</h3> -->
                    <div class="items-description">
                        <p class="description-text">
                            <b>Acreditación como Investigador Criminalístico Privado </b> facultado para el recaudo de la información legalmente obtenida.
                        </p>
                    </div>
                    <div class="items-img photobox">
                        <img data-src="img/1-service-4-4.jpg" alt="lazy" loading="lazy" >     
                    </div>
                </div>
                <div class="swiper-slide">
                    <!-- <h3>Analisis Documental</h3> -->
                    <div class="items-description">
                        <p class="description-text">
                            <b>Acompañamiento continuo en cada una de las audiencias </b> con relación a esta Búsqueda Selectiva en Bases de Datos, tales como la inicial que se denomina “Control Previo”, la segunda en dado caso que las entidades no den respuesta en los primeros quince (15) días calendario, se deberá solicitar una Audiencia de “Prórroga”, por una única vez en un plazo de quince (15) días calendario, y en el mismo sentido, una tercera audiencia que se denomina “Control de legalidad a los resultados obtenidos”.
                        </p>
                    </div>
                    <div class="items-img photobox">
                        <img data-src="img/nn.jpg" alt="lazy" loading="lazy" >     
                    </div>
                </div>
                <div class="swiper-slide">
                    <!-- <h3>Analisis Documental</h3> -->
                    <div class="items-description">
                        <p class="description-text">
                            <b>Toda la logística de los oficios de radicación, supervisión en los tramites y recaudo en las respectivas entidades,</b> para dar legalidad en las 36 horas siguientes, una vez recibida la información.
                        </p>
                    </div>
                    <div class="items-img photobox">
                        <img data-src="img/nn.jpg" alt="lazy" loading="lazy" >     
                    </div>
                </div>
                <div class="swiper-slide">
                    <!-- <h3>Analisis Documental</h3> -->
                    <div class="items-description">
                        <p class="description-text">
                            <b>Realización de los respectivos Informes de Investigador de Campo,</b> con el fin de dar un Control de Legalidad a los resultados obtenidos.
                        </p>
                    </div>
                    <div class="items-img photobox">
                        <img data-src="img/nn.jpg" alt="lazy" loading="lazy" >     
                    </div>
                </div>
            </div>

        </div>
    </section>
    <section class="section service-subservice1">
        <div class="row">
            <div class="col-md-8">
                <div class="sub-title-service">
                    <video id="v_5" src="{{ asset('assets/effect/smoke.mp4') }}" autoplay  muted class="smoke-white"></video>
                    <h2>Verificación y Referencia Personal</h2>
                    <div class="fadingEffect"></div>
                </div>
                <div class="img-service">
                    <div class="number-service">
                        <p>05</p>
                    </div>
                    <div class="photobox">
                        <img data-src="img/1-service-5.jpg" alt="lazy" loading="lazy" >
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="sub-description">
                    <p class="description-text">
                        <strong>Asistencia como testigo de acreditación, y la incorporación de los elementos materiales probatorios recaudados en el desarrollo del proceso, </strong>debido a que toda actividad investigativa efectuada se sustenta en Despachos Fiscales y en los Estrados Judiciales, cabe resaltar que estas actuaciones son de estricto cumplimiento.
                    </p>
                </div>
            </div>
            <div class="col-md-2">
            </div>
        </div>
        
    </section>
    <section class="section service-subservice2">
        <div class="row">
            <div class="col-md-4">
                <div class="sub-description">
                    <p class="description-text">
                        <b>Absolutamente todas las labores Investigativas y Forenses adelantadas por personal adscrito al GRUPO DE INVESTIGADORES ESPECIALIZADO llevan su sustento en la presentación en tiempos de un Informe Técnico,</b> ya sea de Análisis, de labores de campo e incluso aquellos que ostentan resorte pericial.
                    </p>
                </div>
            </div>
            
            <div class="col-md-8">
                <div class="sub-title-service right">
                    <video id="v_6" src="{{ asset('assets/effect/smoke.mp4') }}" autoplay  muted class="smoke-black"></video>
                    <h2>Verificación de Conflictos</h2>
                    <div class="fadingEffect black"></div>
                </div>
                <div class="img-service right">
                    <div class="number-service left">
                        <p>06</p>
                    </div>
                    <div class="photobox">
                        <img data-src="img/1-service-6.png" alt="lazy" loading="lazy" >

                    </div>
                </div>
            </div>
        </div>
        <div id="swiper-container-4" class="swiper-container">
            
            <div class="items-subservice swiper-wrapper">
                <div class="swiper-slide">
                    <div class="col-md-12">
                        <h3>Informes <b>Ejecutivos</b></h3>
                        <!-- <div class="items-description">
                            <p class="description-text"><b>Elaboración de las respectivas notificaciones a las partes en el proceso, </b>con el fin de cumplir con el requisito establecido denominado “notificación en debida forma”.</p>
                        </div>
                        <div class="items-img photobox">
                            <img data-src="img/nn.jpg" alt="lazy" loading="lazy" >    
                        </div> -->
                    </div>
                    <div class="col-md-12">
                        <h3>Informes de <b>Marcos Contextuales</b></h3>
                        <!-- <div class="items-description">
                            <p class="description-text">
                                <b>Elaboración de la Orden de Trabajo con membrete del Grupo de Investigaciones Especializado,</b> en la que se plasma la normatividad vigente, el marco contextual del caso, y las peticiones especificas con destino a las respectivas entidades.</p>
                        </div>
                        <div class="items-img photobox">
                            <img data-src="img/nn.jpg" alt="lazy" loading="lazy" >    
                        </div> -->
                    </div>
                    <div class="col-md-12">
                        <h3>Informes de <b>Investigador de Campo</b></h3>
                        <!-- <div class="items-description">
                            <p class="description-text">
                                <b>Organización y señalización en los elementos materiales probatorios </b> que se usaran como sustento del origen de la información a solicitar al Juez Constitucional, por parte del Defensor de Confianza.
                            </p>
                        </div>
                        <div class="items-img photobox">
                            <img data-src="img/nn.jpg" alt="lazy" loading="lazy" >     
                        </div> -->
                    </div>
                    <div class="col-md-12">
                        <h3>Registros <b>Fotográficos</b></h3>
                        <!-- <div class="items-description">
                            <p class="description-text">
                                <b>Acreditación como Investigador Criminalístico Privado </b> facultado para el recaudo de la información legalmente obtenida.
                            </p>
                        </div>
                        <div class="items-img photobox">
                            <img data-src="img/nn.jpg" alt="lazy" loading="lazy" >     
                        </div> -->
                    </div>
                </div>
                <div class="swiper-slide">
                    <div class="col-md-12">
                        <h3>informes de <b>Analista en Evidencia Digital</b></h3>
                        <!-- <div class="items-description">
                            <p class="description-text">
                                <b>Acompañamiento continuo en cada una de las audiencias </b> con relación a esta Búsqueda Selectiva en Bases de Datos, tales como la inicial que se denomina “Control Previo”, la segunda en dado caso que las entidades no den respuesta en los primeros quince (15) días calendario, se deberá solicitar una Audiencia de “Prórroga”, por una única vez en un plazo de quince (15) días calendario, y en el mismo sentido, una tercera audiencia que se denomina “Control de legalidad a los resultados obtenidos”.
                            </p>
                        </div>
                        <div class="items-img photobox">
                            <img data-src="img/nn.jpg" alt="lazy" loading="lazy" >     
                        </div> -->
                    </div>
                    <div class="col-md-12">
                        <h3>Análisis de <b>Relevancia</b></h3>
                        <!-- <div class="items-description">
                            <p class="description-text">
                                <b>Toda la logística de los oficios de radicación, supervisión en los tramites y recaudo en las respectivas entidades,</b> para dar legalidad en las 36 horas siguientes, una vez recibida la información.
                            </p>
                        </div>
                        <div class="items-img photobox">
                            <img data-src="img/nn.jpg" alt="lazy" loading="lazy" >     
                        </div> -->
                    </div>
                    <div class="col-md-12">
                        <h3>Informes de <b>Exploración</b></h3>
                        <!-- <div class="items-description">
                            <p class="description-text">
                                <b>Realización de los respectivos Informes de Investigador de Campo,</b> con el fin de dar un Control de Legalidad a los resultados obtenidos.
                            </p>
                        </div>
                        <div class="items-img photobox">
                            <img data-src="img/nn.jpg" alt="lazy" loading="lazy" >     
                        </div> -->
                    </div>
                    <div class="col-md-12">
                        <h3>Cotejo de <b>E.M.P</b></h3>
                        <!-- <div class="items-description">
                            <p class="description-text">
                                <b>Realización de los respectivos Informes de Investigador de Campo,</b> con el fin de dar un Control de Legalidad a los resultados obtenidos.
                            </p>
                        </div>
                        <div class="items-img photobox">
                            <img data-src="img/nn.jpg" alt="lazy" loading="lazy" >     
                        </div> -->
                    </div>
                    
                </div>
                <div class="swiper-slide">
                    <div class="col-md-12">
                        <h3>Informes <b>Cuantitativos y Cualitativos</b></h3>
                        <!-- <div class="items-description">
                            <p class="description-text">
                                <b>Realización de los respectivos Informes de Investigador de Campo,</b> con el fin de dar un Control de Legalidad a los resultados obtenidos.
                            </p>
                        </div>
                        <div class="items-img photobox">
                            <img data-src="img/nn.jpg" alt="lazy" loading="lazy" >     
                        </div> -->
                    </div>
                    <div class="col-md-12">
                        <h3><b>Búsqueda Privada</b> de información</h3>
                        <!-- <div class="items-description">
                            <p class="description-text">
                                <b>Realización de los respectivos Informes de Investigador de Campo,</b> con el fin de dar un Control de Legalidad a los resultados obtenidos.
                            </p>
                        </div>
                        <div class="items-img photobox">
                            <img data-src="img/nn.jpg" alt="lazy" loading="lazy" >     
                        </div> -->
                </div>
            </div>
        </div>
    </section>
    <section class="section service-subservice1">
        <div class="row">
            <div class="col-md-8">
                <div class="sub-title-service">
                    <video id="v_7" src="{{ asset('assets/effect/smoke.mp4') }}" autoplay  muted class="smoke-white"></video>
                    <h2>Verificación Financiera</h2>
                    <div class="fadingEffect"></div>
                </div>
                <div class="img-service">
                    <div class="number-service">
                        <p>07</p>
                    </div>
                    <div class="photobox">
                        <img data-src="img/1-service-7.jpg" alt="lazy" loading="lazy" >
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="sub-description">
                    <p class="description-text">
                        Los gastos que se generen a través de los desplazamientos de los Investigadores, el usuario asumirá el costo de estos, así:
                        <br><br>
                            <b>Desplazamientos a nivel nacional o internacional.</b>
                        <br><br>
                            <b>Viáticos de Alimentación, Hospedaje, Transporte terrestre o aéreo.</b>
                        
                    </p>
                </div>
            </div>
            <div class="col-md-2">
            </div>
        </div>
        
    </section>
    <section class="section service-subservice2">
        <div class="row">
            <div class="col-md-4">
                <div class="sub-description">
                    <p class="description-text">
                        El Grupo de Investigaciones Especializado sugiere los siguientes profesionales con la experticia idónea en cada área, así:
                    </p>
                </div>
            </div>
            
            <div class="col-md-8">
                <div class="sub-title-service right">
                    <video id="v_8" src="{{ asset('assets/effect/smoke.mp4') }}" autoplay  muted class="smoke-black"></video>
                    <h2>Poligrafías</h2>
                    <div class="fadingEffect black"></div>
                </div>
                <div class="img-service right">
                    <div class="number-service left">
                        <p>08</p>
                    </div>
                    <div class="photobox">
                        <img data-src="img/1-service-8.jpg" alt="lazy" loading="lazy" >

                    </div>
                </div>
            </div>
        </div>
        <div class="swiper-container" id="swiper-container-5">
            <div class="items-subservice swiper-wrapper">
                <div class="swiper-slide">
                    <h3>Investigadores Criminalísticos Privados: </h3>
                    <div class="items-description">
                        <p class="description-text">
                            Se encargarán de las labores de campo, recaudo de información legalmente obtenida a través de los diferentes mecanismos como lo son derechos de petición, búsquedas selectivas, extracción de información digital. De igual forma ellos realizaran las entrevistas pertinentes del caso, la obtención de información que repose como archivo comercial, empresarial, personal, el análisis de la información y los programas metodológicos en cada etapa del proceso con la directriz de la Defensa Técnica. De lo anterior presentara los Informes de Investigador de Campo correspondientes
                        </p>
                    </div>
                    <div class="items-img photobox">
                        <img data-src="img/1-service-8-1.jpg" alt="lazy" loading="lazy" >    
                    </div>
                </div>
                <div class="swiper-slide">
                    <h3>Perito – Analista en Evidencia Digital:</h3>
                    <div class="items-description">
                        <p class="description-text">
                            Quien a través de él se recaudará toda la información por medio del Laboratorio de Informática Forense, de aquello que se encuentre almacenado en dispositivos celulares, USB, discos duros, DVD, CD, PC. De igual modo realizara la obtención de Origen, Procedimiento técnico de aseguramiento, sellado digital de correos electrónico-enviados o recibidos entre las partes, y de la mensajería de texto, o WhatsApp. De lo anterior presentara los Informes de tipo Pericial del Laboratorio de Informática Forense.    
                        </p>
                    </div>
                    <div class="items-img photobox">
                        <img data-src="img/1-service-8-2.jpg" alt="lazy" loading="lazy" >    
                    </div>
                </div>
                <div class="swiper-slide">
                    <h3>Emisión De Dictamenes Periciales:</h3>
                    <div class="items-description">
                        <p class="description-text">
                            Consecuentemente todos los procedimientos técnicos mencionados anteriormente, cuentan con todo el soporte del GRUPO DE INVESTIGACIONES ESPECIALIZADO, toda vez que siempre se generan informes técnicos y/o dictámenes periciales que soportan la labor adelantada y posteriormente nuestros Informáticos Forenses, sustentaran ante un Juez de la República, sus procedimientos.
                        </p>
                    </div>
                    <div class="items-img photobox">
                        <img data-src="img/1-service-8-3.jpg" alt="lazy" loading="lazy" >     
                    </div>
                </div>
                <div class="swiper-slide">
                    <h3>El Laboratorio de Ciberseguridad, Informática Forense y Análisis de Evidencia Digital</h3>
                    <div class="items-description">
                        <p class="description-text">
                            procederá a someter los medios ópticos que contienen las interceptaciones descubiertas por el Ente Persecutor del Estado, iniciando con la obtención de imágenes forenses (copia espejo) con el objeto de recaudar los valores de huella hash y suma de verificación de archivos MD5, garantes de los principios de originalidad, mismidad e integridad cumplimiento con lo consagrado en la Ley 906 de 2004 referente a temas relacionados con el debido descubrimiento y la norma ISO / IEC 27037 que regula los procedimientos de obtención, tratamiento y preservación de evidencia digital. Lo anterior, servirá de insumo a la defensa técnica cuando valorando el estado de la evidencia digital, decida solicitar exclusión, rechazo, inadmisión o en su defecto la presentación en juicio de un dictamen pericial que logre derruir lo planteado por la Fiscalía General de la Nación.
                        </p>
                    </div>
                    <div class="items-img photobox">
                        <img data-src="img/1-service-8-4.jpg" alt="lazy" loading="lazy" >     
                    </div>
                </div>
                
            </div>

        </div>
    </section>

@stop