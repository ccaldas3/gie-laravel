<!DOCTYPE html>
<html>
<head>
	<title></title>
	<link rel="stylesheet" type="text/css" href="css.css">

  @include('sub-modulos.menu-ciruclar-css')
</head>
<body>
<!-- https://codepen.io/jcoulterdesign/pen/pjQdGb -->
<div class='grain'></div>
<div class='intro wheel' id="winston">
  <div class='center ' id="dropzone">
    <div class='core'></div>
    <div class='outer_one'>
      <div class='outer_one__piece'></div>
      <div class='outer_one__piece'></div>
      <div class='outer_one__piece'></div>
      <div class='outer_one__piece'></div>
      <div class='outer_one__piece'></div>
      <div class='outer_one__piece'></div>
      <div class='outer_one__piece'></div>
      <div class='outer_one__piece'></div>
      <div class='outer_one__piece'></div>
      <div class='outer_one__piece'></div>
      <div class='outer_one__piece'></div>
      <div class='outer_one__piece'></div>
      <div class='outer_one__piece'></div>
      <div class='outer_one__piece'></div>
      <div class='outer_one__piece'></div>
      <div class='outer_one__piece'></div>
      <div class='outer_one__piece'></div>
      <div class='outer_one__piece'></div>
      <div class='outer_one__piece'></div>
      <div class='outer_one__piece'></div>
      <div class='outer_one__piece'></div>
      <div class='outer_one__piece'></div>
      <div class='outer_one__piece'></div>
      <div class='outer_one__piece'></div>
      <div class='outer_one__piece'></div>
      <div class='outer_one__piece'></div>
      <div class='outer_one__piece'></div>
      <div class='outer_one__piece'></div>
      <div class='outer_one__piece'></div>
      <div class='outer_one__piece'></div>
      <div class='outer_one__piece'></div>
      <div class='outer_one__piece'></div>
      <div class='outer_one__piece'></div>
      <div class='outer_one__piece'></div>
      <div class='outer_one__piece'></div>
      <div class='outer_one__piece'></div>
    </div>
    <div class='outer_two'>
      <div class='outer_two__piece'></div>
      <div class='outer_two__piece'></div>
      <div class='outer_two__piece'></div>
      <div class='outer_two__piece'></div>
      <div class='outer_two__piece'></div>
      <div class='outer_two__piece'></div>
      <div class='outer_two__piece'></div>
      <div class='outer_two__piece'></div>
      <div class='outer_two__piece'></div>
      <div class='outer_two__piece'></div>
      <div class='outer_two__piece'></div>
      <div class='outer_two__piece'></div>
      <div class='outer_two__piece'></div>
      <div class='outer_two__piece'></div>
      <div class='outer_two__piece'></div>
      <div class='outer_two__piece'></div>
      <div class='outer_two__piece'></div>
      <div class='outer_two__piece'></div>
      <div class='outer_two__piece'></div>
      <div class='outer_two__piece'></div>
      <div class='outer_two__piece'></div>
      <div class='outer_two__piece'></div>
      <div class='outer_two__piece'></div>
      <div class='outer_two__piece'></div>
      <div class='outer_two__piece'></div>
      <div class='outer_two__piece'></div>
      <div class='outer_two__piece'></div>
      <div class='outer_two__piece'></div>
      <div class='outer_two__piece'></div>
      <div class='outer_two__piece'></div>
      <div class='outer_two__piece'></div>
      <div class='outer_two__piece'></div>
      <div class='outer_two__piece'></div>
      <div class='outer_two__piece'></div>
      <div class='outer_two__piece'></div>
      <div class='outer_two__piece'></div>
    </div>
    <div class='outer_three'>
      <div class='outer_three__piece'></div>
      <div class='outer_three__piece'></div>
      <div class='outer_three__piece'></div>
      <div class='outer_three__piece'></div>
      <div class='outer_three__piece'></div>
      <div class='outer_three__piece'></div>
      <div class='outer_three__piece'></div>
      <div class='outer_three__piece'></div>
      <div class='outer_three__piece'></div>
      <div class='outer_three__piece'></div>
      <div class='outer_three__piece'></div>
      <div class='outer_three__piece'></div>
      <div class='outer_three__piece'></div>
      <div class='outer_three__piece'></div>
      <div class='outer_three__piece'></div>
      <div class='outer_three__piece'></div>
      <div class='outer_three__piece'></div>
      <div class='outer_three__piece'></div>
      <div class='outer_three__piece'></div>
      <div class='outer_three__piece'></div>
      <div class='outer_three__piece'></div>
      <div class='outer_three__piece'></div>
      <div class='outer_three__piece'></div>
      <div class='outer_three__piece'></div>
      <div class='outer_three__piece'></div>
      <div class='outer_three__piece'></div>
      <div class='outer_three__piece'></div>
      <div class='outer_three__piece'></div>
      <div class='outer_three__piece'></div>
      <div class='outer_three__piece'></div>
      <div class='outer_three__piece'></div>
      <div class='outer_three__piece'></div>
      <div class='outer_three__piece'></div>
      <div class='outer_three__piece'></div>
      <div class='outer_three__piece'></div>
      <div class='outer_three__piece'></div>
    </div>
    <div class='outer_four'>
      <div class='outer_four__piece'></div>
      <div class='outer_four__piece'></div>
      <div class='outer_four__piece'></div>
      <div class='outer_four__piece'></div>
      <div class='outer_four__piece'></div>
      <div class='outer_four__piece'></div>
      <div class='outer_four__piece'></div>
      <div class='outer_four__piece'></div>
      <div class='outer_four__piece'></div>
      <div class='outer_four__piece'></div>
      <div class='outer_four__piece'></div>
      <div class='outer_four__piece'></div>
      <div class='outer_four__piece'></div>
      <div class='outer_four__piece'></div>
      <div class='outer_four__piece'></div>
      <div class='outer_four__piece'></div>
      <div class='outer_four__piece'></div>
      <div class='outer_four__piece'></div>
      <div class='outer_four__piece'></div>
      <div class='outer_four__piece'></div>
      <div class='outer_four__piece'></div>
      <div class='outer_four__piece'></div>
      <div class='outer_four__piece'></div>
      <div class='outer_four__piece'></div>
      <div class='outer_four__piece'></div>
      <div class='outer_four__piece'></div>
      <div class='outer_four__piece'></div>
      <div class='outer_four__piece'></div>
      <div class='outer_four__piece'></div>
      <div class='outer_four__piece'></div>
      <div class='outer_four__piece'></div>
      <div class='outer_four__piece'></div>
      <div class='outer_four__piece'></div>
      <div class='outer_four__piece'></div>
      <div class='outer_four__piece'></div>
      <div class='outer_four__piece'></div>
    </div>
    <div class='outer_five'>
      <div class='outer_five__piece'></div>
      <div class='outer_five__piece'></div>
      <div class='outer_five__piece'></div>
      <div class='outer_five__piece'></div>
      <div class='outer_five__piece'></div>
      <div class='outer_five__piece'></div>
      <div class='outer_five__piece'></div>
      <div class='outer_five__piece'></div>
      <div class='outer_five__piece'></div>
      <div class='outer_five__piece'></div>
      <div class='outer_five__piece'></div>
      <div class='outer_five__piece'></div>
      <div class='outer_five__piece'></div>
      <div class='outer_five__piece'></div>
      <div class='outer_five__piece'></div>
      <div class='outer_five__piece'></div>
      <div class='outer_five__piece'></div>
      <div class='outer_five__piece'></div>
      <div class='outer_five__piece'></div>
      <div class='outer_five__piece'></div>
      <div class='outer_five__piece'></div>
      <div class='outer_five__piece'></div>
      <div class='outer_five__piece'></div>
      <div class='outer_five__piece'></div>
      <div class='outer_five__piece'></div>
      <div class='outer_five__piece'></div>
      <div class='outer_five__piece'></div>
      <div class='outer_five__piece'></div>
      <div class='outer_five__piece'></div>
      <div class='outer_five__piece'></div>
      <div class='outer_five__piece'></div>
      <div class='outer_five__piece'></div>
      <div class='outer_five__piece'></div>
      <div class='outer_five__piece'></div>
      <div class='outer_five__piece'></div>
      <div class='outer_five__piece'></div>
    </div>
    <div class='pieces'>
      <div class='future_ui__piece'>
        <span>Desde las Directrices</span>
        <div class='line'></div>
        <div class='tip'>
          Procedimental
        </div>
      </div>
      <div class='future_ui__piece'>
        <span></span>
        <div class='line'></div>
        <div class='tip'>
          
        </div>
      </div>
      <div class='future_ui__piece'>
        <span>Desde el Derecho Penal</span>
        <div class='line'></div>
        <div class='tip'>
          Presentación de Evidencias Digitales
        </div>
      </div>
      <div class='future_ui__piece'>
        <span></span>
        <div class='line'></div>
        <div class='tip'>
          
        </div>
      </div>
      <div class='future_ui__piece'>
        <span>Desde la Investigación Privada</span>
        <div class='line'></div>
        <div class='tip'>
          Análisis de Vulnerabilidades
        </div>
      </div>
      <div class='future_ui__piece'>
        <span></span>
        <div class='line'></div>
        <div class='tip'>
          
        </div>
      </div>
      <!-- <div class='future_ui__piece blank'></div>
      <div class='future_ui__piece blank'></div>
      <div class='future_ui__piece blank'></div>
      <div class='future_ui__piece blank'></div> -->
      <div class='future_ui__piece blank'></div>
      <div class='future_ui__piece blank'></div>
      <div class='future_ui__piece blank'></div>
      <div class='future_ui__piece blank'></div>
      <div class='future_ui__piece blank'></div>
      <div class='future_ui__piece blank'></div>
      <div class='future_ui__piece blank'></div>
      <div class='future_ui__piece blank'></div>
      <div class='future_ui__piece blank'></div>
      <div class='future_ui__piece blank'></div>
      <div class='future_ui__piece blank'></div>
      <div class='future_ui__piece blank'></div>
      <div class='future_ui__piece blank'></div>
      <div class='future_ui__piece blank'></div>
      <div class='future_ui__piece blank'></div>
      <div class='future_ui__piece blank'></div>
      <div class='future_ui__piece blank'></div>
      <div class='future_ui__piece blank'></div>
      <div class='future_ui__piece blank'></div>
      <div class='future_ui__piece blank'></div>
      <div class='future_ui__piece blank'></div>
      <div class='future_ui__piece blank'></div>
      <div class='future_ui__piece blank'></div>
      <div class='future_ui__piece blank'></div>
      <!-- <div class='future_ui__piece blank'></div>
      <div class='future_ui__piece blank'></div>
      <div class='future_ui__piece blank'></div> -->
    </div>
  </div>
</div>

<!-- <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" ></script> -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>
<script src="jquery.circlemenu.js" type="text/javascript" charset="utf-8" ></script>
<script type="text/javascript">
    // $(document).ready(function () {
    //   $('.wheel').circleMenu({
    //     lockDirection: true,
    //     dragMouse: true,
    //     dragTouch: true
    //   });
    // });
  </script>
<script type="text/javascript">
    $(document).ready(function () {

      var e_start = '';
      var e_move = '';
      var e_stop = '';

      //Cuando es movil, utiliza API
      if (isMobil() != null){
        e_start = e_start + 'touchstart ';
        e_move = e_move + 'touchmove ';
        e_stop = e_stop + 'touchend touchcancel ';
      }else{
        e_start = e_start + 'mousedown ';
        e_move = e_move + 'mousemove ';
        e_stop = e_stop + 'mouseup ';
      }

      rotation = 0;
      rotation_sum = 40;
      position_mouse = 0;
      var transform = 'translateY(40vh) translateZ(-870px) rotateX(69deg) rotateY(-26deg) rotatez(-647deg) scale(0.9) ';

      // $(document).on('pageinit', '#index', function () {
      
      
      $('.intro').on(e_start, function ($this) {
          $('.intro').on(e_move, function(e) {
            console.log("Entro");
            
            
            rotation =  getGrades(e);


            console.log(e_move, rotation);
            $(".center").css('transform', 'rotate(' + rotation + 'deg)');
            var rotationString = 'rotate(' + rotation + 'deg)';
            var prev = $('.center').css('transform');
            

            $('.center').css('transform', transform + " " + rotationString);
            e.preventDefault();
          // });
          });
          $(window).one(e_stop, function() {
            console.log("termino");
            $('.intro').off(e_move);
          });

      });

          // Stop event bubbling from the drag
        $('.intro').on(e_start, function(e) {


          console.log("terminó", rotation);
          rotation =  getGrades(e);


          e.stopPropagation();
        });
      });

     var offset = $('.intro').offset();
    function getGrades(e){
      // Change event properties if touch or mouse
            var event = e;
            console.log('e.type', e.type);
            if (isMobil() != null){
              event = e.originalEvent.touches[0] || e.originalEvent.changedTouches[0];
              console.log('event', event);
            }
            var center_x = (offset.left) + ($('.intro').width()/2);

            var center_y = (offset.top) + ($('.intro').height()/2);

            var mouse_x = event.pageX; var mouse_y = event.pageY;

            var radians = Math.atan2(mouse_x - center_x, mouse_y - center_y);

            var degree = (radians * (180 / Math.PI) * -1) + 90; 

            


            var x = event.pageX - $('.intro').offset().left;
            var y = event.pageY - $('.intro').offset().top;

            // position_mouse_tmp = (x + y )/2;
            position_mouse_tmp = degree;


            if(position_mouse_tmp > position_mouse){
              rotation_sum = position_mouse_tmp - position_mouse;
              rotation = rotation + rotation_sum;  
            }else{
              rotation_sum = position_mouse - position_mouse_tmp;
              rotation = rotation - rotation_sum;  
            }

            position_mouse = position_mouse_tmp;

            return degree;
    }


    function isMobil(){
            var mobile = {
              Android: function() {
                return navigator.userAgent.match(/Android/i);
              },
              BlackBerry: function() {
                return navigator.userAgent.match(/BlackBerry/i);
              },
              iOS: function() {
                return navigator.userAgent.match(/iPhone|iPad|iPod/i);
              },
              Opera: function() {
                return navigator.userAgent.match(/Opera Mini/i);
              },
              Windows: function() {
                return navigator.userAgent.match(/IEMobile/i);
              },
              any: function() {
                return (mobile.Android() || mobile.BlackBerry() || mobile.iOS() || mobile.Opera() || mobile.Windows());
              }
            };
            return mobile.any();
        }
  </script>
</body>
</html>