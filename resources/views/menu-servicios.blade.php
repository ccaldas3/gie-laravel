{{-- <aside class="side-bar">
        <input class="tgl tgl-ios" id="btn-menu" type="checkbox" />
        <label class="tgl-btn" for="btn-menu">
            <span id="icon-menu" class='hamburger'></span>
        </label>
    </aside> --}}
    <aside class="menu-services oscuro active">
            <a href="{{ route('home')}}" class="logo transition absolute top-0 w-16 flex justify-center py-4 text-white">
                <!-- <img class="" src="../assets/img/logo2.png" alt="Logo"> -->
                <img class="logo-aside" src="{{asset('assets/img/logo2.png') }} " alt="Logo">
            </a>
            <a href="#" class="top transition absolute bottom-0 whitespace-no-wrap block pt-10 flex items-center text-white">
                <span class="mr-4">Back to top</span>
                <svg width="14" height="19" viewBox="0 0 14 19" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M1 7.12817L7 1.12817L13 7.12817" stroke="#364652" stroke-width="1.5"></path>
                    <path d="M7 1L7 19" stroke="#364652" stroke-width="1.5"></path>
                </svg>
            </a>
            <a href="#" id="btn-menu-service" class="menu-toggle transition text-white">
                <span></span>
                <span></span>
                <span></span>
            </a>
            
            <nav class="animation-delay absolute bottom-0 left-0 pb-5 w-12 lg:w-16 items-center flex flex-col opacity-100 text-white">
                <div id="scroll-down">
                  <span class="arrow-down">
                  <!-- css generated icon -->
                  </span>
                </div>
                {{-- Des
                Li
                Za --}}
            </nav>
    </aside>
    <section id="menu-section-services" class="" ng-controller="PhoneListController">
        <!-- <div class="separator">
            <div class="title">
                <h1>NUESTROS SERVICIOS</h1>
            </div>
            <p class="hashtag-text">#ProfesionalismoGIE</p>
            <span class="follow-container">
                <button class="follow">SÍGUENOS</button>
            </span>
            <div class="social-list">
                <div class="social-item">
                    <a href="#">
                        <i class="fab fa-facebook-f"></i>
                    </a>
                </div>
                <div class="social-item">
                    <a href="#">
                        <i class="fab fa-twitter"></i>
                    </a>
                </div>
                <div class="social-item">
                    <a href="#">
                        <i class="fab fa-instagram"></i>
                    </a>
                </div>
            </div>
        </div> -->
        <div class="content">
            <div id="content">
                <input type="text" name="input" class="input" id="search-input" ng-model="searchText" placeholder="Escriba la busqueda ...">
                <button type="reset" class="search" id="search-btn"></button>
            </div>
            <ul class="category">
                
                <li class="service" ng-repeat="service in services | filter:searchText">
                    <a href="{{url('/')}}/<%service.url%>" title="<%service.bullet%>-<%service.title%>">
                        <h2 class="service-title">
                            <span class="bullet-service"><%service.bullet%></span> <%service.title%>
                        </h2>
                    </a>
                    <ul class="service-items">
                        <%service.subtitle%>
                        <li class="service-item" ng-repeat="item in service.items">
                            <a ng-href="<%item.url%>"><span><%item.sombra%></span> <%item.text%></a>
                        </li>
                    </ul>
                </li>

                {{-- <li class="service">
                    <h2 class="service-title"><span class="bullet-service">E.</span> LABORATORIO DE GRAFÍSTICA</h2>
                    <ul class="service-items">
                        & DOCUMENTOSCOPIA
                        <li class="service-item">
                            <a href="#"><span>Alcance & </span> Procedimientos</a>
                        </li>
                        <li class="service-item">
                            <a href="#"><span>Nuestros</span> Informes Periciales</a>
                        </li>
                        <li class="service-item">
                            <a href="#"><span>Laboratorio de</span> Grafología</a>
                        </li>
                        <li class="service-item">
                            <a href="#"><span>Perfil del</span> Grafólogo Forense</a>
                        </li>
                    </ul>
                </li> --}}
            </ul>
        </div>
    </section>

   
   
 