<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
})->name('home');


Route::get('/circular', function () {
    return view('sub-modulos.menu-circular');
})->name('circular');

Route::get('/home', function () {
    return view('home');
});

Route::get('/servicios', function () {
    return view('servicios');
})->name('ser');

Route::get('/galeria', function () {
    return view('galeria');
})->name('galeria');

Route::get('/contacto', function () {
    return view('contacto');
})->name('contacto');

Route::get('/autorización-para-el-tratamiento-de-datos-personales-sensibles', function () {
    return view('tratamiento_de_datos');
})->name('tratamiento_de_datos');

Route::get('noticias', 'ApisController@twitter')->name('noticias');

# Service 1 
Route::get('/servicios/investigación-para-la-defensa-técnica', function () {
    return view('servicios.1-investigacion-defensa-tecnica');
})->name('defensa-tecnica');
Route::get('/defensa-tecnica', function () {
    return redirect(route('defensa-tecnica'));
});

# Service 2 
Route::get('/servicios/investigación-privada-empresarial', function () {
    return view('servicios.2-investigacion-privada-empresarial');
})->name('investigacion-privada');
Route::get('/investigacion-privada', function () {
    return redirect(route('investigacion-privada'));
});

# Service 3
Route::get('/servicios/Laboratorio-de-ciberseguridad-e-informatica-forense', function () {
    return view('servicios.4-laboratorio-ciberseguridad');
})->name('ciber');
Route::get('/ciber', function () {
    return redirect(route('ciber'));
});

# Service 4
Route::get('/servicios/investigacion-financiera-forense', function () {
    return view('servicios.3-investigacion-financiera-forense');
})->name('investigacion-financiera');
Route::get('/investigacion-financiera', function () {
    return redirect(route('investigacion-financiera'));
});

# Service 5
Route::get('/servicios/psicologia-juridica-forense', function () {
    return view('servicios.5-psicologia-juridica-forense');
})->name('psicologia');

Route::get('/psicologia', function () {
    return redirect(route('psicologia'));
});

Route::get('/service/selector/{from?}', function ($from = null) {
	
    switch ($from) {
    	case 'A':
    		return redirect()->route('defensa-tecnica');
    		break;
    	case 'B':
    		return redirect()->route('investigacion-privada');
    		break;
    	case 'C':
    		return redirect()->route('ciber');
    		break;
    	case 'D':
    		return redirect()->route('investigacion-financiera');
    		break;
    	case 'E':
    		return redirect()->route('psicologia');
    		break;
    	default:
    		return redirect()->route('ser');
    		break;
    }
});




#####################
#####
##### Blog 
#####
#####################

Auth::routes();
Route::group(['prefix' => 'blog', 'as' => 'blog'], function () {
    // User Auth
    Auth::routes();
    Route::post('password/change', 'UserController@changePassword')->middleware('auth');

    // Github Auth Route
    Route::group(['prefix' => 'auth/github'], function () {
        Route::get('/', 'Auth\AuthController@redirectToProvider');
        Route::get('callback', 'Auth\AuthController@handleProviderCallback');
        Route::get('register', 'Auth\AuthController@create');
        Route::post('register', 'Auth\AuthController@store');
    });

    // Search
    Route::get('search', 'HomeController@search');

    // Discussion
    Route::resource('discussion', 'DiscussionController', ['except' => 'destroy']);

    // User
    Route::group(['prefix' => 'user'], function () {
        Route::get('/', 'UserController@index');

        Route::group(['middleware' => 'auth'], function () {
            Route::get('profile', 'UserController@edit');
            Route::put('profile/{id}', 'UserController@update');
            Route::post('follow/{id}', 'UserController@doFollow');
            Route::get('notification', 'UserController@notifications');
            Route::post('notification', 'UserController@markAsRead');
        });

        Route::group(['prefix' => '{username}'], function () {
            Route::get('/', 'UserController@show');
            Route::get('comments', 'UserController@comments');
            Route::get('following', 'UserController@following');
            Route::get('discussions', 'UserController@discussions');
        });
    });

    // User Setting
    Route::group(['middleware' => 'auth', 'prefix' => 'setting'], function () {
        Route::get('/', 'SettingController@index')->name('setting.index');
        Route::get('binding', 'SettingController@binding')->name('setting.binding');

        Route::get('notification', 'SettingController@notification')->name('setting.notification');
        Route::post('notification', 'SettingController@setNotification');
    });

    // Link
    Route::get('link', 'LinkController@index');

    // Category
    Route::group(['prefix' => 'category'], function () {
        Route::get('{category}', 'CategoryController@show');
        Route::get('/', 'CategoryController@index');
    });

    // Tag
    Route::group(['prefix' => 'tag'], function () {
        Route::get('/', 'TagController@index');
        Route::get('{tag}', 'TagController@show');
    });

    // Article
    Route::get('/', 'ArticleController@index');
    Route::get('{slug?}', 'ArticleController@show');


});

/* Dashboard Index */
Route::group(['prefix' => 'dashboard', 'as' =>'dashboard', 'middleware' => ['auth', 'admin']], function () {
   Route::get('{path?}', 'HomeController@dashboard')->where('path', '[\/\w\.-]*');
});